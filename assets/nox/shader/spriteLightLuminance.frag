/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//? required 130

in vec2 fragTexCoord;
in vec4 fragColor;
in vec2 fragLightLuminance;

varying vec2 fragWorldCoord;

uniform sampler2D renderTexture;
uniform sampler2D normalTexture;
uniform sampler2D specularTexture;

// Pixel to world-meter ratio
uniform float ptmRatio;

const int MAX_LIGHTS = 16;
uniform int numLights;
uniform vec2 lightPos[MAX_LIGHTS];
uniform vec3 lightAttenuation[MAX_LIGHTS];
uniform float lightZHeight[MAX_LIGHTS];
uniform float lightRange[MAX_LIGHTS];


out vec4 outputFragColor;
out vec4 outLightLuminance;

vec3 textureNormal()
{
	return texture(normalTexture, fragTexCoord).rgb * 2.0 - 1.0;
}

float specularIntensity()
{
	float noTexture = step(textureSize(specularTexture, 0).x, 1);

	vec4 color = texture(specularTexture, fragTexCoord);
	float avg = (color.r + color.g + color.b) / 3.0;

	float intensity = ((1.0 - noTexture) * avg * color.a) + (noTexture * 1.0);
	return intensity;
}

void main()
{
	vec4 texColor = texture(renderTexture, fragTexCoord) * fragColor;
	vec4 finalColor = texColor;

	float noTexture = step(textureSize(normalTexture, 0).x, 1);
	float finalIntensity = 0.0 + (texColor.a * noTexture);

	for (int i=0; i<numLights; i++)
	{
		// Get the normal of the texture
		vec3 norm = textureNormal();

		// Find the distance to the light
		vec3 diff = vec3(lightPos[i] - fragWorldCoord, lightZHeight[i]);

		vec3 normUnit = normalize(norm);
		vec3 diffUnit = normalize(diff);

		float intensity = max(dot(normUnit, diffUnit), 0.0);

		// Normalize the distance in the XY-plane
		float pixelXyDist = length(diff.xy);
		float meterXyDist = pixelXyDist / ptmRatio;
		float xyDist = (meterXyDist / lightRange[i]) * 100.0;

		vec3 falloff = lightAttenuation[i];
		float attenuation = 1.0 / (falloff.x + (falloff.y*xyDist) + (falloff.z*xyDist*xyDist));
		intensity = intensity * attenuation * specularIntensity();

		finalColor += vec4(vec3(intensity), 0.0);
		finalIntensity += intensity;
	}

	outputFragColor = finalColor;

	outLightLuminance = vec4(
		fragLightLuminance.r * finalIntensity,
		fragLightLuminance.g * finalIntensity,
		0.0,
		texColor.a
	);
}
