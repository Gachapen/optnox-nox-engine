#include <nox/util/Clock.h>

namespace nox { namespace util
{

Clock::Clock()
{
	this->start();
}

void Clock::start()
{
	startTime = std::chrono::high_resolution_clock::now();
}

Duration Clock::getElapsedTime() const
{
	auto end = std::chrono::high_resolution_clock::now();
	auto diff = (end - this->startTime);

	Duration dur = std::chrono::duration_cast<Duration>(diff);
	return dur;
}

}
}
