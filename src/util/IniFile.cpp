#include <nox/util/IniFile.h>
#include <algorithm>
#include <sstream>
#include <cctype>

namespace nox { namespace util {

/**
 * Trim whitespace from a string. Implementation taken from:
 * http://stackoverflow.com/a/17976541
 */
static inline std::string trim(const std::string &s)
{
   auto  wsfront=std::find_if_not(s.begin(),s.end(),
		   [](int c){ return std::isspace(c); });

   return std::string(wsfront,
					 std::find_if_not(s.rbegin(),
					 std::string::const_reverse_iterator(wsfront),
					 [](int c){ return std::isspace(c); }).base());
}


IniFile::Section::Section(std::string name):
	sectionName(name)
{

}

std::string IniFile::Section::getName()
{
	return this->sectionName;
}

std::string IniFile::Section::getString(std::string key, std::string def) const
{
	if (this->kvPairs.count(key) == 0)
	{
		return def;
	}

	return kvPairs[key];
}

long IniFile::Section::getLong(std::string key, long def) const
{
	std::string valstr = this->getString(key, "");
	const char* value = valstr.c_str();
	char* end = nullptr;

	long val = strtol(value, &end, 0);

	if (end > value)
	{
		return val;
	}

	return def;
}

double IniFile::Section::getDouble(std::string key, double def) const
{
	std::string valstr = this->getString(key, "");
	const char* value = valstr.c_str();
	char* end = nullptr;

	double val = strtod(value, &end);

	if (end > value)
	{
		return val;
	}

	return def;
}

bool IniFile::Section::getBool(std::string key, bool def) const
{
	std::string str = this->getString(key, "");
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);

	if (str == "true" || str == "yes" || str == "1" || str == "on")
	{
		return true;
	}

	if (str == "false" || str == "no" || str == "0" || str == "off")
	{
		return false;
	}

	return def;
}


void IniFile::Section::setString(std::string key, std::string val) const
{
	key = trim(key);
	val = trim(val);

	if (!key.empty() && !val.empty())
	{
		this->kvPairs[key] = val;
	}
}

void IniFile::Section::setLong(std::string key, long val) const
{
	std::stringstream ss;
	ss << val;
	this->setString(key, ss.str());
}

void IniFile::Section::setDouble(std::string key, double val) const
{
	std::stringstream ss;
	ss << val;
	this->setString(key, ss.str());
}

void IniFile::Section::setBool(std::string key, bool val) const
{
	this->setString(key, (val ? "true" : "false"));
}


void IniFile::Section::writeToFile(std::ofstream &file)
{
	file << "[" << this->sectionName << "]" << std::endl;

	for (auto pair : this->kvPairs)
	{
		file << pair.first << "=" << pair.second << std::endl;
	}

	file << std::endl;
}

const std::map<std::string,std::string>& IniFile::Section::getValues() const
{
	return this->kvPairs;
}



IniFile::IniFile():
	lastFile("")
{

}

IniFile::~IniFile()
{
	for (auto &p : this->sections)
	{
		delete p.second;
	}
}

bool IniFile::appendParse(std::string file)
{
	int successes = 0;
	std::ifstream strm(file);

	if (!strm.is_open() || !strm.good())
	{
		this->errors.push_back("Unable to open '" + file + "' for reading");
		return false;
	}

	int lineNo = 0;

	std::string line;
	const Section *curSection = nullptr;

	while (strm && ++lineNo)
	{
		std::getline(strm, line);

		auto open = line.find_first_of('[');
		auto close = line.find_first_of(']');
		auto comment = line.find_first_of(';');

		if (open != std::string::npos && close != std::string::npos)
		{
			// Ensure that the open tag occurrs before the closing tag
			if (close < open)
			{
				std::stringstream ss;
				ss << "Expected '[' before ']' on line " << lineNo << ", col " << close+1;
				this->errors.push_back(ss.str());
				continue;
			}

			// Ensure that comments occurr *after* the ]-tag
			if (comment != std::string::npos && comment > open && comment < close)
			{
				std::stringstream ss;
				ss << "Expected ']' before ';' on line " << lineNo << ", col " << comment+1;
				this->errors.push_back(ss.str());
				continue;
			}

			// Ensure that following the end-tag, only comments and whitespace exists
			auto idx = close;
			auto max = (comment != std::string::npos ? comment : line.length());
			while (++idx < max)
			{
				if (!std::isspace(line[idx]))
				{
					std::stringstream ss;
					ss << "Invalid character at line " << lineNo << ", col " << idx;
					this->errors.push_back(ss.str());
					continue;
				}
			}

			std::string secName = line.substr(open+1, close-open-1);

			// Ensure that the name is of non-zero length
			if (secName.empty())
			{
				std::stringstream ss;
				ss << "Empty section name on line " << lineNo << ", col " << open+1;
				this->errors.push_back(ss.str());
				continue;
			}

			// Ensure that the name is valid (alphanumerical only)
			for (char c : secName)
			{
				if (!std::isalnum(c))
				{
					std::stringstream ss;
					ss << "Only alpha-numerical characters allowed in section names (" << secName << ")"
					   << " on line " << lineNo;
					this->errors.push_back(ss.str());
					continue;
				}
			}

			curSection = this->getSection(secName);
		}
		else
		{
			auto equal = line.find_first_of('=');
			auto max = (comment != std::string::npos ? comment : line.size());

			// Ensure that lines not containig "=" are pure comment or empty lines
			if (equal == std::string::npos)
			{
				int idx = 0;
				while (idx < max)
				{
					if (!std::isspace(line[idx]))
					{
						std::stringstream ss;
						ss << "Invalid character on line " << lineNo << ", col " << idx;
						this->errors.push_back(ss.str());
						break;
					}
				}
				continue;
			}

			// Ensure that "=" comes before ";"
			if (comment != std::string::npos && comment < equal)
			{
				std::stringstream ss;
				ss << "Expected '=' before ';' on line " << lineNo << ", col " << comment;
				this->errors.push_back(ss.str());
				continue;
			}

			// Ensure that the line exists under a section
			if (curSection == nullptr)
			{
				std::stringstream ss;
				ss << "Valid  line '" << trim(line.substr(0, max)) << "' on line " << lineNo
				   << " has no section defined.";
				this->errors.push_back(ss.str());
				continue;
			}

			std::string key = line.substr(0, equal);
			std::string val = line.substr(equal + 1, max - equal - 1);

			// Should either "key" or "val" be of zero length, Section ignore it.
			curSection->setString(key, val);
			successes++;
		}
	}

	if (successes != 0)
	{
		this->lastFile = file;
	}

	return (successes != 0);
}

bool IniFile::replaceParse(std::string file)
{
	this->sections.clear();
	return this->appendParse(file);
}

bool IniFile::writeToFile()
{
	if (this->lastFile.empty())
	{
		return false;
	}

	return this->writeToFile(this->lastFile);
}

bool IniFile::writeToFile(std::string file)
{
	std::ofstream strm(file);
	if (strm.is_open() && strm.good())
	{
		for (auto &s : this->sections)
		{
			s.second->writeToFile(strm);
		}

		return true;
	}

	return false;
}

const IniFile::Section* IniFile::getSection(std::string name)
{
	if (this->sections.count(name) == 0)
	{
		this->sections[name] = new Section(name);
	}

	return this->sections[name];
}


std::string IniFile::getError()
{
	std::string error;

	if (this->errors.size())
	{
		error = this->errors.front();
		this->errors.erase(this->errors.begin());
	}

	return error;
}

bool IniFile::hasError()
{
	return !this->errors.empty();
}


} }
