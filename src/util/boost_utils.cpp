/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <glm/gtc/matrix_transform.hpp>
#include <boost/geometry/algorithms/append.hpp>
#include <nox/util/boost_utils.h>

namespace nox { namespace util {

// http://stackoverflow.com/a/1881201
bool isConvex(const BoostPolygon::ring_type& polygonRing)
{
	for (size_t i = 0; i < polygonRing.size() - 1; i++)
	{
		const unsigned int firstIndex = (unsigned int)i;
		const unsigned int secondIndex = (unsigned int)((i + 1) % polygonRing.size());
		const unsigned int thirdIndex = (unsigned int)((i + 2) % polygonRing.size());

		const float dx1 = polygonRing[secondIndex].x - polygonRing[firstIndex].x;
		const float dy1 = polygonRing[secondIndex].y - polygonRing[firstIndex].y;

		const float dx2 = polygonRing[thirdIndex].x - polygonRing[secondIndex].x;
		const float dy2 = polygonRing[thirdIndex].y - polygonRing[secondIndex].y;

		const float zcrossproduct = dx1 * dy2 - dy1 * dx2;

		if (zcrossproduct < 0.0f)
		{
			return false;
		}
	}

	return true;
}

BoostPolygon transformPolygon(const BoostPolygon& polygon, const glm::vec2& translation, float rotation, const glm::vec2& scale)
{
	glm::mat4 transformMatrix;
	
	if (translation.x != 0.0f || translation.y != 0.0f)
	{
		transformMatrix = glm::translate(transformMatrix, glm::vec3(translation, 0.0f));
	}

	if (rotation != 0.0f)
	{
		transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	}

	if (scale.x != 1.0f || scale.y != 1.0f)
	{
		transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));
	}

	BoostPolygon transformedPolygon;
	
	for (const auto& vertex : polygon.outer())
	{
		glm::vec4 translatedVertex = transformMatrix * glm::vec4(vertex, 0.0f, 1.0f);
		boost::geometry::append(transformedPolygon, glm::vec2(translatedVertex));
	}
	
	for (const auto& innerRing : polygon.inners())
	{
		BoostRing translatedRing;

		for (const auto& vertex : innerRing)
		{
			glm::vec4 translatedVertex = transformMatrix * glm::vec4(vertex, 0.0f, 1.0f);
			boost::geometry::append(translatedRing, glm::vec2(translatedVertex));
		}

		transformedPolygon.inners().push_back(std::move(translatedRing));
	}

	return transformedPolygon;
}

BoostMultiPolygon transformPolygons(const BoostMultiPolygon& polygons, const glm::vec2& translation, float rotation, const glm::vec2& scale)
{
	glm::mat4 transformMatrix;

	if (translation.x != 0.0f || translation.y != 0.0f)
	{
		transformMatrix = glm::translate(transformMatrix, glm::vec3(translation, 0.0f));
	}

	if (rotation != 0.0f)
	{
		transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	}

	if (scale.x != 1.0f || scale.y != 1.0f)
	{
		transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));
	}

	BoostMultiPolygon transformedPolygons;

	for (const BoostPolygon& polygon : polygons)
	{
		transformedPolygons.emplace_back();

		BoostPolygon& transformedPolygon = transformedPolygons.back();

		for (const auto& vertex : polygon.outer())
		{
			glm::vec4 translatedVertex = transformMatrix * glm::vec4(vertex, 0.0f, 1.0f);
			boost::geometry::append(transformedPolygon, glm::vec2(translatedVertex));
		}

		for (const auto& innerRing : polygon.inners())
		{
			transformedPolygon.inners().emplace_back();

			BoostRing& translatedRing = transformedPolygon.inners().back();

			for (const auto& vertex : innerRing)
			{
				glm::vec4 translatedVertex = transformMatrix * glm::vec4(vertex, 0.0f, 1.0f);
				boost::geometry::append(translatedRing, glm::vec2(translatedVertex));
			}
		}
	}

	return transformedPolygons;
}

BoostBox transformBox(const BoostBox& box, const glm::vec2& translation, float rotation, const glm::vec2& scale)
{
	glm::mat4 transformMatrix;

	if (translation.x != 0.0f || translation.y != 0.0f)
	{
		transformMatrix = glm::translate(transformMatrix, glm::vec3(translation, 0.0f));
	}

	if (rotation != 0.0f)
	{
		transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	}

	if (scale.x != 1.0f || scale.y != 1.0f)
	{
		transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));
	}

	BoostBox transformedBox;
	transformedBox.min_corner() = glm::vec2(transformMatrix * glm::vec4(box.min_corner(), 0.0f, 1.0f));
	transformedBox.max_corner() = glm::vec2(transformMatrix * glm::vec4(box.max_corner(), 0.0f, 1.0f));

	return transformedBox;
}

void removeClosePoints(const BoostRing& inputRing, BoostRing& outputRing, const float minPointDistance)
{
	if (inputRing.size() <= 3)
	{
		boost::geometry::append(outputRing, inputRing);
	}
	else
	{
		const auto beginIt = inputRing.cbegin();
		const auto endIt = inputRing.cend();

		auto it = beginIt;
		while (it != endIt)
		{
			const glm::vec2 currentPoint = *it;

			auto nextIt = it + 1;
			float distance = glm::distance(currentPoint, *nextIt);

			while (distance < minPointDistance && nextIt != endIt)
			{
				++nextIt;

				if (nextIt != endIt)
				{
					distance = glm::distance(currentPoint, *nextIt);
				}
			}

			if (nextIt != endIt)
			{
				boost::geometry::append(outputRing, currentPoint);
			}
			else
			{
				boost::geometry::append(outputRing, *(endIt - 1));
			}

			it = nextIt;
		}
	}
}

void removeClosePoints(const BoostPolygon& inputPolygon, BoostPolygon& outputPolygon, const float minPointDistance)
{
	BoostPolygon::ring_type outputOuterRing;
	removeClosePoints(inputPolygon.outer(), outputOuterRing, minPointDistance);

	if (outputOuterRing.size() >= 4)
	{
		outputPolygon.outer() = std::move(outputOuterRing);
	}

	for (const BoostPolygon::ring_type& innerRing : inputPolygon.inners())
	{
		BoostPolygon::ring_type outputInnnerRing;
		removeClosePoints(innerRing, outputInnnerRing, minPointDistance);

		if (outputInnnerRing.size() >= 4)
		{
			outputPolygon.inners().push_back(std::move(outputInnnerRing));
		}
	}
}

void removeClosePoints(BoostPolygon& polygon, const float minPointDistance)
{
	BoostPolygon outputPolygon;
	removeClosePoints(polygon, outputPolygon, minPointDistance);

	polygon = std::move(outputPolygon);
}

} }
