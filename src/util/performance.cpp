#include <nox/util/performance.h>
#include <boost/filesystem.hpp>
#include <fstream>
#include <algorithm>


namespace nox { namespace util { namespace perf
{

/*
==================
DataSet
==================
*/
DataSet::DataSet(std::string nm, DataType dtype, AccumulationType atype, int curFrame):
	name(nm),
	accType(atype),
	dataType(dtype),
	startFrame(curFrame),
	curEntry(0.0),
	clockRunning(false)
{

}

std::string DataSet::getName() const
{
	return this->name;
}

AccumulationType DataSet::getAccumulationType() const
{
	return this->accType;
}

DataType DataSet::getDataType() const
{
	return this->dataType;
}

void DataSet::onNewFrame()
{
	if (this->accType == AccumulationType::FRAME)
	{
		bool restartTimer = false;
		if (this->dataType == DataType::TIME && this->clockRunning)
		{
			this->stopTimer();
			restartTimer = true;
		}

		// Stash away the current entry
		this->entries.push_back(this->curEntry);
		this->curEntry = 0.0;

		// Restart the timer if it was already running.
		if (restartTimer)
		{
			this->startTimer();
		}
	}
}

bool DataSet::startTimer()
{
	if (this->dataType != DataType::TIME)
	{
		// Not allowed to take time in non-TIME DataSet.
		return false;
	}

	if (!this->clockRunning)
	{
		this->clock.start();
		this->clockRunning = true;
		return true;
	}

	return false;
}

bool DataSet::stopTimer()
{
	if (this->dataType != DataType::TIME)
	{
		return false;
	}

	if (this->clockRunning)
	{
		this->clockRunning = false;
		Duration dur = this->clock.getElapsedTime();

		std::chrono::duration<long double> seconds;
		seconds = std::chrono::duration_cast<std::chrono::seconds>(dur);

		this->curEntry += seconds.count();
		return true;
	}

	return false;
}

bool DataSet::addCountValue(long double value)
{
	if (this->dataType != DataType::COUNT)
	{
		return false;
	}

	this->curEntry += value;
	return true;
}

void DataSet::writeToFile(std::ofstream& file)
{
	// "myDataSet" data.time acc.frame
	// 0 14.3
	// 1 20.9
	// 3 5.4
	// ...
	file << "\"" << this->name << "\"";
	file << " ";
	file << (this->dataType == DataType::TIME ? "data.time" : "data.count");
	file << " ";
	file << (this->accType == AccumulationType::GLOBAL ? "acc.global" : "acc.frame");
	file << std::endl;

	if (this->accType == AccumulationType::FRAME)
	{
		int frame = this->startFrame;
		for (auto entry : this->entries)
		{
			file << frame << " " << entry << std::endl;
			frame++;
		}

		file << frame << " " << this->curEntry << std::endl;
	}
	else
	{
		file << this->curEntry << std::endl;
	}

	file << std::endl;
}


/*
==================
PerformanceMonitor
==================
*/
PerformanceMonitor::PerformanceMonitor(std::string name):
	name(name),
	copy(false)
{
	PerformanceManager::activeMonitors.push_back(this);
}

PerformanceMonitor::PerformanceMonitor(PerformanceMonitor* other):
	copy(true)
{
	this->name = other->name;
	this->dataSets = other->dataSets;
	this->events = other->events;

	other->dataSets.clear();
	other->events.clear();
}

PerformanceMonitor::~PerformanceMonitor()
{
	if (!this->copy)
	{
		PerformanceMonitor copy(*this);

		PerformanceManager::activeMonitors.remove(this);
		PerformanceManager::deadMonitors.push_back(copy);
	}
	else
	{
		for (auto it = this->dataSets.begin(); it != this->dataSets.end(); it++)
		{
			delete it->second;
		}
	}
}

bool PerformanceMonitor::addDataSet(std::string setName, DataType dtype, AccumulationType atype)
{
	if (this->dataSets.count(setName) != 0)
	{
		return false;
	}

	DataSet *set = new DataSet(setName, dtype, atype, PerformanceManager::frameNo);
	this->dataSets[setName] = set;
	return true;
}

bool PerformanceMonitor::add(std::string setName, long double value)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->addCountValue(value);
	}

	return false;
}

bool PerformanceMonitor::startTimer(std::string setName)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->startTimer();
	}

	return false;
}

bool PerformanceMonitor::stopTimer(std::string setName)
{
	auto iter = this->dataSets.find(setName);

	if (iter != this->dataSets.end())
	{
		return iter->second->stopTimer();
	}

	return false;
}

void PerformanceMonitor::recordEvent(std::string name)
{
	std::replace(name.begin(), name.end(), '\n', ' ');
	std::replace(name.begin(), name.end(), '"', '\'');

	this->events.push_back(std::pair<std::string,int>(name, PerformanceManager::frameNo));
}

bool PerformanceMonitor::dumpDataToFile(std::string filePath)
{
	std::ofstream file(filePath);

	if (!file.is_open() || !file.good())
	{
		return false;
	}

	file << this->name << std::endl << std::endl;

	if (this->dataSets.size())
	{
		file << "DATA" << std::endl;
		for (auto pair : this->dataSets)
		{
			pair.second->writeToFile(file);
		}
	}

	if (this->events.size())
	{
		file << "EVENT" << std::endl;
		for (auto pair : this->events)
		{
			file << pair.second << " " << pair.first << std::endl;
		}
	}

	return true;
}

void PerformanceMonitor::onNewFrame()
{
	for (auto pair : this->dataSets)
	{
		pair.second->onNewFrame();
	}
}


/*
==================
PerformanceManager
==================
*/
int PerformanceManager::frameNo = 0;
std::list<PerformanceMonitor*> PerformanceManager::activeMonitors;
std::list<PerformanceMonitor> PerformanceManager::deadMonitors;


void PerformanceManager::onFrameBegin()
{
	PerformanceManager::frameNo++;

	for (PerformanceMonitor *perfMon : activeMonitors)
	{
		perfMon->onNewFrame();
	}
}

void PerformanceManager::dumpPerformanceData(std::string directory)
{
	std::vector<PerformanceMonitor*> monitors;
	monitors.reserve(activeMonitors.size() + deadMonitors.size());

	for (auto pm : activeMonitors)
	{
		monitors.push_back(pm);
	}

	for (auto &pm : deadMonitors)
	{
		monitors.push_back(&pm);
	}

	boost::filesystem::path path(directory);

	for (auto pm : monitors)
	{
		boost::filesystem::path pmFile(path);

		// Someone should be mutilated for thinking this overload is sensible.
		// Either way, it appends the path.
		pmFile /= (pm->name + ".perfmon");

		pm->dumpDataToFile(std::string(pmFile.c_str()));
	}
}

}
} }
