/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/window/SdlKeyboardControlMapper.h>

#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/util/json_utils.h>

#include <glm/geometric.hpp>
#include <cassert>

namespace nox { namespace window
{

SdlKeyboardControlMapper::SdlKeyboardControlMapper():
	controlledActor(nullptr)
{
}

void SdlKeyboardControlMapper::loadKeyboardLayout(const std::shared_ptr<app::resource::Handle>& handle)
{
	assert(handle != nullptr);

	const Json::Value& documentRoot = handle->getExtraData<app::resource::JsonExtraData>()->getRootValue();

	const Json::Value& buttons = documentRoot["vectorControls"]["buttons"];
	std::vector<std::string> buttonNames = buttons.getMemberNames();
	for (const std::string& buttonName : buttonNames)
	{
		SDL_Scancode buttonScanCode = SDL_GetScancodeFromName(buttonName.c_str());

		const Json::Value& buttonsJson = buttons[buttonName];

		for (const Json::Value& directionActionJson : buttonsJson)
		{
			DirectionAction directionAction;

			directionAction.action = directionActionJson["action"].asString();
			directionAction.direction = util::parseJsonVec(directionActionJson["vector"], glm::vec2(0.0f, 0.0f));

			this->directionMapping[buttonScanCode].push_back(directionAction);

		}
	}

	const Json::Value& analogueActions = documentRoot.get("strengthControls", Json::objectValue).get("actions", Json::arrayValue);

	for (const Json::Value& action : analogueActions)
	{
		std::string actionName = action.get("name", "").asString();
		const Json::Value& keyArray = action.get("keys",Json::arrayValue);

		for (const Json::Value& key : keyArray)
		{
			this->analogueMapping[SDL_GetScancodeFromName(key.asCString())].push_back(actionName);
		}
	}

	const Json::Value& switchActions = documentRoot.get("switchControls", Json::objectValue).get("actions", Json::arrayValue);

	for (const Json::Value& action : switchActions)
	{
		std::string actionName = action.get("name", "").asString();
		const Json::Value& keyArray = action.get("keys",Json::arrayValue);

		for (const Json::Value& key : keyArray)
		{
			this->switchMapping[SDL_GetScancodeFromName(key.asCString())].push_back(actionName);
		}
	}

	const Json::Value& toggleActions = documentRoot.get("toggleControls", Json::objectValue).get("actions", Json::arrayValue);

	for (const Json::Value& action : toggleActions)
	{
		std::string actionName = action.get("name", "").asString();
		const Json::Value& keyArray = action.get("keys",Json::arrayValue);

		for (const Json::Value& key : keyArray)
		{
			this->toggleMapping[SDL_GetScancodeFromName(key.asCString())].push_back(actionName);
		}
	}
}

void SdlKeyboardControlMapper::setControlledActor(logic::actor::Actor* actor)
{
	this->controlledActor = actor;
}

bool SdlKeyboardControlMapper::hasControlledActor() const
{
	return this->controlledActor != nullptr;
}

SdlKeyboardControlMapper::ControlEventContainer SdlKeyboardControlMapper::mapKeyEvent(const SDL_Scancode& keysym)
{
	ControlEventContainer controlEvents;

	if (this->directionMapping.empty() == false)
	{
		this->mapMovementEvent(keysym, controlEvents);
	}

	if (this->analogueMapping.empty() == false)
	{
		const auto& it = this->analogueMapping.find(keysym);
		if(it != this->analogueMapping.end())
		{
			for (const std::string& actionName : it->second)
			{
				controlEvents.push_back(this->mapSwitchEvent(keysym, actionName));
			}
		}
	}

	if (this->switchMapping.empty() == false)
	{
		const auto& it = this->switchMapping.find(keysym);
		if(it != this->switchMapping.end())
		{
			for (const std::string& actionName : it->second)
			{
				controlEvents.push_back(this->mapSwitchEvent(keysym, actionName));
			}
		}
	}

	if (this->toggleMapping.empty() == false)
	{
		const auto& it = this->toggleMapping.find(keysym);
		if(it != this->toggleMapping.end() && this->keyPressed[keysym] == true)
		{
			for (const std::string& actionName : it->second)
			{
				controlEvents.push_back(std::make_shared<logic::control::Action>(this->controlledActor, actionName));
			}
		}
	}

	return controlEvents;
}

void SdlKeyboardControlMapper::mapMovementEvent(const SDL_Scancode& keysym, ControlEventContainer& controlEvents)
{
	const auto directionIt = this->directionMapping.find(keysym);
	if (directionIt != this->directionMapping.end())
	{
		for (const DirectionAction& directionAction : directionIt->second)
		{
			std::map<SDL_Scancode, glm::vec2>& actionDirectionMap = this->directionActionStates[directionAction.action];

			if (this->keyPressed[keysym] == true)
			{
				actionDirectionMap[keysym] = directionAction.direction;
			}
			else
			{
				actionDirectionMap[keysym] = glm::vec2(0.0f, 0.0f);
			}

			glm::vec2 eventDirection = glm::vec2(0.0f, 0.0f);

			for (const auto& it : actionDirectionMap)
			{
				eventDirection += it.second;
			}

			if (glm::length(eventDirection) > 1.0f)
			{
				eventDirection = glm::normalize(eventDirection);
			}

			auto controlEvent = std::make_shared<logic::control::Action>(this->controlledActor, directionAction.action, eventDirection);

			controlEvents.push_back(controlEvent);
		}
	}
}


std::shared_ptr<logic::control::Action> SdlKeyboardControlMapper::mapSwitchEvent(const SDL_Scancode& keysym, const std::string& controlName)
{
	bool switchOn = this->keyPressed[keysym];

	auto controlEvent = std::make_shared<logic::control::Action>(this->controlledActor, controlName, switchOn);
	return controlEvent;
}

SdlKeyboardControlMapper::ControlEventContainer SdlKeyboardControlMapper::mapKeyPress(const SDL_Keysym& keysym)
{
	if (this->keyPressed[keysym.scancode] == false)
	{
		this->keyPressed[keysym.scancode] = true;
		return this->mapKeyEvent(keysym.scancode);
	}
	else
	{
		return {};
	}
}

SdlKeyboardControlMapper::ControlEventContainer SdlKeyboardControlMapper::mapKeyRelease(const SDL_Keysym& keysym)
{
	if (this->keyPressed[keysym.scancode] == true)
	{
		this->keyPressed[keysym.scancode] = false;
		return this->mapKeyEvent(keysym.scancode);
	}
	else
	{
		return {};
	}
}

SdlKeyboardControlMapper::ControlEventContainer SdlKeyboardControlMapper::mapAllKeysReleased()
{
	ControlEventContainer events;

	for (auto& pressedKey : this->keyPressed)
	{
		if (pressedKey.second == true)
		{
			pressedKey.second = false;

			const auto keyEvents = this->mapKeyEvent(pressedKey.first);
			std::move(keyEvents.begin(), keyEvents.end(), std::back_inserter(events));
		}
	}

	return events;
}

} }
