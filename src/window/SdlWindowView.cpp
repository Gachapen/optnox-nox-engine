/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/IContext.h>
#include <nox/window/SdlWindowView.h>
#include <SDL2/SDL.h>
#include <glm/gtx/string_cast.hpp>
#include <string>
#include <cassert>

namespace nox
{
namespace window
{

SdlWindowView::SdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, const bool enableOpenGl):
	applicationContext(applicationContext),
	windowTitle(windowTitle),
	windowSize(800, 600),
	enableOpengl(enableOpenGl),
	window(nullptr),
	glContext(nullptr)
{
	assert(this->applicationContext != nullptr);

	this->log = this->applicationContext->createLogger();
	this->log.setName("SdlWindowView");
}

bool SdlWindowView::initialize(logic::IContext* /*context*/)
{
	Uint32 windowFlags = SDL_WINDOW_RESIZABLE;
	if (this->enableOpengl == true)
	{
		windowFlags |= SDL_WINDOW_OPENGL;
	}

	this->window = SDL_CreateWindow(
		this->windowTitle.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		static_cast<int>(this->windowSize.x),
		static_cast<int>(this->windowSize.y),
		windowFlags);

	if (this->window == nullptr)
	{
		this->log.fatal().format("Failed creating window: %s", SDL_GetError());
		return false;
	}
	else
	{
		this->log.verbose().raw("SDL window created.");
	}

	if (this->enableOpengl == true)
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		this->glContext = SDL_GL_CreateContext(this->window);

		if (this->glContext == nullptr)
		{
			this->log.fatal().format("Failed creating OpenGL context: %s", SDL_GetError());
			return false;
		}
		else
		{
			this->log.verbose().raw("SDL OpenGL context created.");
		}
	}

	if (this->onWindowCreated(this->window) == false)
	{
		return false;
	}

	this->onWindowSizeChanged(this->windowSize);

	return true;
}

void SdlWindowView::destroy()
{
	this->onDestroy();

	if (this->glContext != nullptr)
	{
		SDL_GL_DeleteContext(this->glContext);
		this->glContext = nullptr;
		this->log.verbose().raw("Destroyed SDL OpenGL context.");
	}

	if (this->window != nullptr)
	{
		SDL_DestroyWindow(this->window);
		this->window = nullptr;
		this->log.verbose().raw("Destroyed SDL window.");
	}

	this->log.verbose().raw("Quit SDL video and events subsystems.");
}

void SdlWindowView::update(const Duration& deltaTime)
{
	this->onUpdate(deltaTime);
}

void SdlWindowView::setWindowSize(const glm::uvec2& size)
{
	this->windowSize = size;

	if (this->window != nullptr)
	{
		SDL_SetWindowSize(this->window, static_cast<int>(size.x), static_cast<int>(size.y));
	}
}

const glm::uvec2& SdlWindowView::getWindowSize() const
{
	return this->windowSize;
}

void SdlWindowView::onUpdate(const Duration& /*deltaTime*/)
{
}

void SdlWindowView::onSdlEvent(const SDL_Event& event)
{
	const auto windowId = SDL_GetWindowID(this->window);

	if (event.type == SDL_EventType::SDL_WINDOWEVENT)
	{
		if (event.window.event == SDL_WindowEventID::SDL_WINDOWEVENT_RESIZED)
		{
			if (event.window.data1 >= 0 && event.window.data2 >= 0)
			{
				this->windowSize.x = static_cast<unsigned int>(event.window.data1);
				this->windowSize.y = static_cast<unsigned int>(event.window.data2);

				this->log.debug().format("Window resized: %s", glm::to_string(this->windowSize).c_str());
				this->onWindowSizeChanged(this->windowSize);
			}
			else
			{
				this->log.error().format("Invalid window size from resize event, ignoring: (%d, %d)", event.window.data1, event.window.data2);
			}
		}
	}
	else if (event.type == SDL_EventType::SDL_MOUSEBUTTONDOWN && event.button.windowID == windowId)
	{
		this->onMousePress(event.button);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEBUTTONUP && event.button.windowID == windowId)
	{
		this->onMouseRelease(event.button);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEMOTION && event.motion.windowID == windowId)
	{
		this->onMouseMove(event.motion);
	}
	else if (event.type == SDL_EventType::SDL_MOUSEWHEEL && event.wheel.windowID == windowId)
	{
		this->onMouseScroll(event.wheel);
	}
	else if (event.type == SDL_EventType::SDL_KEYDOWN && event.key.windowID == windowId)
	{
		this->onKeyPress(event.key);
	}
	else if (event.type == SDL_EventType::SDL_KEYUP && event.key.windowID == windowId)
	{
		this->onKeyRelease(event.key);
	}
}

void SdlWindowView::onWindowSizeChanged(const glm::uvec2& /*size*/)
{
}

void SdlWindowView::onMousePress(const SDL_MouseButtonEvent& /*event*/)
{
}

void SdlWindowView::onMouseRelease(const SDL_MouseButtonEvent& /*event*/)
{
}

void SdlWindowView::onMouseMove(const SDL_MouseMotionEvent& /*event*/)
{
}

void SdlWindowView::onKeyPress(const SDL_KeyboardEvent& /*event*/)
{
}

void SdlWindowView::onKeyRelease(const SDL_KeyboardEvent& /*event*/)
{
}

void SdlWindowView::onMouseScroll(const SDL_MouseWheelEvent& /*event*/)
{
}

void SdlWindowView::onDestroy()
{
}

bool SdlWindowView::onWindowCreated(SDL_Window* /*window*/)
{
	return true;
}

std::string SdlWindowView::getTypeString() const
{
	return "SdlWindowView";
}

}
}
