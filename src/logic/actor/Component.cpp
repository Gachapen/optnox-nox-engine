/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/Component.h>
#include <nox/app/net/Packet.h>

#include <sstream>
#include <cassert>

namespace nox { namespace logic { namespace actor
{

Component::Component():
	owner(nullptr),
	logicContext(nullptr)
{
}

Component::~Component() = default;

void Component::setContext(IContext* logicContext)
{
	assert(logicContext != nullptr);

	this->logicContext = logicContext;

	this->log = logicContext->createLogger();

	std::ostringstream logNameStream;
	logNameStream << "Actor " << this->getName();

	this->log.setName(logNameStream.str());
}

void Component::setOwner(Actor* owner)
{
	this->owner = owner;

	if (this->owner != nullptr)
	{
		std::ostringstream logNameStream;
		logNameStream << "Actor " << this->owner->getId() << " '" << this->owner->getName() << "' " << this->getName();

		this->log.setName(logNameStream.str());
	}
}

void Component::onCreate()
{
}

void Component::onComponentAttached(Component* /*component*/)
{
}

void Component::onUpdate(const Duration& /*deltaTime*/)
{
}

void Component::onComponentEvent(const std::shared_ptr<event::Event>& /*event*/)
{
}

void Component::onDestroy()
{
}

void Component::onActivate()
{
}

void Component::onDeactivate()
{
}

bool Component::isActive() const
{
	return this->getOwner()->isActive();
}

std::vector<Component::IdType> Component::findInheritedNames() const
{
	return {};
}

void Component::onChildActorAttached(Actor* /*childActor*/, const std::string& /*childName*/)
{
}

void Component::onChildActorDetached(Actor* /*childActor*/, const std::string& /*childName*/)
{
}

void Component::serialize(nox::app::net::Packet &packet)
{
	this->log.warning().format("Component '%s' has not overwritten "
			"void Component::serialize(nox::app::net::Packet&)",
			this->getName().c_str());
}

void Component::deSerialize(const nox::app::net::Packet &packet)
{
	this->log.warning().format("Component '%s' has not overwritten "
			"void Component::deSerialize(const nox::app::net::Packet&)",
			this->getName().c_str());
}

}
} }
