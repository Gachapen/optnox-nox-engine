#include <nox/logic/actor/SyncAttributes.h>


namespace nox { namespace logic { namespace actor
{

const app::net::LagCompensation SyncAttributes::DEFAULT_LAG_COMP = app::net::LagCompensation::NONE;
const std::string SyncAttributes::DEFAULT_DESTROY_HANDLER = "destroy";

SyncAttributes::SyncAttributes(bool local, ClientId owner, SyncId sync):
	lagCompensation(DEFAULT_LAG_COMP),
	ownedLocally(local),
	ownerClient(owner),
	syncId(sync),
	destroyHandler(DEFAULT_DESTROY_HANDLER)
{

}

void SyncAttributes::addSyncComponent(const Component::IdType &idType)
{
	this->syncComponents.insert(idType);
}

void SyncAttributes::setLagCompensation(app::net::LagCompensation lag)
{
	this->lagCompensation = lag;
}

void SyncAttributes::setDestroyHandlerName(std::string handlerName)
{
	this->destroyHandler = handlerName;
}

const std::set<Component::IdType>& SyncAttributes::getSyncComponents() const
{
	return this->syncComponents;
}

app::net::LagCompensation SyncAttributes::getLagCompensation() const
{
	return this->lagCompensation;
}

bool SyncAttributes::getOwnedLocally() const
{
	return this->ownedLocally;
}

ClientId SyncAttributes::getOwnerClientId() const
{
	return this->ownerClient;
}

SyncId SyncAttributes::getSyncId() const
{
	return this->syncId;
}

std::string SyncAttributes::getDestroyHandlerName() const
{
	return this->destroyHandler;
}

}
} }
