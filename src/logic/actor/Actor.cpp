/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/SyncAttributes.h>
#include <nox/app/net/component/SyncOut.h>
#include <nox/app/net/component/SyncIn.h>

#include <algorithm>
#include <sstream>

namespace nox { namespace logic { namespace actor
{

bool g_actorDesyncSafety = false;

Actor::Actor(IContext* context, Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom, app::log::Logger logger):
	log(std::move(logger)),
	context(context),
	parentActor(nullptr),
	name(name),
	definitionName(definitionName),
	extendedFrom(extendedFrom),
	id(id),
	trivial(false),
	created(false),
	active(false),
	forcedActiveState(ForcedActiveState::NONE),
	synchronized(false),
	ownedLocally(false),
	ownerClientId(0),
	syncId(0)
{
	std::ostringstream logNameStream;
	logNameStream << "Actor " << this->id << " '" << this->name << "'";

	this->log.setName(logNameStream.str());
}

Actor::Actor(IContext* context, Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom):
	Actor(context, id, name, definitionName, extendedFrom, app::log::Logger())
{
}


Actor::Actor(IContext* context, Identifier id, const std::string& name, app::log::Logger logger):
	Actor(context, id, name, "", {}, std::move(logger))
{
}

Actor::Actor(IContext* context, Identifier id, const std::string& name):
	Actor(context, id, name, app::log::Logger())
{
}

bool Actor::addComponent(std::unique_ptr<Component>& component)
{
	std::vector<std::string> names = component->findInheritedNames();
	names.push_back(component->getName());

	const bool duplicate = std::any_of(
			names.begin(),
			names.end(),
			[this](std::string& name)
			{
				return (this->componentMap.find(name) != this->componentMap.end());
			}
	);

	if (duplicate == true)
	{
		return false;
	}
	else
	{
		for (std::string& name : names)
		{
			this->componentMap[name] = component.get();
		}

		component->setOwner(this);

		for (std::unique_ptr<Component>& attachedComponent : this->components)
		{
			attachedComponent->onComponentAttached(component.get());
			component->onComponentAttached(attachedComponent.get());
		}

		if (this->isCreated() == true)
		{
			component->onCreate();
		}

		if (this->isActive() == true)
		{
			component->onActivate();
		}

		this->components.push_back(std::move(component));

		return true;
	}
}

bool Actor::attachChildActor(std::unique_ptr<Actor> actor, const std::string& actorName)
{
	const auto childIt = this->childActorMap.find(actorName);

	if (childIt != this->childActorMap.end())
	{
		this->log.error().format("Tried to attach child actor with name \"%s\" that already is mapped.", actorName.c_str());
		return false;
	}

	this->childActorMap[actorName] = actor.get();

	actor->parentActor = this;

	if (this->created == true && actor->created == false)
	{
		actor->create();
	}
	else if (this->created == false && actor->created == true)
	{
		actor->destroy();
	}

	if (this->forcedActiveState != actor->forcedActiveState)
	{
		actor->forceActiveState(this->forcedActiveState);
	}

	for (const auto& component : this->components)
	{
		component->onChildActorAttached(actor.get(), actorName);
	}

	const std::vector<Child> grandChildren = actor->findChildrenRecursively(actorName);
	for (const Child& child : grandChildren)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorAttached(child.actor, child.name);
		}
	}

	this->childActors.push_back(std::move(actor));

	return true;
}

std::unique_ptr<Actor> Actor::detachChildActor(const std::string& actorName)
{
	const auto childIt = this->childActorMap.find(actorName);

	if (childIt == this->childActorMap.end())
	{
		this->log.error().format("Tried to detach child actor with name \"%s\" that is not mapped.", actorName.c_str());
		return nullptr;
	}

	const Actor* actorPointer = childIt->second;
	const std::string childName = childIt->first;
	
	this->childActorMap.erase(childIt);

	auto actorIt = std::find_if(
			this->childActors.begin(),
			this->childActors.end(),
			[actorPointer] (const std::unique_ptr<Actor>& actor)
			{
				return (actor.get() == actorPointer);
			}
	);

	if (actorIt == this->childActors.end())
	{
		this->log.error().format("Tried detaching child actor \"%s\" that was mapped, but not stored.", actorName.c_str());
		return nullptr;
	}

	for (const auto& component : this->components)
	{
		component->onChildActorDetached(actorIt->get(), actorName);
	}

	const std::vector<Child> grandChildren = (*actorIt)->findChildrenRecursively(childName);
	for (const Child& actor : grandChildren)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorDetached(actor.actor, actor.name);
		}
	}

	std::unique_ptr<Actor> actor = std::move(*actorIt);
	actor->parentActor = nullptr;

	this->childActors.erase(actorIt);

	return std::move(actor);
}

std::vector<std::unique_ptr<Actor>> Actor::detachAllChildActors()
{
	for (const auto& childPair : this->childActorMap)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorDetached(childPair.second, childPair.first);
		}

		const std::vector<Child> grandChildren = childPair.second->findChildrenRecursively(childPair.first);
		for (const Child& actor : grandChildren)
		{
			for (const auto& component : this->components)
			{
				component->onChildActorDetached(actor.actor, actor.name);
			}
		}

		childPair.second->parentActor = nullptr;
	}

	this->childActorMap.clear();

	std::vector<std::unique_ptr<Actor>> detachedActors = std::move(this->childActors);
	this->childActors.clear();

	return std::move(detachedActors);
}

bool Actor::isParentTo(const Actor* childActor) const
{
	const auto childIt = std::find_if(
			this->childActors.begin(),
			this->childActors.end(),
			[childActor](const std::unique_ptr<Actor>& realChildActor)
			{
				return (realChildActor.get() == childActor);
			}
	);

	return (childIt != this->childActors.end());
}

bool Actor::isExtendedFrom(const std::string& definitionName) const
{
	if (this->definitionName == definitionName)
	{
		return true;
	}
	else
	{
		return (std::find(this->extendedFrom.begin(), extendedFrom.end(), definitionName) != this->extendedFrom.end());
	}
}

const std::vector<std::string>& Actor::getDefinitionsExtendedFrom() const
{
	return this->extendedFrom;
}

bool Actor::create()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == false && this->hasForcedActiveState() == false)
	{
		for (auto& childActor : this->childActors)
		{
			childActor->create();
		}
	
		for (auto& component : this->components)
		{
			component->onCreate();
		}

		std::lock_guard<std::mutex> eventQueueLock(this->queuedEventMutex);

		this->broadcastQueuedComponentEvents();

		this->created = true;

		this->log.debug().raw("Created.");

		return true;
	}
	else
	{
		return false;
	}
}

void Actor::update(const Duration& deltaTime)
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	for (auto& component : this->components)
	{
		component->onUpdate(deltaTime);
	}

	for (auto& childActor : this->childActors)
	{
		childActor->update(deltaTime);
	}
}

Component* Actor::findComponent(std::string name) const
{
	auto iter = this->componentMap.find(name);
	if (iter == this->componentMap.end())
	{
		return nullptr;
	}

	return iter->second;
}

void Actor::broadCastComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (this->isCreated() == true)
	{
		for (auto& component : this->components)
		{
			component->onComponentEvent(event);
		}
	}
	else
	{
		std::lock_guard<std::mutex> eventQueueLock(this->queuedEventMutex);

		if (this->isCreated() == true)
		{
			for (auto& component : this->components)
			{
				component->onComponentEvent(event);
			}
		}
		else
		{
			this->queuedComponentEvents.push(event);
		}
	}
}

void Actor::serialize(Json::Value& jsonObject, const bool extendDefinition) const
{
	jsonObject["components"] = Json::arrayValue;

	if (extendDefinition == true && this->definitionName.empty() == false)
	{
		jsonObject["extend"] = this->definitionName;
	}
	else if (this->extendedFrom.empty() == false)
	{
		jsonObject["extend"] = this->extendedFrom.front();
	}

	if (this->name.empty() == false)
	{
		jsonObject["name"] = this->name;
	}

	if (this->trivial == true)
	{
		jsonObject["trivial"] = this->trivial;
	}

	unsigned int componentNumber = 0;
	for (const auto& component : this->components)
	{
		component->serialize(jsonObject["components"][componentNumber]);
		componentNumber++;
	}

	if (this->childActorMap.empty() == false)
	{
		Json::Value& childActorsJson = jsonObject["childActors"];

		for (const auto childActorPair : this->childActorMap)
		{
			childActorPair.second->serialize(childActorsJson[childActorPair.first], extendDefinition);
		}
	}
}

bool Actor::destroy()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->isActive() == false && this->hasForcedActiveState() == false)
	{
		for (auto& component : this->components)
		{
			component->onDestroy();
		}

		for (auto& childActor : this->childActors)
		{
			childActor->destroy();
		}

		this->created = false;

		this->log.debug().raw("Destroyed.");

		return true;
	}
	else
	{
		return false;
	}
}

void Actor::activate()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->active == false)
	{
		this->active = true;

		if (this->forcedActiveState == ForcedActiveState::NONE)
		{
			for (auto& component : this->components)
			{
				component->onActivate();
			}

			this->log.debug().raw("Activated.");

			if (this->context != nullptr)
			{
//				const auto event = std::make_shared<ActorActiveStateChange>(this, this->active);
//				this->context->getEventManager()->queueEvent(event);
			}
		}
	}
}

void Actor::deactivate()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->active == true)
	{
		this->active = false;

		if (this->forcedActiveState == ForcedActiveState::NONE)
		{
			for (auto& component : this->components)
			{
				component->onDeactivate();
			}

			this->log.debug().raw("Deactivated.");

			if (this->context != nullptr)
			{
//				const auto event = std::make_shared<ActorActiveStateChange>(this, this->active);
//				this->context->getEventManager()->queueEvent(event);
			}
		}
	}
}

void Actor::forceActiveState(const ForcedActiveState state)
{
	if (this->forcedActiveState != state)
	{
		bool activeStateChanged = false;
		bool activated = false;

		if (state == ForcedActiveState::ACTIVE && this->active == false)
		{
			activeStateChanged = true;
			activated = true;

			if (this->created == false)
			{
				for (auto& component : this->components)
				{
					component->onCreate();
				}

				std::lock_guard<std::mutex> eventQueueLock(this->queuedEventMutex);

				this->broadcastQueuedComponentEvents();
			}

			for (auto& component : this->components)
			{
				component->onActivate();
			}
		}
		else if (state == ForcedActiveState::INACTIVE && this->active == true)
		{
			activeStateChanged = true;
			activated = false;

			for (auto& component : this->components)
			{
				component->onDeactivate();
			}
		}
		else if (this->forcedActiveState == ForcedActiveState::ACTIVE && this->active == false)
		{
			activeStateChanged = true;
			activated = false;

			for (auto& component : this->components)
			{
				component->onDeactivate();
			}

			if (this->created == false)
			{
				for (auto& component : this->components)
				{
					component->onDestroy();
				}
			}
		}
		else if (this->forcedActiveState == ForcedActiveState::INACTIVE && this->active == true)
		{
			activeStateChanged = true;
			activated = true;

			for (auto& component : this->components)
			{
				component->onActivate();
			}
		}

		if (activeStateChanged == true)
		{
			if (this->context != nullptr)
			{
//				const auto event = std::make_shared<ActorActiveStateChange>(this, activated);
//				this->context->getEventManager()->queueEvent(event);
			}
		}

		this->forcedActiveState = state;

		for (auto& childActor : this->childActors)
		{
			childActor->forceActiveState(this->forcedActiveState);
		}
	}
}

void Actor::setName(const std::string& name)
{
	this->name = name;
}

void Actor::makeTrivial(const bool trivial)
{
	this->trivial = trivial;
}

Actor* Actor::findChildActor(const std::string& actorName) const
{
	const std::string::size_type childNameEndPos = actorName.find('.');

	if (childNameEndPos == std::string::npos)
	{
		auto actorIt = this->childActorMap.find(actorName);
		if (actorIt != this->childActorMap.end())
		{
			return actorIt->second;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		const std::string childName = actorName.substr(0, childNameEndPos);

		auto actorIt = this->childActorMap.find(childName);
		if (actorIt != this->childActorMap.end())
		{
			const std::string::size_type grandChildNameStartPos = childNameEndPos + 1;
			const std::string grandChildName = actorName.substr(grandChildNameStartPos);

			return actorIt->second->findChildActor(grandChildName);
		}
		else
		{
			return nullptr;
		}
	}
}

std::vector<Actor::Child> Actor::findChildrenRecursively() const
{
	std::vector<Child> children;

	for (const auto& child : this->childActorMap)
	{
		children.push_back(Child(child.first, child.second));

		const std::vector<Child> grandChildren = child.second->findChildrenRecursively(child.first);
		children.insert(children.begin(), grandChildren.begin(), grandChildren.end());
	}

	return children;
}

std::vector<Actor::Child> Actor::findChildrenRecursively(const std::string& childName) const
{
	std::vector<Child> children;
	for (const auto& child : this->childActorMap)
	{
		const std::string grandChildName = childName + "." + child.first;

		children.push_back(Child(grandChildName, child.second));

		const std::vector<Child> grandChildren = child.second->findChildrenRecursively(grandChildName);
		children.insert(children.begin(), grandChildren.begin(), grandChildren.end());
	}

	return children;
}

void Actor::setSynchronizedAttributes(const SyncAttributes& attr)
{
	this->synchronized = true;
	this->ownedLocally = attr.getOwnedLocally();
	this->ownerClientId = attr.getOwnerClientId();
	this->synchronizedComponents = attr.getSyncComponents();
	this->syncId = attr.getSyncId();
	this->destroyHandler = attr.getDestroyHandlerName();
}

const std::set<Component::IdType>& Actor::getSynchronizedComponents() const
{
	return this->synchronizedComponents;
}

bool Actor::isSynchronized() const
{
	return this->synchronized;
}

bool Actor::isOwnedLocally() const
{
	return this->ownedLocally;
}

ClientId Actor::getOwnerClientId() const
{
	return this->ownerClientId;
}

SyncId Actor::getSyncId() const
{
	return this->syncId;
}

std::string Actor::getDestroyHandlerName() const
{
	return this->destroyHandler;
}

void Actor::desynchronize()
{
	if (!this->synchronized)
	{
		return;
	}

	if (g_actorDesyncSafety == false)
	{
		this->log.debug().format("ACTOR DESYNCHRONIZED OUTSIDE OF A DESTROY HANDLER! "
				"name='%s' defName='%s' owner='%u'",
				this->name.c_str(), this->definitionName.c_str(), this->ownerClientId);
	}

	this->synchronized = false;

	this->removeComponent(app::net::SyncOut::NAME);
	this->removeComponent(app::net::SyncIn::NAME);
}

void Actor::broadcastQueuedComponentEvents()
{
	while (this->queuedComponentEvents.empty() == false)
	{
		const std::shared_ptr<event::Event>& event = this->queuedComponentEvents.front();

		if (event != nullptr)
		{
			for (auto& component : this->components)
			{
				component->onComponentEvent(event);
			}
		}

		this->queuedComponentEvents.pop();
	}
}

bool Actor::removeComponent(const Component::IdType& compName)
{
	if (this->componentMap.count(compName) != 0)
	{
		this->componentMap.erase(compName);

		for (auto it = this->components.begin(); it != this->components.end(); it++)
		{
			if ((*it)->getName() == compName)
			{
				std::unique_ptr<Component> uptr = std::move(*it);
				uptr->onDestroy();
				uptr.release();

				this->components.erase(it);
				return true;
			}
		}
	}

	return false;
}

} } }
