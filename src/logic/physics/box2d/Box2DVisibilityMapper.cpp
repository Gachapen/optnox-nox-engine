#include <nox/logic/physics/box2d/Box2DVisibilityMapper.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>


namespace nox { namespace logic { namespace physics
{

Box2DVisibilityMapper::Box2DVisibilityMapper(Box2DSimulation *sim):
	simulation(sim)
{

}

std::vector<std::array<glm::vec2,3>> Box2DVisibilityMapper::getInvisibilityRegions(glm::vec2 pos, float radius)
{
	std::vector<std::array<glm::vec2,3>> triangles;

	math::Box<glm::vec2> aabb;
	aabb.setLowerBound(glm::vec2(pos.x - radius, pos.y - radius));
	aabb.setUpperBound(glm::vec2(pos.x + radius, pos.y + radius));

	auto fixtures = this->simulation->getFixturesInAabb(aabb);

	for (const b2Fixture *fix : fixtures)
	{
		const b2Shape *shape = fix->GetShape();

		b2Shape::Type type = shape->GetType();
		if (type == b2Shape::e_polygon)
		{
			b2PolygonShape *polyShape = (b2PolygonShape*)shape;

			auto verts = this->getShapeVertices(polyShape);
			const int vertCount = verts.size();

			std::vector<std::pair<glm::vec2,glm::vec2>> edges;
			edges.reserve(vertCount/2 + 1);

			for (int i=0; i<vertCount; i++)
			{
				glm::vec2 vertA = verts[(!i ? verts.size() - 1 : i)];
				glm::vec2 vertB = verts[(i+1 >= verts.size() ? 0 : i)];

				// The point should be on the LEFT side of the line, as the
				// polygon is wound CCW.
				// http://stackoverflow.com/a/3461533
				float cross = (vertB.x - vertA.x)*(pos.y - vertA.y) - (vertB.y - vertA.y)*(pos.x - vertA.x);
				if (cross >= 0.f)
				{
					std::pair<glm::vec2,glm::vec2> pair(vertA, vertB);
					edges.push_back(pair);
				}
			}

			// Project all shadow casting edges
			for (auto pair : edges)
			{
				glm::vec2 diff;
				float length = 0.f;

				glm::vec2 vertA = pair.first;
				glm::vec2 vertB = pair.second;
				glm::vec2 vertC;
				glm::vec2 vertD;

				// Project A along (A-pos) to C
				diff = vertA - pos;
				length = diff.length();
				diff.x /= length;
				diff.y /= length;
				vertC = (radius * diff) + vertA;

				// Project B along (B-pos) to D
				diff = vertB - pos;
				length = diff.length();
				diff.x /= length;
				diff.y /= length;
				vertD = (radius * diff) + vertB;

				// Create two triangles, (A,B,C) and (B,C,D)
				std::array<glm::vec2,3> tri;

				tri[0] = glm::vec2(vertA.x, vertA.y);
				tri[1] = glm::vec2(vertB.x, vertB.y);
				tri[2] = glm::vec2(vertC.x, vertC.y);
				triangles.push_back(tri);

				tri[0] = glm::vec2(vertB.x, vertB.y);
				tri[1] = glm::vec2(vertC.x, vertC.y);
				tri[2] = glm::vec2(vertD.x, vertD.y);
				triangles.push_back(tri);
			}
		}
		else
		{
			// FIXME
			// Other fixture types - circles, etc, should be handled as well.
		}
	}

	return triangles;
}

std::vector<glm::vec2> Box2DVisibilityMapper::getShapeVertices(const b2PolygonShape* polyShape)
{
	const int vertCount = polyShape->GetVertexCount();

	// Build a list containing all the verts of the shape
	std::vector<glm::vec2> verts;
	verts.reserve(vertCount);

	for (int i=0; i<vertCount; i++)
	{
		b2Vec2 vert = polyShape->GetVertex(i);
		verts.push_back(glm::vec2(vert.x, vert.y));
	}

	return verts;

}

}
} }
