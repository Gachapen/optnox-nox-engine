#include <nox/logic/graphics/event/SceneNodeEdited.h>

namespace nox { namespace logic { namespace graphics
{

SceneNodeEdited::IdType SceneNodeEdited::ID = "nox.graphics.scene_node_edit";

SceneNodeEdited::SceneNodeEdited(const std::shared_ptr<app::graphics::SceneGraphNode>& sceneNode, Action action):
	Event(ID),
	sceneNode(sceneNode),
	action(action)
{
}

const std::shared_ptr<app::graphics::SceneGraphNode>& SceneNodeEdited::getSceneNode() const
{
	return this->sceneNode;
}

SceneNodeEdited::Action SceneNodeEdited::getEditAction() const
{
	return this->action;
}

} } }
