/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl/RenderData.h>

namespace nox { namespace app
{
namespace graphics
{

RenderData::RenderData(GLuint glMajor, GLuint glMinor, const TextureManager* textureManager):
	currentlyBoundVao(0),
	currentlyBoundShaderProgram(0),
	currentActiveTexture(GL_TEXTURE0),
	currentGlMajor(glMajor),
	currentGlMinor(glMinor),
	stencilFailOperation(GL_KEEP),
	stencilMask(0xFF),
	textureManager(textureManager)
{
}

RenderData::RenderData():
	RenderData(0, 0, nullptr)
{
}

GLuint RenderData::bindVertexArray(GLuint vao)
{
	GLuint previous = this->currentlyBoundVao;

	if (this->currentlyBoundVao != vao)
	{
		glBindVertexArray(vao);
		this->currentlyBoundVao = vao;
	}

	return previous;
}

GLuint RenderData::bindBuffer(GLenum target, GLuint vbo)
{
	GLuint previous = this->currentlyBoundVbo[target];

	if (this->currentlyBoundVbo[target] != vbo)
	{
		glBindBuffer(target, vbo);
		this->currentlyBoundVbo[target] = vbo;
	}

	return previous;
}

GLuint RenderData::bindShaderProgram(GLuint shaderProgram)
{
	GLuint previous = this->currentlyBoundShaderProgram;

	if (this->currentlyBoundShaderProgram != shaderProgram)
	{
		glUseProgram(shaderProgram);
		this->currentlyBoundShaderProgram = shaderProgram;
	}

	return previous;
}

GLuint RenderData::bindTexture(GLenum target, GLuint texture, GLenum texUnit)
{
	const std::pair<GLenum,GLenum> pair(target, texUnit);

	GLuint previous = this->currentlyBoundTexture[pair];

	if (this->currentActiveTexture != texUnit)
	{
		glActiveTexture(texUnit);
		this->currentActiveTexture = texUnit;
	}

	if (previous != texture)
	{
		glBindTexture(target, texture);
		this->currentlyBoundTexture[pair] = texture;
	}

	return previous;
}

void RenderData::setStencilFailOperation(GLenum operation)
{
	if (operation != this->stencilFailOperation)
	{
		glStencilOp(operation, GL_KEEP, GL_KEEP);
		this->stencilFailOperation = operation;
	}
}

void RenderData::setStencilMask(GLuint mask)
{
	if (mask != this->stencilMask)
	{
		glStencilMask(mask);
		this->stencilMask = mask;
	}
}

void RenderData::setStencilFunc(GLenum func, GLint ref, GLuint mask)
{
	StencilFunc otherFunc(func, ref, mask);

	if (otherFunc != this->stencilFunc)
	{
		glStencilFunc(func, ref, mask);
		this->stencilFunc = otherFunc;
	}
}

void RenderData::enable(GLenum state)
{
	if (this->enabledStates[state] == false)
	{
		glEnable(state);
		this->enabledStates[state] = true;
	}
}

void RenderData::disable(GLenum state)
{
	if (this->enabledStates[state] == true)
	{
		glDisable(state);
		this->enabledStates[state] = false;
	}
}

GLuint RenderData::getGlMajorVersion() const
{
	return this->currentGlMajor;
}

GLuint RenderData::getGlMinorVersion() const
{
	return this->currentGlMinor;
}

const TextureManager* RenderData::getTextureManager() const
{
	return this->textureManager;
}

GLuint RenderData::getBoundVao() const
{
	return this->currentlyBoundVao;
}

GLuint RenderData::getBoundVbo(GLenum target) const
{
	const auto targetIt = this->currentlyBoundVbo.find(target);

	if (targetIt != this->currentlyBoundVbo.end())
	{
		return targetIt->second;
	}
	else
	{
		return 0u;
	}
}

GLuint RenderData::getBoundShaderProgram() const
{
	return this->currentlyBoundShaderProgram;
}

GLuint RenderData::getBoundTexture(GLenum target, GLenum texUnit) const
{
	const auto targetIt = this->currentlyBoundTexture.find(std::pair<GLenum,GLenum>(target, texUnit));

	if (targetIt != this->currentlyBoundTexture.end())
	{
		return targetIt->second;
	}
	else
	{
		return 0u;
	}
}

RenderData::StencilFunc::StencilFunc():
	func(GL_ALWAYS),
	ref(0),
	mask(0xFF)
{
}

RenderData::StencilFunc::StencilFunc(GLenum func, GLint ref, GLuint mask):
	func(func),
	ref(ref),
	mask(mask)
{
}

bool RenderData::StencilFunc::operator!=(const StencilFunc& other) const
{
	return (this->func != other.func || this->ref != other.ref || this->mask != other.mask);
}

}
} }
