/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl/TextureManager.h>
#include <nox/app/graphics/TexturePackerTextureAtlasReader.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/common/platform.h>

#include <fstream>
#include <memory>
#include <json/reader.h>
#include <GL/glew.h>
#include <SDL2/SDL_image.h>

namespace nox { namespace app
{
namespace graphics
{

TextureManager::TextureManager():
	defaultTexelsPerMeter(70)
{
}

void TextureManager::setLogger(log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("TextureManager");
}

bool TextureManager::readGraphicsFile(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess)
{
	auto graphicsJsonResource = resourceAccess->getHandle(graphicsResourceDescriptor);

	if (graphicsJsonResource == nullptr)
	{
		this->log.error().format("Could not find texture atlas: %s", graphicsResourceDescriptor.getPath().c_str());
		return false;
	}

	auto graphicsJsonData = graphicsJsonResource->getExtraData<app::resource::JsonExtraData>();
	assert(graphicsJsonData != nullptr);

	const auto& graphicsJson = graphicsJsonData->getRootValue();

	const unsigned int texelsPerMeter = graphicsJson.get("texelsPerMeter", this->defaultTexelsPerMeter).asUInt();

	const auto& textureAtlasArray = graphicsJson["textureAtlases"];
	if (textureAtlasArray.isNull())
	{
		this->log.error().raw("JSON without texture atlas info.");
		return false;
	}
	else if (textureAtlasArray.isArray() == false)
	{
		this->log.error().raw("JSON atlas array was not an array.");
		return false;
	}

	const auto numAtlases = textureAtlasArray.size();
	this->textureAtlasFiles.reserve(numAtlases);

	for (const auto& atlasObject : textureAtlasArray)
	{
		TextureAtlas atlas;

		const auto& atlasName = atlasObject["name"];
		if (atlasName.isNull())
		{
			this->log.error().raw("JSON atlas without name.");
		}
		else
		{
			atlas.name = atlasName.asString();
			atlas.imageExtension = atlasObject.get("imageExtension", "png").asString();
			atlas.dataExtension = atlasObject.get("dataExtension", "json").asString();
			atlas.boundTextureBuffer = 0;
			atlas.boundNormalMapBuffer = 0;
			atlas.boundSpecularMapBuffer = 0;
			atlas.id = static_cast<unsigned int>(this->textureAtlasFiles.size());
			atlas.mipMap = atlasObject.get("mipMap", false).asBool();
			atlas.normalMapFile = atlasObject.get("normalMap", "").asString();
			atlas.specularMapFile = atlasObject.get("specularMap", "").asString();

			const resource::Descriptor atlasDescriptor(atlas.name + "." + atlas.dataExtension);
			auto atlasJsonResource = resourceAccess->getHandle(atlasDescriptor);

			if (atlasJsonResource == nullptr)
			{
				this->log.error().format("Could not find texture atlas: %s", atlasDescriptor.getPath().c_str());
			}
			else
			{
				auto atlasJsonData = atlasJsonResource->getExtraData<app::resource::JsonExtraData>();
				assert(atlasJsonData != nullptr);

				const auto& atlasJson = atlasJsonData->getRootValue();

				TexturePackerTextureAtlasReader atlasReader;

				if (atlasReader.readAtlasJson(atlasJson, atlas.id, texelsPerMeter) == true)
				{
					atlas.width = atlasReader.getAtlasWidth();
					atlas.height = atlasReader.getAtlasHeight();

					this->textureAtlasFiles.push_back(atlas);
					this->textureMap.insert(atlasReader.getTextureMap().begin(), atlasReader.getTextureMap().end());
				}
				else
				{
					this->log.error().format("Could not read atlas file \"%s\": %s", atlasDescriptor.getPath().c_str(), atlasReader.getErrorString().c_str());
				}
			}
		}
	}

	auto defaultTextureName = graphicsJson.get("defaultTexture", std::string()).asString();

	if (defaultTextureName.empty() == false)
	{
		auto textureIt = this->textureMap.find(defaultTextureName);

		if (textureIt != this->textureMap.end())
		{
			this->defaultQuad = textureIt->second;
		}
	}

	return true;
}

void TextureManager::writeToTextureBuffers(resource::IResourceAccess* resourceAccess)
{
	for (auto& atlas : this->textureAtlasFiles)
	{
		if (!this->writeTextureAtlasToBuffer(atlas, resourceAccess))
		{
			this->log.error().format("Failed to write texture atlas to OpenGL-buffer");
		}

		if (!this->writeNormalMapToBuffer(atlas, resourceAccess))
		{
			this->log.error().format("Failed to write normal map to OpenGL-buffer");
		}

		if (!this->writeSpecularMapToBuffer(atlas, resourceAccess))
		{
			this->log.error().format("Failed to write specular map to OpenGL-buffer");
		}
	}
}

bool TextureManager::writeTextureAtlasToBuffer(TextureAtlas& atlas, resource::IResourceAccess *resourceAccess)
{
	if (atlas.boundTextureBuffer != 0)
	{
		// Consider this a victory. The buffer has been written at some point.
		return true;
	}

	const auto imageResourceDescriptor = resource::Descriptor(atlas.name + "." + atlas.imageExtension);

	auto imageResource = resourceAccess->getHandle(imageResourceDescriptor);
	if (imageResource == nullptr)
	{
		this->log.error().format("Could not find texture atlas: %s", imageResourceDescriptor.getPath().c_str());
		return false;
	}

	atlas.boundTextureBuffer = this->writeToTextureBuffer(imageResource, atlas);
	return (bool)atlas.boundTextureBuffer;
}

bool TextureManager::writeNormalMapToBuffer(TextureAtlas& atlas, resource::IResourceAccess *resourceAccess)
{
	if (atlas.boundNormalMapBuffer != 0)
	{
		// The buffer has been filled at some point.
		return true;
	}

	if (atlas.normalMapFile.length() == 0)
	{
		// There is no normal map defined. This is not an error, as there is nothing to do.
		return true;
	}

	auto imageResourceDescriptor = resource::Descriptor(atlas.normalMapFile);

	auto imageResource = resourceAccess->getHandle(imageResourceDescriptor);
	if (imageResource == nullptr)
	{
		this->log.error().format("Could not find normal map file: %s", imageResourceDescriptor.getPath().c_str());
		return false;
	}

	atlas.boundNormalMapBuffer = this->writeToTextureBuffer(imageResource, atlas);
	return (bool)atlas.boundNormalMapBuffer;
}

bool TextureManager::writeSpecularMapToBuffer(TextureAtlas& atlas, resource::IResourceAccess* resourceAccess)
{
	if (atlas.boundSpecularMapBuffer != 0)
	{
		// It exists - everything is fine.
		return true;
	}

	if (atlas.specularMapFile.length() == 0)
	{
		// No file specified, nothing to do, nothing to fail at.
		return true;
	}

	auto imageResourceDescriptor = resource::Descriptor(atlas.specularMapFile);

	auto imageResource = resourceAccess->getHandle(imageResourceDescriptor);
	if (imageResource == nullptr)
	{
		this->log.error().format("Could not find specular map file: %s", imageResourceDescriptor.getPath().c_str());
		return false;
	}

	atlas.boundSpecularMapBuffer = this->writeToTextureBuffer(imageResource, atlas);
	return (bool)atlas.boundSpecularMapBuffer;
}

GLuint TextureManager::writeToTextureBuffer(std::shared_ptr<resource::Handle> imageResource, const TextureAtlas& atlas) const
{
	auto imageRwOps = SDL_RWFromConstMem(imageResource->getResourceBuffer(), static_cast<int>(imageResource->getResourceBufferSize()));
	assert(imageRwOps != nullptr);

	SDL_Surface* textureSurface = IMG_Load_RW(imageRwOps, 1);
	if (textureSurface == nullptr)
	{
		this->log.error().format("Failed to load texture \"%s\": %s\n",
					imageResource->getResourceDescriptor().getPath().c_str(), IMG_GetError());
		return 0;
	}

	GLint colorMode = GL_RGB;
	if (textureSurface->format->BytesPerPixel == 4)
	{
		colorMode = GL_RGBA;
	}

	/*
	 *  We had a problem where SDL_image loaded images in the BGR(A) format on some systems.
	 *  Here we check if the blue color is the first color and sets the BGR(A) format if it is.
	 */
	GLenum imageColorMode = GL_RGBA;
	if (textureSurface->format->Bmask == 0xFF)
	{
		imageColorMode = GL_BGR;
		if (textureSurface->format->BytesPerPixel == 4)
		{
			imageColorMode = GL_BGRA;
		}
	}
	GLuint buffer = 0;

	glGenTextures(1, &buffer);
	glBindTexture(GL_TEXTURE_2D, buffer);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (atlas.mipMap == true)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	glTexImage2D(GL_TEXTURE_2D, 0, colorMode, textureSurface->w, textureSurface->h, 0, imageColorMode, GL_UNSIGNED_BYTE, textureSurface->pixels);

	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(textureSurface);

	return buffer;
}

const TextureQuad& TextureManager::getTexture(const std::string& textureName) const
{
	auto textureIt = this->textureMap.find(textureName);

	if (textureIt != this->textureMap.end())
	{
		return textureIt->second;
	}
	else
	{
		this->log.error().format("Did not find texture with name \"%s\", returning default texture.", textureName.c_str());
		return this->defaultQuad;
	}
}

bool TextureManager::hasTexture(const std::string& textureName) const
{
	const auto& textureIt = this->textureMap.find(textureName);
	if (textureIt != this->textureMap.end())
	{
		return true;
	}

	return false;
}

unsigned int TextureManager::getNumAtlases() const
{
	return (unsigned int)this->textureAtlasFiles.size();
}

GLuint TextureManager::getTextureBuffer(unsigned int atlasNumber) const
{
	assert(atlasNumber < this->textureAtlasFiles.size());

	return this->textureAtlasFiles[atlasNumber].boundTextureBuffer;
}

GLuint TextureManager::getTextureBuffer(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return atlas.boundTextureBuffer;
		}
	}

	this->log.error().format("getTextureBuffer: No bound texture for atlas %s", atlasName.c_str());
	return 0;
}

GLuint TextureManager::getNormalMapBuffer(unsigned int atlasNumber) const
{
	assert(atlasNumber < this->textureAtlasFiles.size());

	return this->textureAtlasFiles[atlasNumber].boundNormalMapBuffer;
}

GLuint TextureManager::getNormalMapBuffer(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return atlas.boundNormalMapBuffer;
		}
	}

	this->log.error().format("getNormalMapBuffer: No bound normal map for atlas %s", atlasName.c_str());
	return 0;
}

GLuint TextureManager::getSpecularMapBuffer(unsigned int atlasNumber) const
{
	assert(atlasNumber < this->textureAtlasFiles.size());

	return this->textureAtlasFiles[atlasNumber].boundSpecularMapBuffer;
}

GLuint TextureManager::getSpecularMapBuffer(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return atlas.boundSpecularMapBuffer;
		}
	}

	this->log.error().format("getSpecularMapBuffer: No bound specular map for atlas %s", atlasName.c_str());
	return 0;
}

glm::ivec2 TextureManager::getAtlasSize(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return glm::ivec2(atlas.width, atlas.height);
		}
	}

	this->log.error().format("getAtlasSize: No atlas with name %s", atlasName.c_str());
	return glm::ivec2(0, 0);
}

}
} }
