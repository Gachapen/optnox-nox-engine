/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl/BackgroundGradient.h>

#include <nox/app/graphics/opengl/RenderData.h>
#include <nox/app/graphics/opengl/opengl_utils.h>
#include <nox/app/graphics/opengl/TextureQuad.h>

namespace nox { namespace app
{
namespace graphics
{

struct Vertex
{
	Vertex() = default;
	Vertex(const glm::vec2& position, const glm::vec4& color):
		position(position),
		color(color)
	{}

	glm::vec2 position;
	glm::vec4 color;
};

BackgroundGradient::BackgroundGradient():
	alpha(1.0f),
	vao(0),
	vertexBuffer(0),
	shader(0),
	dataChanged(true)
{
	this->gradientColors = {
		ColorPoint(0.0f, glm::vec3(0.0f)),
		ColorPoint(1.0f, glm::vec3(0.0f))
	};
}

void BackgroundGradient::addColorPoint(float position, const glm::vec3& color)
{
	auto pointIt = this->gradientColors.begin() + 1;
	const auto endIt = this->gradientColors.end() - 1;

	while (pointIt != endIt && pointIt->position < position)
	{
		++pointIt;
	}

	this->gradientColors.insert(pointIt, ColorPoint(position, color));

	this->dataChanged = true;
}

void BackgroundGradient::setBottomColor(const glm::vec3& color)
{
	if (this->gradientColors.front().color != color)
	{
		this->gradientColors.front().color = color;
		this->dataChanged = true;
	}
}

void BackgroundGradient::setTopColor(const glm::vec3& color)
{
	if (this->gradientColors.back().color != color)
	{
		this->gradientColors.back().color = color;
		this->dataChanged = true;
	}
}

void BackgroundGradient::setAlpha(const float alpha)
{
	if (this->alpha != alpha)
	{
		this->alpha = alpha;
		this->dataChanged = true;
	}
}

void BackgroundGradient::clearColorPoints()
{
	if (this->gradientColors.size() > 2)
	{
		this->gradientColors.erase(this->gradientColors.begin() + 1, this->gradientColors.end() - 1);
		this->dataChanged = true;
	}
}

void BackgroundGradient::setupRendering(RenderData& renderData, const GLuint shader)
{
	this->shader = shader;

	glGenVertexArrays(1, &this->vao);
	renderData.bindVertexArray(this->vao);

	glGenBuffers(1, &this->vertexBuffer);
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);


	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
				  sizeof(Vertex),
				  reinterpret_cast<void*>(offsetof(Vertex, position)));

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE,
				  sizeof(Vertex),
				  reinterpret_cast<void*>(offsetof(Vertex, color)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

void BackgroundGradient::handleIo(RenderData& renderData)
{
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);

	std::vector<Vertex> coords(this->gradientColors.size() * 2);
	std::vector<Vertex>::size_type currentCoordIndex = 0;

	for (const ColorPoint& point : this->gradientColors)
	{
		const float height = (point.position * 2.0f) - 1.0f;
		const glm::vec4 color(point.color, alpha);

		coords[currentCoordIndex] = Vertex({-1.0f, height}, color);
		currentCoordIndex++;

		coords[currentCoordIndex] = Vertex({1.0f, height}, color);
		currentCoordIndex++;
	}

	const GLsizeiptr coordsSize = static_cast<GLsizeiptr>(coords.size() * sizeof(Vertex));

	glBufferData(GL_ARRAY_BUFFER, coordsSize, nullptr, GL_DYNAMIC_DRAW);

	handleAsyncDataCopy(coords.data(), coordsSize);

	this->dataChanged = false;
}

void BackgroundGradient::render(RenderData& renderData)
{
	renderData.bindShaderProgram(this->shader);
	renderData.bindVertexArray(this->vao);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, static_cast<GLsizei>(this->gradientColors.size() * 2));
}

bool BackgroundGradient::needsBufferUpdate() const
{
	return this->dataChanged;
}

BackgroundGradient::ColorPoint::ColorPoint(const float position, const glm::vec3& color):
	position(position),
	color(color)
{
}

}
} }
