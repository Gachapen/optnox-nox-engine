/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/opengl/opengl_utils.h>
#include <glm/gtc/type_ptr.hpp>
#include <nox/util/algorithm.h>
#include <algorithm>
#include <fstream>

namespace nox { namespace app
{
namespace graphics
{

StenciledTiledTextureRenderer::StenciledTiledTextureRenderer() :
	TiledTextureRenderer(),
	stencilVAO(0),
	stencilVBO(0),
	stencilChanged(false),
	hasStencilObjects(false),
	numTriangleCoords(0)
{

}


void StenciledTiledTextureRenderer::setup(RenderData& renderData)
{
	this->setupTerrainRendering(renderData);
	this->setupTerrainStencil(renderData);
}

void StenciledTiledTextureRenderer::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/, const GLint stencilRef, const GLenum stencilTest)
{
	renderData.enable(GL_STENCIL_TEST);

	renderData.setStencilMask(0x00);
	renderData.setStencilFunc(stencilTest, stencilRef, 0xFF);

	if (this->hasStencilObjects)
	{
		this->renderTexture(renderData);
	}
}

void StenciledTiledTextureRenderer::stencilIncrement(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	renderData.enable(GL_STENCIL_TEST);

	renderData.setStencilMask(0xFF);
	renderData.setStencilFunc(GL_NEVER, 0x00, 0xFF);
	renderData.setStencilFailOperation(GL_REPLACE);

	this->renderStencil(renderData, viewProjectionMatrix);
}

void StenciledTiledTextureRenderer::stencil(RenderData& renderData, const glm::mat4& viewProjectionMatrix, const GLint ref)
{
	renderData.enable(GL_STENCIL_TEST);

	renderData.setStencilMask(0xFF);
	renderData.setStencilFunc(GL_NEVER, ref, 0xFF);
	renderData.setStencilFailOperation(GL_REPLACE);

	this->renderStencil(renderData, viewProjectionMatrix);
}

void StenciledTiledTextureRenderer::stencilAndRender(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	renderData.enable(GL_STENCIL_TEST);

	renderData.setStencilMask(0xFF);
	glClear(GL_STENCIL_BUFFER_BIT);

	renderData.setStencilFunc(GL_NEVER, 0x01, 0xFF);

	renderData.setStencilFailOperation(GL_REPLACE);

	this->renderStencil(renderData, viewProjectionMatrix);

	renderData.setStencilMask(0x00);

	renderData.setStencilFunc(GL_EQUAL, 0x01, 0xFF);

	if (this->hasStencilObjects)
	{
		this->renderTexture(renderData);
	}
}


void StenciledTiledTextureRenderer::setupTerrainStencil(RenderData& renderData)
{
	const std::string SHADER_NAME = "stencil";

	// Create shaders.
	this->shaderInfo.shaderProgram = glCreateProgram();
	assert(this->shaderInfo.shaderProgram != 0);

	std::ifstream vertFile("assets/shader/" + SHADER_NAME + ".vert");
	std::ifstream fragFile("assets/shader/" + SHADER_NAME + ".frag");
	if (!vertFile || !fragFile)
	{
//		g_systemLogger->error("Could not open " + SHADER_NAME + " shader files.");
	}

	GlslShader stencilVertexShader = createShaderAutoVersion(vertFile, GL_VERTEX_SHADER, renderData.getGlMajorVersion(), renderData.getGlMinorVersion());
	assert(stencilVertexShader.isCompiled());

	GlslShader stencilFragmentShader = createShaderAutoVersion(fragFile, GL_FRAGMENT_SHADER, renderData.getGlMajorVersion(), renderData.getGlMinorVersion());
	assert(stencilFragmentShader.isCompiled());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	glBindAttribLocation(this->shaderInfo.shaderProgram, vertexLocation, "vertex");

	// Link shader program.
	if (linkShaderProgram(this->shaderInfo.shaderProgram, stencilVertexShader.getId(), stencilFragmentShader.getId()) == false)
	{
//		g_systemLogger->error("Renderer: Could not link stencil shader.");
	}

	renderData.bindShaderProgram(this->shaderInfo.shaderProgram);
	this->shaderInfo.ViewProjectionMatrixUniform = glGetUniformLocation(this->shaderInfo.shaderProgram, "modelViewProjectionMatrix");
	assert(this->shaderInfo.shaderProgram > 0);

	glGenVertexArrays(1, &this->stencilVAO);
	renderData.bindVertexArray(this->stencilVAO);



	glGenBuffers(1, &this->stencilVBO);
	renderData.bindBuffer(GL_ARRAY_BUFFER, this->stencilVBO);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ObjectCoordinate), (void*) offsetof(ObjectCoordinate, geometryVertex));
	glEnableVertexAttribArray(0);
}

void StenciledTiledTextureRenderer::renderStencil(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	if (stencilChanged == true)
	{
		std::vector<ObjectCoordinate> coords;

		this->numTriangleCoords = 0;

		for (const std::shared_ptr<GeometrySet>& geometrySet : this->geometrySets)
		{
			if (geometrySet->isActive() == true)
			{
				const std::vector<ObjectCoordinate>& setCoordinates = geometrySet->getVertexData();
				GeometryPrimitive type = geometrySet->getGeometryPrimitive();
				
				const std::vector<ObjectCoordinate>::size_type renderDataSize = setCoordinates.size();
				
				if (type == GeometryPrimitive::TRIANGLE)
				{
					this->numTriangleCoords += renderDataSize;
				}

				// Temporary for loop since insert seems to crash in visual studio debug.
				for (size_t i = 0; i < setCoordinates.size(); i++)
				{
					coords.push_back(setCoordinates[i]);
				}
				
				//coords.insert(coords.end(), setCoordinates.begin(), setCoordinates.begin() + static_cast<std::vector<ObjectCoordinate>::difference_type>(renderDataSize));
			}
		}

		if (coords.size() > 0)
		{
			renderData.bindBuffer(GL_ARRAY_BUFFER, this->stencilVBO);

			glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(coords.size() * sizeof(ObjectCoordinate)), coords.data(), GL_DYNAMIC_DRAW);

			this->hasStencilObjects = true;
		}
		else
		{
			this->hasStencilObjects = false;
		}

		this->stencilChanged = false;
	}

	if (this->hasStencilObjects == true)
	{
		renderData.bindVertexArray(this->stencilVAO);

		GLuint previousShader = renderData.bindShaderProgram(this->shaderInfo.shaderProgram);

		glUniformMatrix4fv(this->shaderInfo.ViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

		if (this->numTriangleCoords > 0)
		{
			glDrawArrays(GL_TRIANGLES, (GLint)0, (GLsizei)numTriangleCoords);
		}

		renderData.bindShaderProgram(previousShader);
	}
}

void StenciledTiledTextureRenderer::addStencilGeometry(const std::shared_ptr<GeometrySet>& set)
{
	this->geometrySets.push_back(set);
	this->stencilChanged = true;
}

void StenciledTiledTextureRenderer::removeStencilGeometry(const std::shared_ptr<GeometrySet>& set)
{
	const bool removed = util::removeFirstFast(this->geometrySets, set);

	if (removed == true)
	{
		this->stencilChanged = true;
	}
}

void StenciledTiledTextureRenderer::notifyGeometryChange()
{
	this->stencilChanged = true;
}

}
} }
