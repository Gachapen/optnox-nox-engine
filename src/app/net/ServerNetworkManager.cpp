#include <nox/app/net/ServerNetworkManager.h>
#include <nox/app/net/common_convo.h>
#include <nox/app/net/RemoteClient.h>
#include <nox/app/net/BroadcastSender.h>
#include <nox/app/net/event/ClientConnected.h>
#include <nox/app/net/event/ClientDisconnected.h>

#include <assert.h>


namespace nox { namespace app { namespace net {


ServerNetworkManager::ServerNetworkManager(IPacketTranslator *translator,
									 ServerDelegate *delegate):
	NetworkManager(translator),
	accessList("access.ini", AccessFilter::FILTER_IP),
	serverDelegate(delegate),
	doBroadcast(false),
	broadcastSender(nullptr),
	serverSocket(nullptr)
{
	const unsigned lobbySize = delegate->getLobbySize();
	if (lobbySize == 0)
	{
		throw std::runtime_error("ServerNetworkManager initiated with a lobby "
				"size of 0. The ServerDelegate implementation must return "
				"a non-zero value in '::getLobbySize() const'.");
	}

	this->setLobbySize(delegate->getLobbySize());
}

ServerNetworkManager::~ServerNetworkManager()
{
	if (this->broadcastSender)
	{
		delete this->broadcastSender;
	}
}


bool ServerNetworkManager::initialize(nox::logic::IContext *context)
{
	if (!NetworkManager::initialize(context))
	{
		return false;
	}

	this->getLogger().setName("ServerNetworkManager");
	this->serverDelegate->setNetworkManager(this);

	// If setEnableDiscoveryBroadcast has been called before initialize,
	// re-call it so that the broadcasting actually starts.
	if (this->doBroadcast && !this->broadcastSender)
	{
		this->setEnableDiscoveryBroadcast(true);
	}


	return true;
}

void ServerNetworkManager::destroy()
{
	NetworkManager::destroy();
}

void ServerNetworkManager::update(const Duration& deltaTime)
{
	NetworkManager::update(deltaTime);

	handleNewConnections();
	handleAcceptConvos();
	updateRemoteClients(deltaTime);

	if (this->broadcastSender)
	{
		this->broadcastSender->update(deltaTime);
	}
}


bool ServerNetworkManager::startServer(Uint16 tcpListenPort)
{
	if (this->serverSocket)
	{
		return false;
	}

	this->serverSocket = new ConnectionListener(tcpListenPort);

	if (!this->serverSocket->isGood())
	{
		delete this->serverSocket;
		this->serverSocket = nullptr;
		return false;
	}

	return true;
}


void ServerNetworkManager::setEnableDiscoveryBroadcast(bool enable)
{
	// Set the "doBroadcast" flag. If initialize() has not yet been called,
	// this method will be called after it has.
	this->doBroadcast = enable;

	if (!this->serverSocket)
	{
		this->getLogger().warning().raw("Attempted to change discover broadcast "
										"setting on instance without a valid "
										"ConnectionListener");
		return;
	}

	const Duration duration = Duration(std::chrono::seconds(2));

	if (enable && !this->broadcastSender && this->getContext())
	{
		Uint16 tcpPort  = this->serverSocket->getListenPort();
		this->broadcastSender = new BroadcastSender(tcpPort, this->getContext(), this);
	}
	else if (!enable && this->broadcastSender)
	{
		delete this->broadcastSender;
		this->broadcastSender = nullptr;
	}
}

std::vector<RemoteClient*> ServerNetworkManager::getClients()
{
	return clients;
}


void ServerNetworkManager::sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() == client)
		{
			rc->sendPacket(&packet, proto);
			return;
		}
	}
}

void ServerNetworkManager::sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() != client)
		{
			rc->sendPacket(&packet, proto);
		}
	}
}

void ServerNetworkManager::sendToEveryone(const Packet& packet, TLProtocol proto)
{
	for (RemoteClient *rc : this->clients)
	{
		rc->sendPacket(&packet, proto);
	}
}


void ServerNetworkManager::kickClient(ClientId id, std::string reason)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		if ((*it)->getUserData().getClientId() == id)
		{
			this->onClientDisconnected(id);
			raiseDcEvent(id, protocol::DcReason::BAN_TIMED, 0, reason);

			delete *it;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void ServerNetworkManager::banClient(ClientId id, const Duration &dur, std::string reason)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		if ((*it)->getUserData().getClientId() == id)
		{
			this->onClientDisconnected(id);
			raiseDcEvent(id, protocol::DcReason::BAN_TIMED, 0, reason);

			IPaddress ipaddr = (*it)->getIpAddress(TLProtocol::TCP);
			std::string uname = (*it)->getUserData().getUserName();

			if (dur.count() == 0)
			{
				// Ban indefinitely
				this->accessList.setAccess(ipaddr, uname, AccessType::ALWAYS_DENY);
			}
			else
			{
				// Ban for a period
				this->accessList.banWithDuration(ipaddr, uname, dur);
			}

			delete *it;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

bool ServerNetworkManager::shouldAcceptClient(IPaddress ipaddr, std::string userName)
{
	if (this->getClientsInLobbyCount() >= this->getLobbySize())
	{
		return false;
	}

	switch (this->accessList.getAccessType(ipaddr, userName))
	{
		case AccessType::ALWAYS_ALLOW:
			return true;
		case AccessType::ALWAYS_DENY:
			return false;
		default:
			break;
	}

	/* Never reached */
	return true;
}

void ServerNetworkManager::sendSyncOutStateUpdates()
{
	// Build a map of every RemoteClient and their SyncOut-state-bundle
	std::map<RemoteClient*,Packet> clientStates;

	for (RemoteClient *rc : this->clients)
	{
		Packet packet;
		rc->packSyncOutUpdates(packet);
		clientStates[rc] = packet;
	}

	// Bundle "everyone else" into a packet and send it to each RC
	for (RemoteClient *rc : this->clients)
	{
		Packet packet;
		packet << static_cast<unsigned>(protocol::PacketId::SYNC_PACKET);

		int stateCount = 0;

		for (auto pair : clientStates)
		{
			if (pair.first != rc)
			{
				packet << pair.second;
				stateCount++;
			}
		}

		if (stateCount != 0)
		{
			rc->sendPacket(&packet, TLProtocol::UDP);
		}
	}
}

void ServerNetworkManager::onClientConnected(UserData userData)
{
	NetworkManager::onClientConnected(userData);
	this->serverDelegate->synchronizeExistingActors(userData.getClientId());
	this->serverDelegate->onClientJoined(this->getClientStats(userData.getClientId()));

	// Send notification to the existing lobby
	{
		event::ClientConnected conEvent(userData);

		Packet pkt;
		pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
		conEvent.serialize(&pkt);

		this->sendToEveryoneElse(userData.getClientId(), pkt);
	}

	// Send notifications about the existing lobby to the new arrival
	for (RemoteClient *rc : this->clients)
	{
		if (rc->getUserData().getClientId() != userData.getClientId())
		{
			event::ClientConnected conEvent(rc->getUserData());

			Packet pkt;
			pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
			conEvent.serialize(&pkt);

			this->sendExeclusivelyTo(userData.getClientId(), pkt);
		}
	}
}

void ServerNetworkManager::onClientDisconnected(ClientId clientId)
{
	this->serverDelegate->onClientLeft(this->getClientStats(clientId));
	NetworkManager::onClientDisconnected(clientId);
}


void ServerNetworkManager::setLobbySize(unsigned size)
{
	if (this->getLobbySize() != 0)
	{
		this->getLogger().warning().raw("ServerNetworkManager::setLobbySize called more than once");
	}

	NetworkManager::setLobbySize(size);
}


void ServerNetworkManager::handleNewConnections()
{
	while (this->serverSocket && this->serverSocket->hasNewConnection())
	{
		SocketTcp *tcp = this->serverSocket->popNewConnection(this->getContext());

		protocol::AcceptClientConversation *convo =
			new protocol::AcceptClientConversation(this, this->getContext(), tcp);
		this->acceptConvos.push_back(convo);
	}
}

void ServerNetworkManager::handleAcceptConvos()
{
	auto iter = this->acceptConvos.begin();
	while (iter != this->acceptConvos.end())
	{
		if (!(*iter)->update())
		{
			delete *iter;
			iter = this->acceptConvos.erase(iter);
			continue;
		}

		if ((*iter)->isDone())
		{
			this->getLogger().info().raw("AcceptConvo done");

			if ((*iter)->acceptedClient())
			{
				RemoteClient *client = (*iter)->getRemoteClient();
				assert(client != nullptr);
				client->setCustomPacketTranslator(packetTranslator);
				client->setServerDelegate(this->serverDelegate);
				clients.push_back(client);

				this->onClientConnected(client->getUserData());

				this->getLogger().info().format("Accepted client %s with ID %u",
						client->getUserData().getUserName().c_str(),
						client->getUserData().getClientId());
			}
			else
			{
				this->getLogger().info().raw("Rejected client");
			}

			delete *iter;
			iter = this->acceptConvos.erase(iter);
			continue;
		}

		iter++;
	}
}

void ServerNetworkManager::updateRemoteClients(const Duration &deltaTime)
{
	auto it = this->clients.begin();
	while (it != this->clients.end())
	{
		RemoteClient *client = *it;

		if (!client->update(deltaTime) || !client->isGood())
		{
			this->onClientDisconnected(client->getUserData().getClientId());
			this->raiseDcEvent(client->getUserData().getClientId(), protocol::CONNECTION_LOST);

			delete client;
			it = this->clients.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void ServerNetworkManager::raiseDcEvent(ClientId id, protocol::DcReason dcReason,
									 unsigned dur, std::string banReason)
{
	event::ClientDisconnected dcEvt(id, dcReason, dur, banReason);

	Packet pkt;
	pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
	dcEvt.serialize(&pkt);

	this->sendToEveryoneElse(id, pkt);
}

} } }
