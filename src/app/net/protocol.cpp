#include <nox/app/net/protocol.h>
#include <nox/app/net/SocketBase.h>
#include <nox/app/net/SocketUdp.h>

#include <nox/app/log/Logger.h>

#include <SDL2/SDL_net.h>

#include <stdlib.h>
#include <assert.h>
#include <sstream>
#include <string.h>


namespace nox { namespace app { namespace net {


std::string fieldTypeToStr(FieldType type)
{
	switch (type)
	{
		case FieldType::UINT32:
			return "UINT32";
		case FieldType::INT32:
			return "INT32";
		case FieldType::FLOAT32:
			return "FLOAT32";
		case FieldType::BYTE:
			return "BYTE";
		case FieldType::STRING:
			return "STRING";
		case FieldType::ARRAY:
			return "ARRAY";
		case FieldType::VEC2:
			return "VEC2";
		case FieldType::UNDEFINED:
			return "UNDEFINED";
		default:
			return "UNDEFINED";
	}
}

int volatileStrlen(const char *str, int maxlen)
{
	int i = 0;

	while (*str && i < maxlen)
	{
		i++;
		str++;
	}

	if (i == maxlen)
	{
		return -1;
	}

	return i;
}

std::string octalIpAddress(const IPaddress &ipaddr)
{
	// Convert the ipaddr from NO to HO.
	Uint32 ip = SDLNet_Read32(&ipaddr.host);

	std::stringstream ss;
	ss << ((ip >> 24) & 0xFF) << ".";
	ss << ((ip >> 16) & 0xFF) << ".";
	ss << ((ip >> 8 ) & 0xFF) << ".";
	ss << ((ip	  ) & 0xFF);
	return ss.str();
}

Uint32 octalToIp(std::string octal, bool convToNO)
{
	Uint32 ip = 0;

	int sections = 0;
	const char *delim = ".";
	char *token = nullptr;
	char str[80];
	strcpy(str, octal.c_str());

	token = strtok(str, delim);
	while (token != nullptr)
	{
		if (!*token)
		{
			return 0;
		}

		int val = atoi(token);
		if (val < 0 || val > 255)
		{
			return 0;
		}

		ip += (unsigned)val << unsigned((3-sections) * 8);

		sections++;
		if (sections > 4)
		{
			return 0;
		}

		token = strtok(nullptr, delim);
	}

	if (sections != 4)
	{
		return 0;
	}

	if (!convToNO)
	{
		return ip;
	}

	// Convert the IP from HO to NO.
	Uint32 noIp = 0;
	SDLNet_Write32(ip, &noIp);

	return noIp;
}

int readInt(const byte *buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	int i = 0;
	assert(sizeof(i) == sizeof(ho));

	memcpy(&i, &ho, sizeof(int));
	return i;
}

unsigned readUint(const byte *buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	return ho;
}

float readFloat(const byte *buf)
{
	Uint32 no = *((Uint32*) buf);
	Uint32 ho = SDLNet_Read32(&no);

	float f = 0.f;

	assert(sizeof f == sizeof ho);

	memcpy(&f, &ho, sizeof(f));
	return f;
}

unsigned networkDword(void *ptr)
{
	Uint32 uint = 0;
	Uint32 out = 0;
	memcpy(&uint, ptr, sizeof(Uint32));
	SDLNet_Write32(uint, &out);

	return out;
}


} } }
