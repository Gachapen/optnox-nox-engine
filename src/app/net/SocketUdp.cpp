#include <nox/app/net/SocketUdp.h>
#include <nox/app/net/Packet.h>

#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>


namespace nox { namespace app { namespace net { 


SocketUdp::SocketUdp(logic::IContext *ctx, Uint32 remHost, Uint16 remPort, Uint16 listPort) 
	:   SocketBase(ctx, remHost, remPort),
		socket(NULL) ,
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);   
}

SocketUdp::SocketUdp(logic::IContext *ctx, std::string hostname, Uint16 port, Uint16 listPort) 
	:   SocketBase(ctx, hostname, port),
		socket(NULL), 
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUdp::SocketUdp(logic::IContext *ctx, IPaddress ipaddr, Uint16 listPort) 
	:   SocketBase(ctx, ipaddr),
		socket(NULL),
		listenPort(listPort)
{
	this->bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUdp::~SocketUdp() 
{
	cleanup();
	SDLNet_FreePacket(this->bufferPacket);
}

bool SocketUdp::initSocket() 
{
	IPaddress *sockip = NULL;

	if (isInitialized()) 
	{
		return true;
	}

	this->socket = SDLNet_UDP_Open(this->listenPort);
	if (!this->socket) 
	{
		this->getLogger().error().format("SocketUdp: Failed to bind UDP port %hu (%s)", 
										 this->listenPort, SDLNet_GetError());
		cleanup();
		return false;
	}

	// Bind the remote host on channel 0 if this is not a server socket 
	if (!isServerSocket()) 
	{
		IPaddress ipaddr = getIPAddress();
		int chan = SDLNet_UDP_Bind(this->socket, 0, &ipaddr);
		if (chan == -1) 
		{
			this->getLogger().error().format("SocketUdp: Unable to bind UDP "
											 "socket on channel 0: %s", 
											 SDLNet_GetError());
			cleanup();
			return false;
		}
	}
	
	// Get the port bound on the local machine 
	sockip = SDLNet_UDP_GetPeerAddress(this->socket, -1);
	Uint16 boundPort = SDLNet_Read16(&sockip->port);
	if (this->listenPort && boundPort != this->listenPort) 
	{
		this->getLogger().warning().format("SocketUdp: Explicitly requested "
										   "port %hu, bound on %hu",
										   this->listenPort, boundPort);
	}

	this->listenPort = boundPort;

	this->getLogger().debug().format("Created UDP socket with remote host "
									 "%s:%hu, listen on %hu",
									 getOctalIP().c_str(), getPort(), this->listenPort);

	if (!onInitSuccess()) 
	{
		cleanup();
		return false;
	}

	return true;
}

TLProtocol SocketUdp::getTransportLayerProtocol() const 
{
	return TLProtocol::UDP;
}

Uint16 SocketUdp::getListenPort() const 
{
	return this->listenPort;
}

SDLNet_GenericSocket SocketUdp::getGenericSocket() const 
{
	return (SDLNet_GenericSocket) this->socket;
}

byte* SocketUdp::getSocketData(int *bufferLen, IPaddress &source) 
{
	*bufferLen = 0;
	byte *buf = NULL;
	int status = 0;

	status = SDLNet_UDP_Recv(this->socket, this->bufferPacket);
	if (status == 1) 
	{
		source = this->bufferPacket->address;

		if (this->bufferPacket->len == this->bufferPacket->maxlen) 
		{
			this->getLogger().error().format("Received packet exceeding "
					 "UDP_MAX_PKT_LEN (%u). Errors are very likely to occur!", 
					 UDP_MAX_PKT_LEN);
		}

		buf = new byte[this->bufferPacket->len];
		memcpy(buf, this->bufferPacket->data, (size_t)this->bufferPacket->len);
		*bufferLen = this->bufferPacket->len;
	} 
	else if (status == 0) 
	{
		// No data received 
		return NULL;
	} 
	else if (status == -1) 
	{
		// Errors occurred when reading 
		this->getLogger().error().format("Error in SDLNet_UDP_Recv: %s", 
										 SDLNet_GetError());
		return NULL;
	}

	return buf;
}

bool SocketUdp::sendBuffer(const byte *buffer, int bufferLen) const 
{
	while (bufferLen > 0) 
	{
		/* Do NOT send the entire packet in one batch if the packet
		 * exceeds UDP_MAX_PKT_LEN - the receiving host WILL be unable
		 * to receive it properly!!!! See SocketUdp documentation for
		 * more information.
		 */
		int toSend = (std::min)(UDP_MAX_PKT_LEN - 1, bufferLen);

		byte *tmp = new byte[toSend];
		memcpy(tmp, buffer, (size_t)toSend);

		UDPpacket pkt;
		pkt.data = tmp;
		pkt.len = toSend;
		pkt.address = getIPAddress();
		pkt.channel = 0;

		if (SDLNet_UDP_Send(this->socket, pkt.channel, &pkt) == 0) 
		{
			this->getLogger().error().format("Failed to send UDP packet: %s", 
					SDLNet_GetError());
			delete [] tmp;
			return false;
		}

		bufferLen -= toSend;

		delete [] tmp;
	}

	return true;
}

void SocketUdp::cleanup() 
{
	if (this->socket) 
	{
		SDLNet_UDP_Close(this->socket);
		this->socket = NULL;
	}
}

} } }
