#include <nox/app/net/common_convo.h>
#include <nox/app/net/ServerNetworkManager.h>

#include <assert.h>


namespace nox { namespace app { namespace net {
namespace protocol {

static ClientId g_clientId = 1;


/*
==================
JoinServerConversation
==================
*/
JoinServerConversation::JoinServerConversation(nox::logic::IContext *ctx,
											   SocketTcp *tcp,
											   UserData &user):
	Conversation(tcp, ctx),
	userData(user),
	tcpSocket(tcp),
	udpSocket(nullptr),
	localClient(nullptr),
	lobbySize(0U),
	init(false),
	sentTcp(false),
	sentUdp(false),
	accepted(false),
	done(false)
{
	this->createLogger("JoinServerConvo");
}

JoinServerConversation::~JoinServerConversation()
{
	if (!this->accepted && this->tcpSocket)
	{
		delete this->tcpSocket;
		assert(this->localClient == nullptr);
	}

	if (!this->accepted && this->udpSocket)
	{
		delete this->udpSocket;
		assert(this->localClient == nullptr);
	}
}


bool JoinServerConversation::start()
{
	if (this->init)
	{
		return false;
	}

	// Create a new UDP socket, bind to a random listening port.
	this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIPAddress(), 0);

	this->getLogger().info().format("UDP listening at %u", udpSocket->getListenPort());

	if (!this->udpSocket->initSocket())
	{
		this->done = true;
		return false;
	}

	if (!this->udpSocket->isGood())
	{
		this->done = true;
		return false;
	}

	if (!this->sendTcpRequest())
	{
		this->done = true;
		return false;
	}

	this->init = true;
	return true;
}

bool JoinServerConversation::update()
{
	if (this->done)
		return false;

	if (!this->tcpSocket->parseTrafficData())
	{
		this->getLogger().error().raw("Bad TCP socket state");
		this->done = true;
		this->accepted = false;
		return false;
	}

	if (this->sentTcp && !this->sentUdp)
	{
		if (this->handleTcpResponse())
		{
			// We have been accepted. Now we need to ensure that
			// the UDP sockets are functional.
			if (!this->sendUdpRequest())
			{
				this->done = true;
				this->accepted = false;
				return false;
			}
		}
	}

	if (this->sentUdp)
	{
		if (!this->udpSocket->parseTrafficData())
		{
			this->getLogger().error().raw("Bad UDP socket state");
			this->done = true;
			this->accepted = false;
			return false;
		}

		this->handleUdpResponse();
	}

	return true;
}

bool JoinServerConversation::isDone()
{
	return this->done;
}


bool JoinServerConversation::wasAccepted() const
{
	return this->accepted;
}

unsigned JoinServerConversation::getLobbySize() const
{
	return this->lobbySize;
}

LocalClient* JoinServerConversation::getLocalClient()
{
	if (this->localClient == nullptr && this->accepted)
	{
		this->localClient = new LocalClient(this->getContext(), this->userData,
											this->tcpSocket, this->udpSocket);
	}

	return this->localClient;
}

bool JoinServerConversation::sendTcpRequest()
{
	Packet pkt;

	pkt << static_cast<unsigned>(PacketId::JOIN_REQUEST);
	pkt << (unsigned int)this->udpSocket->getListenPort();
	pkt << this->getContext()->getApplicationId();
	pkt << this->getContext()->getApplicationVersionId();
	this->userData.serialize(&pkt);

	if (!this->tcpSocket->sendPacket(&pkt))
	{
		this->getLogger().error().raw("Failed to send TCP JOIN_REQUEST");
		return false;
	}

	this->sentTcp = true;

	this->getLogger().info().raw("Sent TCP JOIN_REQUEST");
	return true;
}

bool JoinServerConversation::handleTcpResponse()
{
	unsigned id = (unsigned)PacketId::JOIN_RESPONSE;
	Packet *pkt = this->findPacket<unsigned>(this->tcpSocket, id);

	if (pkt != nullptr)
	{
		unsigned id = 0;
		byte accept = 0;
		ClientId clientId = 0;
		unsigned udpPort = 0;

		*pkt >> id >> accept >> clientId >> udpPort >> this->lobbySize;
		delete pkt;

		this->accepted = accept;

		this->getLogger().info().format("Received JOIN_RESPONSE. "
										"Accepted=%i LobbySize=%u",
										accept, this->lobbySize);

		if (this->accepted)
		{
			this->userData.setClientId(clientId);

			// Create a new SocketUdp now that we know the port numbers of
			// both ourselves and the server.
			Uint16 inPort = this->udpSocket->getListenPort();
			Uint16 outPort = (Uint16)udpPort;
			delete this->udpSocket;

			this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIP(),
											outPort, inPort);

			if (!this->udpSocket->initSocket() || !this->udpSocket->isGood())
			{
				// If the UDP socket initialization failed, abort everything.
				// The manager of this conversation will clean up the TCP
				// socket, which will notify the server of our departure.
				this->done = true;
				this->accepted = false;

				this->getLogger().error().format("Second UDP socket "
												"initialization failed.");
			}
			else
			{
				this->userData.setClientId(clientId);
				return true;
			}
		}
	}

	return false;
}

bool JoinServerConversation::sendUdpRequest()
{
	Packet pkt;

	pkt << static_cast<unsigned>(PacketId::JOIN_REQUEST);

	if (!this->udpSocket->sendPacket(&pkt))
	{
		this->getLogger().error().raw("Failed to send UDP JOIN_REQUEST");
	}

	this->sentUdp = true;
	this->getLogger().info().raw("Sent UDP JOIN_REQUEST");
	return true;
}

void JoinServerConversation::handleUdpResponse()
{
	unsigned id = (unsigned)PacketId::JOIN_RESPONSE;
	Packet *pkt = this->findPacket<unsigned>(this->udpSocket, id);

	if (pkt)
	{
		delete pkt;

		// Coolio! All done!
		this->done = true;
	}
}



/*
==================
AcceptClientConversation
==================
*/
AcceptClientConversation::AcceptClientConversation(ServerNetworkManager *mgr,
												   nox::logic::IContext *ctx,
												   SocketTcp *tcp):
	Conversation(tcp, ctx),
	serverMgr(mgr),
	tcpSocket(tcp),
	udpSocket(nullptr),
	remoteClient(nullptr),
	recvTcpRequest(false),
	recvUdpRequest(false),
	done(false),
	accepted(false)
{
	this->createLogger("AcceptClientConvo");
}

AcceptClientConversation::~AcceptClientConversation()
{
	if (!this->accepted && this->tcpSocket)
	{
		delete this->tcpSocket;
		assert(this->remoteClient == nullptr);
	}

	if (!this->accepted && this->udpSocket)
	{
		delete this->udpSocket;
		assert(this->remoteClient == nullptr);
	}
}

bool AcceptClientConversation::update()
{
	if (this->done)
		return false;

	if (!this->tcpSocket->parseTrafficData())
	{
		this->getLogger().error().raw("Bad state in TCP socket");
		this->done = true;
		this->accepted = false;
		return false;
	}

	if (!this->recvTcpRequest)
	{
		if (!this->handleTcpRequest())
		{
			this->done = true;
			this->accepted = false;
			return false;
		}
	}
	else if (!this->recvUdpRequest)
	{
		if (!this->udpSocket)
		{
			this->getLogger().error().raw("Internal inconsistency: UDP socket is null");
			return false;
		}

		if (!this->udpSocket->parseTrafficData())
		{
			this->getLogger().error().raw("Bad state in UDP socket");
			this->done = true;
			this->accepted = false;
			return false;
		}

		if (!this->handleUdpRequest())
		{
			this->done = true;
			this->accepted = false;
			return false;
		}
	}


	return true;
}

bool AcceptClientConversation::isDone()
{
	return this->done;
}

bool AcceptClientConversation::acceptedClient() const
{
	return this->accepted;
}

RemoteClient* AcceptClientConversation::getRemoteClient()
{
	if (this->accepted && this->remoteClient == nullptr)
	{
		this->remoteClient = new RemoteClient(this->getContext(), this->userData,
											  this->tcpSocket, this->udpSocket);
	}

	return this->remoteClient;
}

bool AcceptClientConversation::handleTcpRequest()
{
	unsigned uId = (unsigned)PacketId::JOIN_REQUEST;
	Packet *pkt = findPacket<unsigned>(this->tcpSocket, uId);

	if (pkt)
	{
		this->recvTcpRequest = true;

		unsigned udpPort;
		bool admission = true;
		std::string appId;
		std::string versionId;

		*pkt >> uId
			 >> udpPort
			 >> appId
			 >> versionId;
		if (!this->userData.deSerialize(pkt))
		{
			this->getLogger().error().raw("Unable to deserialize UserData");
			admission = false;
		}

		if (admission == true)
		{
			// Deny the client if the app-ID or version-ID are mismatches
			if (appId != this->getContext()->getApplicationId() ||
				versionId != this->getContext()->getApplicationVersionId())
			{
				admission = false;
			}
		}

		if (admission == true)
		{
			IPaddress addr = this->tcpSocket->getIPAddress();
			admission = this->serverMgr->shouldAcceptClient(addr, this->userData.getUserName());
		}

		this->getLogger().info().format("Receieved TCP JOIN_REQUEST from %s (%s)",
										this->userData.getUserName().c_str(),
										(admission ? "accepted" : "rejected"));
		if (admission)
		{
			Uint16 outPort = (Uint16)udpPort;
			Uint16 inPort = 0;  // Let the OS decide a free port

			this->udpSocket = new SocketUdp(this->getContext(), this->tcpSocket->getIP(),
											outPort, inPort);
			if (!this->udpSocket->initSocket() || !this->udpSocket->isGood())
			{
				admission = false;
			}
		}

		return this->sendTcpResponse(admission);
	}

	return true;
}

bool AcceptClientConversation::sendTcpResponse(bool response)
{
	Packet pkt;

	ClientId clientId = (g_clientId++);
	this->userData.setClientId(clientId);

	const unsigned lobbySize = this->serverMgr->getLobbySize();


	pkt << (unsigned)PacketId::JOIN_RESPONSE;
	pkt << (byte)response;
	pkt << clientId;
	pkt << (response ? (unsigned)this->udpSocket->getListenPort() : 0U);
	pkt << lobbySize;

	if (!this->tcpSocket->sendPacket(&pkt))
	{
		this->accepted = false;
		this->getLogger().error().raw("Failed to send TCP JOIN_RESPONSE");
		return false;
	}

	this->getLogger().info().raw("Sent TCP JOIN_RESPONSE");

	this->accepted = response;
	return response;
}

bool AcceptClientConversation::handleUdpRequest()
{
	unsigned id = (unsigned)PacketId::JOIN_REQUEST;
	Packet *pkt = this->findPacket<unsigned>(this->udpSocket, id);

	if (pkt)
	{
		this->getLogger().info().raw("Received UDP JOIN_REQUEST");
		this->recvUdpRequest = true;
		delete pkt;
		return this->sendUdpResponse();
	}

	return true;
}

bool AcceptClientConversation::sendUdpResponse()
{
	Packet pkt;

	pkt << static_cast<unsigned>(PacketId::JOIN_RESPONSE);

	if (!this->udpSocket->sendPacket(&pkt))
	{
		this->accepted = false;
		this->getLogger().error().raw("Failed to send UDP JOIN_RESPONSE");
		return false;
	}

	this->getLogger().info().raw("Sent UDP JOIN_RESPONSE");
	this->done = true;
	return true;
}


}

} } }
