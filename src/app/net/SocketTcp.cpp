#include <nox/app/net/SocketTcp.h>
#include <nox/app/net/Packet.h>

#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>


namespace nox { namespace app { namespace net { 

SocketTcp::SocketTcp(logic::IContext *ctx, Uint32 remHost, Uint16 remPort) 
	:   SocketBase(ctx, remHost, remPort),
		socket(NULL) 
{

}

SocketTcp::SocketTcp(logic::IContext *ctx, std::string hostname, Uint16 remPort) 
	:   SocketBase(ctx, hostname, remPort),
		socket(NULL) 
{
	
}

SocketTcp::SocketTcp(logic::IContext *ctx, TCPsocket sock)
	:   SocketBase(ctx, *SDLNet_TCP_GetPeerAddress(sock)),
		socket(sock) 
{

}

SocketTcp::SocketTcp(logic::IContext *ctx, IPaddress ipaddr) 
	:   SocketBase(ctx, ipaddr),
		socket(NULL) 
{

}

SocketTcp::~SocketTcp() 
{
	cleanup();
}

bool SocketTcp::initSocket() 
{
	if (isInitialized()) 
	{
		return true;
	}

	if (!validIPAddress()) 
	{
		return false;
	}

	if (!this->socket) 
	{
		// The connection has NOT been previously established, 
		// connect to the remote host (or bind port in case of server)
		IPaddress ipaddr = getIPAddress();
		this->socket = SDLNet_TCP_Open(&ipaddr);
		if (!this->socket) 
		{
			this->getLogger().debug().format("Unable to initialize TCP socket (%s:%hu): \"%s\"",
											 getOctalIP().c_str(), 
											 getPort(),
											 SDLNet_GetError());
			cleanup();
			return false;
		}
	}

	if (!onInitSuccess()) 
	{
		cleanup();
		return false;
	}

	return true;
}

TLProtocol SocketTcp::getTransportLayerProtocol() const 
{
	return TLProtocol::TCP;
}

SDLNet_GenericSocket SocketTcp::getGenericSocket() const 
{
	return (SDLNet_GenericSocket) this->socket;
}

byte* SocketTcp::getSocketData(int *bufferLen, IPaddress &source) 
{
	*bufferLen = 0;
	byte *buf = NULL;
	int read = 0;

	// Use the default source
	source.host = 0U;
	source.port = 0U;

	if (isServerSocket()) 
	{
		this->getLogger().warning().raw("Attempted to get socket data from server-TCP socket");
		return NULL;
	}

	if (SDLNet_SocketReady(this->socket) <= 0) 
	{
		return NULL;
	}

	int maxlen = 128;
	int totRead = 0;
	buf = new byte[maxlen];

	read = SDLNet_TCP_Recv(this->socket, buf, maxlen);
	if (read <= 0) 
	{
		delete [] buf;
		return NULL;
	}

	totRead = read;

	while (read == maxlen) 
	{
		maxlen *= 2;
		buf = (byte*) realloc(buf, (size_t)maxlen);
		read = SDLNet_TCP_Recv(this->socket, buf + totRead, maxlen);
		totRead += read;
	}

	*bufferLen = totRead;
	return buf;
}

bool SocketTcp::sendBuffer(const byte *buffer, int bufferLen) const 
{
	if (!buffer || !bufferLen) 
	{
		return false;
	}

	if (SDLNet_TCP_Send(this->socket, buffer, bufferLen) <= 0) 
	{
		this->getLogger().error().format("Failed to send TCP packet: %s", SDLNet_GetError());
		return false;
	}

	return true;
}

void SocketTcp::cleanup() 
{
	if (this->socket) 
	{
		SDLNet_TCP_Close(this->socket);
		this->socket = NULL;
	}
}

} } }

