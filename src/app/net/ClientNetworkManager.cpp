#include <nox/app/net/ClientNetworkManager.h>
#include <nox/app/net/BroadcastReceiver.h>
#include <nox/app/net/common_convo.h>

#include <nox/app/net/event/ConnectionStarted.h>
#include <nox/app/net/event/ConnectionSuccess.h>
#include <nox/app/net/event/ConnectionFailed.h>


namespace nox { namespace app { namespace net {


ClientNetworkManager::ClientNetworkManager(IPacketTranslator *translator):
	NetworkManager(translator),
	joinConvo(nullptr),
	client(nullptr),
	broadcastRecv(nullptr),
	listenToBroadcasts(false)
{

}

ClientNetworkManager::~ClientNetworkManager()
{
	if (this->broadcastRecv)
	{
		delete this->broadcastRecv;
	}
}


bool ClientNetworkManager::connectToServer(std::string addr, Uint16 tcpPort,
										UserData &ud)
{
	this->serverHostname = addr;

	IPaddress ipaddr;
	SocketBase::resolveHost(addr, tcpPort, ipaddr);
	return connectToServer(ipaddr, ud);
}

bool ClientNetworkManager::connectToServer(IPaddress ipaddr, UserData &ud)
{
	this->serverHostname = octalIpAddress(ipaddr);

	if (!this->getContext())
	{
		// If we don't have a context yet, we also don't have a Logger.
		std::string msg = "ERROR: ClientNetworkManager::connectToServer called "
						  "before initialize() has been called. Hand the manager "
						  "over to the Logic-instance before attempting "
						  "to connect.";
		printf("%s\n", msg.c_str());
		throw std::runtime_error(msg);

		return false;
	}

	if (this->joinConvo || this->client)
	{
		this->getLogger().error().raw("Attempting to connect with an already "
									  "connected ClientNetworkManager.");
		return false;
	}

	this->onConnectionStarted();

	SocketTcp *tcp = new SocketTcp(this->getContext(), ipaddr);
	if (!tcp->initSocket() || !tcp->isGood())
	{
		this->onConnectionFailed(protocol::DcReason::UNKNOWN);

		this->getLogger().error().raw("Unable to connect to the server");
		delete tcp;
		return false;
	}

	this->joinConvo = new protocol::JoinServerConversation(getContext(), tcp, ud);

	if (!this->joinConvo->start())
	{
		this->onConnectionFailed(protocol::DcReason::UNKNOWN);

		this->getLogger().error().raw("Unable to start the join-conversation");
		delete this->joinConvo;
		this->joinConvo = nullptr;
		return false;
	}

	return true;
}

bool ClientNetworkManager::isConnected() const
{
	return (this->client != nullptr && this->serverHostname.length() > 0);
}

void ClientNetworkManager::setBroadcastListening(bool listen)
{
	this->listenToBroadcasts = listen;

	if (listen)
	{
		if (!this->broadcastRecv && this->getContext())
		{
			this->broadcastRecv = new BroadcastReceiver(this->getContext());
		}
	}
	else
	{
		if (this->broadcastRecv)
		{
			delete this->broadcastRecv;
			this->broadcastRecv = nullptr;
		}
	}
}


bool ClientNetworkManager::initialize(nox::logic::IContext *context)
{
	if (!NetworkManager::initialize(context))
	{
		return false;
	}

	this->getLogger().setName("ClientNetworkManager");

	// Delayed initialization of BC listening, a context is needed
	if (this->listenToBroadcasts)
	{
		this->setBroadcastListening(true);
	}

	return true;
}

void ClientNetworkManager::destroy()
{
	NetworkManager::destroy();
}

void ClientNetworkManager::update(const Duration& deltaTime)
{
	NetworkManager::update(deltaTime);

	handleJoinConvo();

	if (this->broadcastRecv)
	{
		this->broadcastRecv->update(deltaTime);
	}

	if (this->client)
	{
		this->client->update(deltaTime);

		if (!this->client->isGood())
		{
			this->onConnectionFailed(protocol::DcReason::CONNECTION_LOST);
		}
	}
}


LocalClient* ClientNetworkManager::getClient()
{
	return client;
}


void ClientNetworkManager::sendSyncOutStateUpdates()
{
	if (this->client == nullptr)
	{
		return;
	}

	Packet packet;
	packet << static_cast<unsigned>(protocol::PacketId::SYNC_PACKET);

	this->client->packSyncOutUpdates(packet);
	this->client->sendPacket(&packet, TLProtocol::UDP);
}

void ClientNetworkManager::handleJoinConvo()
{
	if (this->joinConvo)
	{
		if (!this->joinConvo->update())
		{
			delete this->joinConvo;
			this->joinConvo = nullptr;
		}
		else if (this->joinConvo->isDone())
		{
			this->getLogger().info().raw("JoinConvo done");

			if (this->joinConvo->wasAccepted())
			{
				this->client = this->joinConvo->getLocalClient();
				this->client->setCustomPacketTranslator(this->packetTranslator);
				this->setLobbySize(this->joinConvo->getLobbySize());
				this->onClientConnected(this->client->getUserData());
				this->onConnectionSuccess();

				this->getLogger().info().raw("Connected to server");
			}
			else
			{
				this->getLogger().info().raw("Server rejected the join request");
				this->onConnectionFailed(protocol::DcReason::UNKNOWN);
			}

			delete this->joinConvo;
			this->joinConvo = nullptr;
		}
	}
}

void ClientNetworkManager::onConnectionStarted()
{
	auto evt = std::make_shared<event::ConnectionStarted>(this->serverHostname);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);
}

void ClientNetworkManager::onConnectionSuccess()
{
	UserData ud = this->client->getUserData();
	auto evt = std::make_shared<event::ConnectionSuccess>(this->serverHostname, ud);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);

	assert(this->isConnected());
}

void ClientNetworkManager::onConnectionFailed(protocol::DcReason dc,
										   std::string reason,
										   unsigned dur)
{
	if (this->client)
	{
		delete this->client;
		this->client = nullptr;
	}

	auto evt = std::make_shared<event::ConnectionFailed>(
			this->serverHostname, dc, reason, dur);
	this->getContext()->getEventBroadcaster()->queueEvent(evt);

	this->serverHostname = "";

	assert(!this->isConnected());
}


} } }
