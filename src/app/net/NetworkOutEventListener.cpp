#include <nox/app/net/NetworkOutEventListener.h>
#include <nox/app/net/ServerNetworkManager.h>
#include <nox/app/net/ClientNetworkManager.h>
#include <nox/app/net/RemoteClient.h>
#include <nox/app/net/LocalClient.h>

#include <functional>

namespace nox { namespace app { namespace net {

NetworkOutEventListener::NetworkOutEventListener(bool server, NetworkManager *net):
				netMgr(netMgr),
				eventListener("NetworkOutEventListener"),
				log(netMgr->getContext()->createLogger())
{
	//Set the function used to send packets.
	if (server)
	{
		setServer();
	}
	else
	{
		setClient();
	}

	//Listen for NetworkOutEvents.
	this->eventBroadcaster = this->netMgr->getContext()->getEventBroadcaster();
	this->eventListener.setup(this, this->eventBroadcaster);

	this->eventListener.addEventTypeToListenFor(event::NetworkOutEvent::ID);
	this->eventListener.startListening();
}

void NetworkOutEventListener::setServer()
{
	this->isServer = true;
}

void NetworkOutEventListener::setClient()
{
	this->isServer = false;
}

NetworkOutEventListener::~NetworkOutEventListener()
{
	this->eventListener.stopListening();
}

void NetworkOutEventListener::onEvent(const std::shared_ptr<logic::event::Event>& event)
{
	if (event->isType(event::NetworkOutEvent::ID))
	{
		Packet pckt;
		pckt << (unsigned)protocol::EVENT_PACKET;

		event::NetworkOutEvent *netEvent = ((event::NetworkOutEvent*)event.get());
		logic::event::Event *contained = netEvent->getContainedEvent();
		TLProtocol protocol = netEvent->getProtocol();
		contained->serialize(&pckt);

		if (isServer)
		{
			sendPacketAsServer(&pckt, protocol);
		}
		else
		{
			sendPacketAsClient(&pckt, protocol);
		}
	}
}

void NetworkOutEventListener::sendPacketAsServer(const Packet* packet, TLProtocol protocol)
{
	//Send to everyone except self and originator of packet.
	ServerNetworkManager *sNetMgr = (ServerNetworkManager*)netMgr;

	for(RemoteClient* client : sNetMgr->getClients())
	{
		bool packetSent = client->sendPacket(packet, protocol);
		if (!packetSent) {
			this->log.warning().format("NetworkOutEventListener was unable to send packet");
		}
	}
}

void NetworkOutEventListener::sendPacketAsClient(const Packet* packet, TLProtocol protocol)
{
	//Send to server.
	ClientNetworkManager *cNetMgr = (ClientNetworkManager*)netMgr;
	bool packetSent = cNetMgr->getClient()->sendPacket(packet, TLProtocol::UDP);
	if (!packetSent) {
		this->log.warning().format("NetworkOutEventListener was unable to send packet");
	}
}

} } }
