#include <nox/app/net/ClientStats.h>



namespace nox { namespace app { namespace net {


ClientStats::ClientStats(UserData ud):
	userData(ud)
{
	
}

ClientStats::ClientStats(const ClientStats &other):
	userData(other.userData),
	msPing(other.msPing)
{

}

const UserData& ClientStats::getUserData() const
{
	return this->userData;
}

unsigned ClientStats::getPing() const
{
	return this->msPing;
}

void ClientStats::setPing(unsigned ms)
{
	this->msPing = ms;
}


} } }
