#include <nox/app/net/BroadcastSender.h>
#include <nox/app/net/Packet.h>
#include <nox/app/net/protocol.h>
#include <nox/app/net/NetworkManager.h>


namespace nox { namespace app { namespace net {


BroadcastSender::BroadcastSender(Uint16 port,
								 logic::IContext *ctx,
								 NetworkManager *mgr):
	context(ctx),
	interval(SERVER_BROADCAST_INTERV),
	accumulatedTime(interval),
	tcpPort(port),
	socket(nullptr),
	netMgr(mgr),
	logger(context->createLogger())
{
	this->logger.setName("BroadcastSender");

	SocketUdp *udp = new SocketUdp(this->context, "255.255.255.255",
								   SERVER_BROADCAST_PORT_NO, 0);

	if (!udp->initSocket() || !udp->isGood())
	{
		this->logger.error().raw("Unable to create broadcast socket");
		delete udp;
	}
	else
	{
		this->socket = udp;
	}
}

BroadcastSender::~BroadcastSender()
{
	if (this->socket)
	{
		delete this->socket;
	}
}


void BroadcastSender::setBroadcastInterval(const Duration &interval)
{
	this->interval = interval;
}

void BroadcastSender::update(const Duration &deltaTime)
{
	if (!this->socket->isGood())
	{
		this->logger.error().raw("Socket turned sour");
		delete this->socket;
		this->socket = nullptr;
	}
	else
	{
		this->accumulatedTime += deltaTime;
		if (this->accumulatedTime >= this->interval)
		{
			// Do not keep overflowed time
			this->accumulatedTime = Duration(0);
			this->sendBroadcast();
		}
	}
}


void BroadcastSender::sendBroadcast()
{
	Packet pkt;

	pkt << (unsigned)protocol::SERVER_BROADCAST;
	pkt << this->context->getApplicationId();
	pkt << this->context->getApplicationVersionId();
	pkt << "A Game Server";		 // Server name
	pkt << this->netMgr->getClientsInLobbyCount();
	pkt << this->netMgr->getLobbySize();
	pkt << (unsigned)this->tcpPort; // Server TCP connection port

	if (!this->socket->sendPacket(&pkt))
	{
		this->logger.error().raw("Unable to send broadcast packet");
	}
}


} } }
