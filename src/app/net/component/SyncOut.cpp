#include <nox/app/net/component/SyncOut.h>
#include <nox/app/net/Packet.h>
#include <nox/logic/actor/Actor.h>


namespace nox { namespace app { namespace net
{

const std::string SyncOut::PACKET_HEADER = "SyncOut";
const std::string SyncOut::PACKET_FOOTER = "/SyncOut";

const logic::actor::Component::IdType SyncOut::NAME = "SyncOut";

SyncOut::SyncOut()
{

}

const SyncOut::IdType& SyncOut::getName() const
{
	return SyncOut::NAME;
}

bool SyncOut::initialize(const Json::Value& componentJson)
{
	// TODO
	// Properly handle, or properly disable initialization via JSON.
	return false;
}

void SyncOut::serialize(Json::Value& componentObject)
{
	// TODO
	// Properly handle, or properly disable serialization of SyncOut components.
}

void SyncOut::packNetworkData(Packet &packet)
{
	auto syncComponents = this->getOwner()->getSynchronizedComponents();

	packet << PACKET_HEADER;			// Sync header
	packet << getOwner()->getSyncId();  // SyncId

	for (auto &id : syncComponents)
	{
		Component *comp = this->getOwner()->findComponent(id);
		if (comp != nullptr)
		{
			// Serialize into a temporary packet. This is done so we can check if
			// the Component actually put any values in - if it did, we need to
			// add a Component-header.
			Packet tmp;
			comp->serialize(tmp);
			if (tmp.getFieldCount() != 0)
			{
				// We need to store the ID so we know where to put the data, and
				// the number of fields to handle errors when deserializing
				packet << id;
				packet << (int)tmp.getFieldCount();
				packet << tmp;
			}
		}
	}

	packet << PACKET_FOOTER;
}


}
} }
