#include <nox/app/net/component/SyncIn.h>
#include <nox/app/net/component/SyncOut.h>
#include <nox/app/net/Packet.h>
#include <nox/logic/actor/Actor.h>


namespace nox { namespace app { namespace net
{

const logic::actor::Component::IdType SyncIn::NAME = "SyncIn";


SyncIn::SyncIn()
{
}

const logic::actor::Component::IdType& SyncIn::getName() const
{
	return SyncIn::NAME;
}

bool SyncIn::initialize(const Json::Value& componentJsonObject)
{
	return false;
}

void SyncIn::serialize(Json::Value& componentObject)
{

}

void SyncIn::unpackNetworkData(const Packet& packet) const
{
	auto syncComponents = this->getOwner()->getSynchronizedComponents();

	std::string header;
	packet >> header;

	while (header != SyncOut::PACKET_FOOTER)
	{
		int numFields = 0;
		packet >> numFields;

		int position = packet.getOutStreamPosition();

		// Skip this Component's state if it is not attached to the Actor, or
		// it is not listed in the synchronized components set.
		Component *component = this->getOwner()->findComponent(header);
		if (component == nullptr || syncComponents.find(header) == syncComponents.end())
		{
			if (!packet.setOutStreamPosition(position + numFields))
			{
				// This is bad. The Component header lied about its length, or
				// data has been corrupted. None of the remaining data can be
				// trusted.
				return;
			}
		}
		else
		{
			component->deSerialize(packet);

			// In case the Components deSerialize() didn't retrieve all fields,
			// set the out stream index to what it *should* have read. This step
			// is redundant in nearly all cases, but still an important safety measure.
			if (!packet.setOutStreamPosition(position + numFields))
			{
				// As when the Component is NULL (branch above this one), this indicates
				// a more severe problem. None of the data can be trusted.
				return;
			}
		}

		packet >> header;
	}
}


} } }
