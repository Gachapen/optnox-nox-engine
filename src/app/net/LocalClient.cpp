#include <nox/app/net/LocalClient.h>
#include <nox/app/net/component/SyncOut.h>
#include <nox/app/net/component/SyncIn.h>

#include <nox/app/net/event/NetworkOutEvent.h>

#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>

#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>

#include <algorithm>


namespace nox { namespace app { namespace net {

LocalClient::LocalClient(nox::logic::IContext *context, UserData user,
						 SocketTcp *tcp, SocketUdp *udp):
	CommunicationController(context, user, tcp, udp)
{
	this->addPacketHandler(protocol::PacketId::EVENT_PACKET,
		[&](const Packet& packet)
		{
			this->onNetworkEvent(packet);
		});
}


bool LocalClient::update(const Duration& deltaTime)
{
	return CommunicationController::update(deltaTime);
}

void LocalClient::onNetworkEvent(const Packet& packet)
{
	auto event = this->translatePacket(packet);

	if (event != nullptr)
	{
		//What if Jimmy packs a NetworkOutEvent into another NetworkOutEvent?
		if (event->isType(event::NetworkOutEvent::ID))
		{
			this->getLogger().error().raw("PacketTranslator returned a NetworkOutEvent");
			return;
		}

		this->getLogger().debug().format("Received event '%s'", event->getType().c_str());

		this->getContext()->getEventBroadcaster()->queueEvent(event);
	}

}

} } }
