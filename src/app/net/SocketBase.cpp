#include <nox/app/net/SocketBase.h>
#include <nox/app/net/Packet.h>

#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>


namespace nox { namespace app { namespace net {

bool SocketBase::resolveHost(std::string hostname, Uint16 port, IPaddress &ipaddr)
{
	if (SDLNet_ResolveHost(&ipaddr, hostname.c_str(), port) == -1)
	{
		// Log::error("Failed to resolve host: %s:%hu", hostname.c_str(), port);
		ipaddr.host = 0;
		ipaddr.port = 0;
		return false;
	}

	return true;
}


SocketBase::SocketBase(logic::IContext *ctx, IPaddress ipaddr)
	:   sockset(0),
		initialized(false),
		dc(false),
		ipaddr(ipaddr),
		logger(ctx->createLogger())
{
	this->logger.setName("Socket");
}

SocketBase::SocketBase(logic::IContext *ctx, Uint32 host, Uint16 port)
	:   sockset(0) ,
		initialized(false),
		dc(false),
		logger(ctx->createLogger())
{
	this->logger.setName("Socket");
	SDLNet_Write32(host, &this->ipaddr.host);
	SDLNet_Write16(port, &this->ipaddr.port);
}

SocketBase::SocketBase(logic::IContext *ctx, std::string hostname, Uint16 port)
	:   sockset(0),
		initialized(false),
		dc(false),
		logger(ctx->createLogger())
{
	this->logger.setName("Socket");

	if (!resolveHost(hostname, port, this->ipaddr)) {
		this->logger.error().format("Unable to resolve host '%s':'%u'!", hostname.c_str(), port);
	}
}

SocketBase::~SocketBase() {
	for (auto iter = this->inQueue.begin(); iter != this->inQueue.end(); iter++) {
		delete (*iter);
	}

	for (auto iter = this->outQueue.begin(); iter != this->outQueue.end(); iter++) {
		delete (*iter);
	}

	reset();
}

bool SocketBase::isServerSocket() const {
	return (this->ipaddr.host == 0);
}

bool SocketBase::isInitialized() const {
	return this->initialized;
}

bool SocketBase::isGood() const {
	return isInitialized() && !this->dc;
}

bool SocketBase::validIPAddress() const {
	return !(!this->ipaddr.host && !this->ipaddr.port);
}

bool SocketBase::hasActivity() const {
	if (!isGood()) {
		return false;
	}

	int numready = SDLNet_CheckSockets(this->sockset, 0);
	if (numready) {
		SDLNet_GenericSocket socket = getGenericSocket();
		return (SDLNet_SocketReady(socket) != 0);
	}

	return false;
}

bool SocketBase::parseTrafficData() {
	if (!isGood()) {
		return false;
	}

	if (readPackets() < 0) {
		return false;
	}

	return true;
}

bool SocketBase::sendPacket(const Packet *packet) const {
	if (!isGood()) {
		this->logger.debug().format("Attempted to send packet on bad socket");
		return false;
	}

	int len = 0;
	byte *buf = packet->serialize(len);

	if (!buf || !len) {
		if (buf) {
			delete [] buf;
		}

		return false;
	}

	bool status = sendPacket(buf, len);
	delete [] buf;

	return status;
}

bool SocketBase::sendPacket(const byte *buffer, int len) const {
	if (!isGood()) {
		this->logger.debug().raw("Attempted to send unnamed buffer on bad socket");
		return false;
	}

	return sendBuffer(buffer, len);
}

unsigned SocketBase::getIncomingPacketQueueSize() const {
	return (unsigned)this->inQueue.size();
}

void SocketBase::queuePacket(Packet *packet) const {
	this->outQueue.push_back(packet);
}

unsigned SocketBase::getOutgoingPacketQueueSize() const {
	return (unsigned)this->outQueue.size();
}

bool SocketBase::flushOutgoingPacketQueue() const {
	std::vector<byte> buffer;

	for (Packet *pkt: this->outQueue) {
		int len = 0;
		byte *buf = pkt->serialize(len);
		if (len <= 0 || buf == nullptr) {
			this->logger.warning().raw("SocketBase: Unable to serialize packet");
		} else {
			buffer.insert(buffer.end(), buf, buf+len);
			delete[] buf;
		}

		delete pkt;
	}

	this->outQueue.clear();
	bool success = true;

	if (buffer.size()) {
		if (!sendPacket(&buffer[0], (int)buffer.size())) {
			this->logger.error().raw("SocketBase: Unable to flush outgoing "
									 "packet queue");
			success = false;
		}
	}

	return success;
}

Packet* SocketBase::popPacket() {
	return this->getPacket(0);
}

Packet* SocketBase::getPacket(unsigned index) {
	if (index < this->inQueue.size()) {
		auto iter = this->inQueue.begin();
		std::advance(iter, index);

		Packet *pkt = *iter;
		this->inQueue.erase(iter);

		pkt->resetStream();
		return pkt;
	}

	return nullptr;
}

const Packet* SocketBase::peekPacket(unsigned index) const {
	if (this->inQueue.size() <= index) {
		return NULL;
	}

	auto iter = this->inQueue.cbegin();

	for (; iter != this->inQueue.cend(); iter++, index--) {
		if (index == 0) {
			(*iter)->resetStream();
			return *iter;
		}
	}

	return NULL;
}

IPaddress SocketBase::getIPAddress() const {
	return this->ipaddr;
}

std::string SocketBase::getOctalIP() const {
	return octalIpAddress(this->ipaddr);
}

Uint32 SocketBase::getIP() const {
	return SDLNet_Read32(&this->ipaddr.host);
}

Uint16 SocketBase::getPort() const {
	return SDLNet_Read16(&this->ipaddr.port);
}

log::Logger& SocketBase::getLogger() const
{
	return this->logger;
}

int SocketBase::readPackets() {
	if (!hasActivity())
		return 0;

	int len = 0;
	byte *buf = NULL;

	IPaddress sourceIp;
	memset(&sourceIp, 0, sizeof(sourceIp));

	buf = getSocketData(&len, sourceIp);
	if (!buf || len <= 0) {
		// It is VERY likely that the remote host has
		// disconnected.
		this->logger.warning().raw("SocketBase::readPackets(): Connection lost");
		this->dc = true;
		return -1;
	}

	int numRecv = 0;
	int read = 0;
	byte *bptr = buf;

	if (sourceIp.host == 0 || sourceIp.port == 0)
	{
		sourceIp = this->getIPAddress();
	}

	while (len > 0) {
		int pktLen = 0;
		Packet *pkt = new Packet;
		pktLen = pkt->deserialize(bptr, (size_t)len);

		pkt->setSourceAddress(sourceIp);

		if (pktLen > 0) {
			len -= pktLen;
			read += pktLen;
			bptr += pktLen;

			this->inQueue.push_back(pkt);
			numRecv++;
		} else {
			delete pkt;
			this->logger.error().format("Unable to decode received data. None "
					"of the remaining data (%i bytes) can be trusted!", len);
			break;
		}

	}

	delete[] buf;
	return numRecv;
}

bool SocketBase::onInitSuccess() {
	if (!createSocketSet()) {
		reset();
		return false;
	}

	SDLNet_GenericSocket socket = getGenericSocket();
	if (socket == NULL) {
		this->logger.error().raw("SocketBase::onInitSuccess(): "
								 "getGenericSocket() returned NULL");
		reset();
		return false;
	}

	if (!addToSocketSet(socket)) {
		reset();
		return false;
	}

	this->initialized = true;
	return true;
}

bool SocketBase::createSocketSet() {
	if (!this->sockset) {
		this->sockset = SDLNet_AllocSocketSet(10);
		if (!this->sockset) {
			this->logger.error().raw("Failed to allocate socket set");
			return false;
		}
	}

	return true;
}

bool SocketBase::addToSocketSet(SDLNet_GenericSocket socket) {
	if (!this->sockset || !socket) {
		return false;
	}

	if (SDLNet_AddSocket(this->sockset, socket) == -1) {
		this->logger.error().format("Unable to add socket to socket set (%s:%hu)",
									getOctalIP().c_str(), getPort());
		return false;
	}

	return true;
}

void SocketBase::reset() {
	if (this->sockset) {
		SDLNet_FreeSocketSet(this->sockset);
		this->sockset = NULL;
	}

	this->dc = false;
	this->initialized = false;
}

} } }
