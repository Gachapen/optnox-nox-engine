#include <nox/app/net/Conversation.h>


namespace nox { namespace app { namespace net {


Conversation::Conversation(SocketBase *sock, logic::IContext *ctx):
	socket(sock),
	context(ctx),
	initLogger(false)
{
	
}

bool Conversation::start()
{
	this->getLogger().error().raw("Conversation::start not overridden. Are you sure "
							 "you meant to call it in the first place?");

	return false;   
}

void Conversation::createLogger(std::string name)
{
	this->logger = this->context->createLogger();
	this->logger.setName(name);
	this->initLogger = true;
}

SocketBase* Conversation::getSocket()
{
	return this->socket;
}


log::Logger& Conversation::getLogger()
{
	if (!this->initLogger)
	{
		createLogger("Conversation");
	}

	return this->logger;
}

logic::IContext* Conversation::getContext()
{
	return this->context;
}


} } }
