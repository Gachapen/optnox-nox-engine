#include <nox/app/net/event/ConnectionSuccess.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType ConnectionSuccess::ID = "nox.conSucc";

ConnectionSuccess::ConnectionSuccess(std::string hostname, UserData ud):
	logic::event::Event(ID),
	serverHostname(hostname),
	userData(ud)
{
}

std::string ConnectionSuccess::getServerHostname() const
{
	return this->serverHostname;
}

const UserData& ConnectionSuccess::getUserData() const
{
	return this->userData;
}

}
} } }
