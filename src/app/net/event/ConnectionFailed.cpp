#include <nox/app/net/event/ConnectionFailed.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType ConnectionFailed::ID = "nox.conFailed";

ConnectionFailed::ConnectionFailed(std::string hostname, protocol::DcReason dc,
								   std::string reason, unsigned banDur):
	logic::event::Event(ID),
	serverHostname(hostname),
	dcType(dc),
	kickReason(reason),
	banDuration(banDur)
{
}

std::string ConnectionFailed::getServerHostname() const
{
	return this->serverHostname;
}

protocol::DcReason ConnectionFailed::getDisconnectType() const
{
	return this->dcType;
}

std::string ConnectionFailed::getKickReason() const
{
	return this->kickReason;
}

unsigned ConnectionFailed::getBanDuration() const
{
	return this->banDuration;
}


}
} } }
