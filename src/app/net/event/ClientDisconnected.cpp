#include <nox/app/net/event/ClientDisconnected.h>

namespace nox { namespace app { namespace net { namespace event {

const logic::event::Event::IdType ClientDisconnected::ID = "nox.net.ClientDC";

ClientDisconnected::ClientDisconnected(ClientId id, protocol::DcReason dcr, unsigned dur, std::string br):
	logic::event::Event(ID),
	clientId(id),
	dcReason(dcr),
	banDuration(dur),
	banReason(br)
{
}

ClientDisconnected::ClientDisconnected(const Packet* packet):
	logic::event::Event(ID)
{
	this->deSerialize(packet);
}


void ClientDisconnected::doSerialize() const
{
	logic::event::Event::doSerialize();

	*this << (unsigned)this->clientId
		  << (unsigned)this->dcReason
		  << this->banDuration
		  << this->banReason;
}

void ClientDisconnected::doDeSerialize()
{
	logic::event::Event::doDeSerialize();

	unsigned uClientId;
	unsigned uDcReason;

	*this >> uClientId
		  >> uDcReason
		  >> this->banDuration
		  >> this->banReason;

	this->clientId = static_cast<ClientId>(uClientId);
	this->dcReason = static_cast<protocol::DcReason>(uDcReason);
}


ClientId ClientDisconnected::getClientId() const
{
	return this->clientId;
}

protocol::DcReason ClientDisconnected::getDisconnectReason() const
{
	return this->dcReason;
}

unsigned ClientDisconnected::getBanDuration() const
{
	return this->banDuration;
}

std::string ClientDisconnected::getBanReason() const
{
	return this->banReason;
}


} } } }
