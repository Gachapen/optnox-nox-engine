#include <nox/app/net/event/ConnectionStarted.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType ConnectionStarted::ID = "nox.conInit";

ConnectionStarted::ConnectionStarted(std::string hostname):
	logic::event::Event(ID),
	serverHostName(hostname)
{
}

std::string ConnectionStarted::getServerHostname() const
{
	return this->serverHostName;
}

}
} } }
