#include <nox/app/net/event/ClientConnected.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType ClientConnected::ID = "nox.cliCon";

ClientConnected::ClientConnected(UserData ud):
	logic::event::Event(ID),
	userData(ud)
{
}

ClientConnected::ClientConnected(const Packet* packet):
	logic::event::Event(ID)
{
	this->deSerialize(packet);
}

const UserData& ClientConnected::getUserData() const
{
	return this->userData;
}

void ClientConnected::doSerialize() const
{
	logic::event::Event::doSerialize();
	*this << this->userData;
}

void ClientConnected::doDeSerialize()
{
	logic::event::Event::doDeSerialize();
	*this >> this->userData;
}

}
} } }
