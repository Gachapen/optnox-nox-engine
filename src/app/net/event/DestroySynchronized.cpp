#include <nox/app/net/event/DestroySynchronized.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType DestroySynchronized::ID = "nox.destroySync";

DestroySynchronized::DestroySynchronized(ClientId owner, SyncId id):
	logic::event::Event(ID),
	ownerClient(owner),
	syncId(id)
{
}

DestroySynchronized::DestroySynchronized(const Packet* packet):
	logic::event::Event(ID)
{
	this->deSerialize(packet);
}

ClientId DestroySynchronized::getOwningClientId() const
{
	return this->ownerClient;
}

SyncId DestroySynchronized::getSyncId() const
{
	return this->syncId;
}

void DestroySynchronized::doSerialize() const
{
	logic::event::Event::doSerialize();
	*this << this->ownerClient << this->syncId;
}

void DestroySynchronized::doDeSerialize()
{
	logic::event::Event::doDeSerialize();
	*this >> this->ownerClient >> this->syncId;
}

}
} } }
