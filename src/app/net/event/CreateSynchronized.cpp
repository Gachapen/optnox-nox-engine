#include <nox/app/net/event/CreateSynchronized.h>


namespace nox { namespace app { namespace net { namespace event
{

const logic::event::Event::IdType CreateSynchronized::ID = "nox.createSync";

CreateSynchronized::CreateSynchronized(std::string actor, ClientId owner, SyncId sync):
	logic::event::Event(CreateSynchronized::ID),
	actorName(actor),
	ownerClient(owner),
	syncId(sync)
{
}

CreateSynchronized::CreateSynchronized(const Packet* packet):
	logic::event::Event(ID)
{
	this->deSerialize(packet);
}

std::string CreateSynchronized::getActorName() const
{
	return this->actorName;
}

ClientId CreateSynchronized::getOwningClientId() const
{
	return this->ownerClient;
}

SyncId CreateSynchronized::getSyncId() const
{
	return this->syncId;
}

void CreateSynchronized::doSerialize() const
{
	logic::event::Event::doSerialize();
	*this << this->actorName << this->ownerClient << this->syncId;
}

void CreateSynchronized::doDeSerialize()
{
	logic::event::Event::doDeSerialize();
	*this >> this->actorName >> this->ownerClient >> this->syncId;
}


}
} } }

