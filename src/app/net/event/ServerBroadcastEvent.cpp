#include <nox/app/net/event/ServerBroadcastEvent.h>


namespace nox { namespace app { namespace net { namespace event {


const logic::event::Event::IdType ServerBroadcastEvent::ID = "nox.net.ServerBroadcast";

ServerBroadcastEvent::ServerBroadcastEvent(bool on, ServerConnectionInfo sci):
	logic::event::Event(ID),
	online(on),
	serverInfo(sci)
{

}

bool ServerBroadcastEvent::getOnline() const
{
	return this->online;
}

ServerConnectionInfo ServerBroadcastEvent::getServerConnectionInfo() const
{
	return this->serverInfo;
}


} } } }
