#include <nox/app/net/NetworkManager.h>
#include <nox/app/log/Logger.h>


namespace nox { namespace app { namespace net {


NetworkManager::NetworkManager(IPacketTranslator *translator):
	packetTranslator(translator),
	context(nullptr),
	comControl(nullptr),
	lobbySize(0U),
	syncOutFrequency(std::chrono::milliseconds(100))
{
}

bool NetworkManager::initialize(nox::logic::IContext *context)
{
	this->context = context;
	this->logger = this->context->createLogger();
	this->logger.setName("NetworkManager");

	return true;
}

void NetworkManager::destroy()
{
	for (auto it=this->clientStats.begin(); it!=this->clientStats.end(); it++)
	{
		delete it->second;
	}

	this->clientStats.clear();
}

void NetworkManager::update(const Duration& deltaTime)
{
	this->syncOutTimer += std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime);
	if (this->syncOutTimer > this->syncOutFrequency)
	{
		this->sendSyncOutStateUpdates();
		this->syncOutTimer = Duration(0);
	}
}


const std::vector<const ClientStats*> NetworkManager::getConnectedClients() const
{
	std::vector<const ClientStats*> vec;

	for (auto it=this->clientStats.begin(); it!=this->clientStats.end(); it++)
	{
		vec.push_back(it->second);
	}

	return vec;
}

const ClientStats* NetworkManager::getClientStats(ClientId client) const
{
	if (this->clientStats.count(client) != 0)
	{
		return this->clientStats.at(client);
	}

	return nullptr;
}

bool NetworkManager::isClientConnected(ClientId clientId) const
{
	return this->clientStats.count(clientId) != 0;
}

unsigned NetworkManager::getClientsInLobbyCount() const
{
	return (unsigned)this->clientStats.size();
}

unsigned NetworkManager::getLobbySize() const
{
	return this->lobbySize;
}

nox::logic::IContext* NetworkManager::getContext() const
{
	return this->context;
}


void NetworkManager::setCommunicationController(CommunicationController *comCont)
{
	this->comControl = comCont;
}

void NetworkManager::setLobbySize(unsigned size)
{
	this->lobbySize = size;
}

log::Logger& NetworkManager::getLogger()
{
	return this->logger;
}


void NetworkManager::onClientConnected(UserData userData)
{
	ClientId id = userData.getClientId();

	if (id == 0 || this->clientStats.count(id) != 0)
	{
		this->logger.error().format("Client%u (%s) either exists or is 0.",
				id, userData.getUserName().c_str());
		return;
	}

	this->clientStats[id] = new ClientStats(userData);
}

void NetworkManager::onClientDisconnected(ClientId id)
{
	if (this->clientStats.count(id))
	{
		delete this->clientStats[id];
		this->clientStats.erase(id);
	}
	else
	{
		this->logger.warning().format("Client '%u' is not a registered client", id);
	}
}

void NetworkManager::onClientPingUpdate(ClientId id, unsigned ms)
{
	if (this->clientStats.count(id))
	{
		this->clientStats[id]->setPing(ms);
	}
	else
	{
		this->logger.warning().format("Unable to update ping of non-existing Client '%u'", id);
	}
}

void NetworkManager::onEventPacketReceived(const Packet *packet)
{
	//Start by checking that it is in fact an event packet.
	unsigned id = 0;
	*packet >> id;
	if (id == static_cast<unsigned>(protocol::PacketId::EVENT_PACKET))
	{
		std::string eventId;
		*packet >> eventId;
		packet->setOutStreamPosition(1);

		std::shared_ptr<logic::event::Event> event = packetTranslator->translatePacket(eventId, packet);
		this->context->getEventBroadcaster()->queueEvent(event);
	}
}

} } }
