#include <nox/app/net/RemoteClient.h>
#include <nox/app/net/component/SyncOut.h>
#include <nox/app/net/ServerDelegate.h>
#include <nox/logic/actor/Actor.h>


namespace nox { namespace app { namespace net {


RemoteClient::RemoteClient(nox::logic::IContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp):
	CommunicationController(context, user, tcp, udp)
{
	this->addPacketHandler(protocol::PacketId::EVENT_PACKET,
		[&](const Packet& packet)
		{
			this->onNetworkEvent(packet);
		});
}


bool RemoteClient::update(const Duration& deltaTime)
{
	return CommunicationController::update(deltaTime);
}

void RemoteClient::setServerDelegate(ServerDelegate* serverDelegate)
{
	this->serverDelegate = serverDelegate;
}


void RemoteClient::onActorCreated(logic::actor::Actor* actor)
{
	/**
	 * As RemoteClients - unlike CommunicationController - is only responsible
	 * for dealing with Actors owned by the remote client, do nothing if  the
	 * Actor is owned by someone else.
	 */
	if (actor->getOwnerClientId() != this->getUserData().getClientId())
	{
		return;
	}

	/**
	 * RemoteClients are ONLY used on the server. Remotely owned synchronized actors
	 * only have a SyncIn-component attached to them by default. However, in order to
	 * synchronize the states to the non-owning clients, the actors must be given a
	 * SyncOut component as well.
	 */
	if (!actor->isOwnedLocally() && actor->findComponent<SyncOut>() == nullptr)
	{
		std::unique_ptr<logic::actor::Component> syncOut = std::make_unique<SyncOut>();
		assert(actor->addComponent(syncOut) == true);
	}

	CommunicationController::onActorCreated(actor);
}

void RemoteClient::onNetworkEvent(const Packet& packet)
{
	auto event = this->translatePacket(packet);

	if (event != nullptr)
	{
		if (this->serverDelegate == nullptr)
		{
			this->getLogger().error().raw("Unable to handle incoming packets: no ServerDelegate");
			return;
		}

		this->getLogger().debug().format("Received network event '%s' from client '%u'",
				event->getType().c_str(), this->getUserData().getClientId());

		this->serverDelegate->onClientNetworkEvent(this->getUserData().getClientId(), event);
	}
}

} } }
