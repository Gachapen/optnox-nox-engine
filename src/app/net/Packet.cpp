#include <nox/app/net/Packet.h>
#include <nox/app/net/protocol.h>
#include <nox/common/types.h>

#include <string.h>
#include <sstream>
#include <assert.h>


namespace nox { namespace app { namespace net {


Packet::Field::Field(FieldType type)
{
	this->type = type;
	memset(&this->value, 0, sizeof(this->value));

	assert(type != FieldType::UNDEFINED);
}

Packet::Field::Field(const Packet::Field &other):
	Packet::Field(other.getType())
{
	this->type = other.type;

	if (this->type == FieldType::STRING)
	{
		set(other.getString());
	}
	else if (this->type == FieldType::ARRAY)
	{
		FieldValue v = other.getValue();
		set(v.array.ptr, v.array.len);
	}
	else
	{
		memcpy(&this->value, &other.value, sizeof(this->value));
	}
}

Packet::Field::~Field()
{
	if (this->type == FieldType::STRING && this->value.str != NULL)
	{
		delete[] this->value.str;
	}
	else if (this->type == FieldType::ARRAY && this->value.array.ptr != NULL)
	{
		delete[] this->value.array.ptr;
	}
}

bool Packet::Field::set(void *val)
{
	if (!val)
	{
		return false;
	}

	switch (this->type)
	{
		case FieldType::UINT32:
			this->value.u32 = *((unsigned*)val);
			break;
		case FieldType::INT32:
			this->value.i32 = *((int*)val);
			break;
		case FieldType::FLOAT32:
			this->value.f32 = *((float*)val);
			break;
		case FieldType::BYTE:
			this->value.b = *((byte*)val);
			break;
		default:
			//Log::error("Called Packet::Field::set(void*) on Field of "
			//		   "type STRING or ARRAY");
			return false;
	}

	return true;
}

bool Packet::Field::set(glm::vec2 vec)
{
	if (this->type == FieldType::VEC2)
	{
		this->value.vec2.x = vec.x;
		this->value.vec2.y = vec.y;
		return true;
	}

	return false;
}

bool Packet::Field::set(std::string strValue)
{
	if (this->type != FieldType::STRING)
	{
		//Log::error("Called Packet::Field::set(std::string) on Field of "
		//	"non-string type");
		return false;
	}

	if (this->value.str)
	{
		delete[] this->value.str;
	}

	this->value.str = new char[strValue.length() + 1];
	memcpy(this->value.str, strValue.c_str(), strValue.length() + 1);

	return true;
}

bool Packet::Field::set(const byte *buffer, unsigned len)
{
	if (this->type != FieldType::ARRAY)
	{
		//Log::error("Called Packet::Field::set(byte*,int) on Filed of "
		//	"non-array type");
		return false;
	}

	if (len <= 0 || buffer == NULL)
	{
		//Log::error("Called Packet::Field::set(byte*,int) with zero-length "
		//	"or NULL-buffer");
		return false;
	}

	if (this->value.array.ptr)
	{
		delete[] this->value.array.ptr;
	}

	this->value.array.len = len;
	this->value.array.ptr = new byte[len];
	memcpy(this->value.array.ptr, buffer, len);
	return true;
}

const Packet::Field::FieldValue& Packet::Field::getValue() const
{
	return this->value;
}

FieldType Packet::Field::getType() const
{
	return this->type;
}

std::string Packet::Field::getString() const
{
	if (this->type != FieldType::STRING)
	{
		//Log::warning("Packet::Field::getString() called on non-string field");
		return "";
	}

	if (!this->value.str)
	{
		return "";
	}

	return std::string(this->value.str);
}

std::vector<byte> Packet::Field::getArray() const
{
	std::vector<byte> vec;

	if (this->type != FieldType::ARRAY)
	{
		//Log::warning("Packet::Field::getArray() called on non-array field");
		return vec;
	}

	for (size_t i = 0; i < this->value.array.len; i++)
	{
		vec.push_back(this->value.array.ptr[i]);
	}

	return vec;
}

glm::vec2 Packet::Field::getVec2() const
{
	if (this->type == FieldType::VEC2)
	{
		return glm::vec2(this->value.vec2.x, this->value.vec2.y);
	}

	return glm::vec2(0.f, 0.f);
}



Packet::Packet():
	outFieldIndex(0)
{
	this->sourceIp.host = 0U;
	this->sourceIp.port = 0;
}

Packet::~Packet()
{

}

byte* Packet::serialize(int &pktlen) const
{
	std::vector<byte> buffer;

	/**
	 * The total length of the packet is defined in the first four bytes of
	 * the packet. To avoid having to calculate the size before the buffer is
	 * filled, allocate the first four bytes and overwrite them once we know
	 * the size. The alternative is to insert the size into the first four
	 * bytes after the buffer is filled, but that will involve a relatively
	 * costly shift of the entire buffer content.
	 */
	for (int i=0; i<4; i++)
		buffer.push_back(0xFF);

	for (auto field : this->fields)
	{
		// Add the field header - the type of the following item
		buffer.push_back(static_cast<byte>(field.getType()));

		switch (field.getType())
		{
			case FieldType::UINT32:
				/* FALLTHROUGH */

			case FieldType::INT32:
				/* FALLTHROUGH */

			case FieldType::FLOAT32:
			{

				Packet::Field::FieldValue fval = field.getValue();

				/**
				 * Assert that the union uses the same memory address for
				 * all 32 bit fields, otherwise using a single fallthrough for
				 * all 32 bit fields is impossible
				 */
				assert((void*) &fval.u32 == (void*) &fval.i32
					&& (void*) &fval.u32 == (void*) &fval.f32);

				unsigned u32 = fval.u32;
				void *vptr = (void*) &u32;
				unsigned netdw = networkDword(vptr);
				byte *bytes = (byte*) &netdw;

				for (int i = 0; i < 4; i++)
				{
					buffer.push_back(bytes[i]);
				}
				break;
			}

			case FieldType::BYTE:
			{
				buffer.push_back(field.getValue().b);
				break;
			}

			case FieldType::STRING:
			{
				std::string str = field.getString();
				for (unsigned i = 0; i < str.length(); i++)
				{
					buffer.push_back((byte)str[i]);
				}

				buffer.push_back(0);
				break;
			}

			case FieldType::ARRAY:
			{
				/* The first four bytes of a serialized binary array is the
				 * 32 bit unsigned representation of the length in network
				 * byte order.
				 */
				unsigned l = field.getValue().array.len;
				byte *p = field.getValue().array.ptr;

				unsigned netdw = networkDword(&l);
				byte *netdwptr = (byte*) &netdw;
				for (int i = 0; i < 4; i++)
				{
					buffer.push_back(netdwptr[i]);
				}

				for (unsigned i = 0; i < l; i++)
				{
					buffer.push_back(p[i]);
				}
				break;
			}

			case FieldType::VEC2:
			{
				byte buf[8];

				Packet::Field::FieldValue fval = field.getValue();

				// Cast the X and Y addresses to (unsigned*) and dereference them to interpret
				// the byte values as "native unsigned".
				// This avoids casting the float values to unsigned ones (resulting in loss of
				// precision), while also avoiding usage of extensive copies.
				SDLNet_Write32(*(unsigned*)&fval.vec2.x, buf);
				SDLNet_Write32(*(unsigned*)&fval.vec2.y, buf + 4);

				for (int i = 0; i < 8; i++)
				{
					buffer.push_back(buf[i]);
				}

				break;
			}

			case FieldType::UNDEFINED:
			{
				break;
			}
		}
	}

	// The buffer will always contain at least 4 bytes. Do not count these when
	// deciding whether or not we filled the packet with something useful.
	pktlen = (int)buffer.size() - 4;
	if (pktlen < 0)
		return NULL;

	// Replace the allocated header with the length of the packet minus the
	// header itself.
	unsigned nwdw = networkDword((void*)&pktlen);
	byte *vp = (byte*)&nwdw;
	for (unsigned int i=0; i<4; i++)
		buffer[i] = vp[i];

	// The external caller is interested in the total size of the buffer.
	// Re-include the packet header.
	pktlen += 4;

	byte *b = new byte[pktlen];
	for (unsigned i = 0; i < buffer.size(); i++)
		b[i] = buffer[i];

	return b;
}

int Packet::deserialize(const byte *buf, size_t buflen)
{
	// Erase all existing contents.
	if (fields.size())
		fields.clear();

	int read = 0;

	const unsigned pktlen = readUint(buf) + 4;
	read += (int)sizeof(pktlen);

	if (pktlen > buflen || buflen < 4)
	{
		// TODO
		// Log a warning indicating that the given buffer is smaller than
		// what it claims to be. Also figure out a way to handle this error
		// properly. Can any of the remaining buffer content be trusted?
		// Probably not. How do we notify the caller properly?
		return 0;
	}

	while ((unsigned)read < pktlen)
	{
		FieldType fieldType = static_cast<FieldType>(buf[read++]);
		Field field(fieldType);

		switch (fieldType)
		{
			case FieldType::UINT32:
			{
				unsigned val = readUint(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::INT32:
			{
				int val = readInt(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::FLOAT32:
			{
				float val = readFloat(buf + read);
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::BYTE:
			{
				byte val = buf[read];
				field.set(&val);
				read += (int)sizeof(val);
				break;
			}

			case FieldType::STRING:
			{
				char *str = (char*) (buf + read);
				int len = volatileStrlen(str, (int)pktlen - read);
				if (len == -1)
				{
					//Log::error("Error in Packet::deserialize(): "
					//	"String field '%s' is not null-terminated!",
					//	field->getName().cthis->str());
				}
				else
				{
					std::string cppstr(str);
					field.set(cppstr);
					read += (len + 1);
				}
				break;
			}

			case FieldType::ARRAY:
			{
				/* The first four bytes is the 32 bit unsigned representation
				 * of the length of the following array.
				 */
				unsigned len = readUint(buf + read);
				field.set(buf + read + sizeof(len), len);

				read += (int)sizeof(len);
				read += (int)len;
				break;
			}

			case FieldType::VEC2:
			{
				glm::vec2 vec2;

				vec2.x = readFloat(buf + read);
				read += sizeof(vec2.x);

				vec2.y = readFloat(buf + read);
				read += sizeof(vec2.y);

				field.set(vec2);
				break;
			}


			case FieldType::UNDEFINED:
				/* FALLTHROUGH */
			default:
			{
				//Log::error("Packet::deserialize(): Unable to deserialize into "
				//	"field with undefined type");
				break;
			}
		}

		fields.push_back(field);
	}

	return (int)read;
}


const Packet& Packet::operator>>(byte &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::BYTE);
	val = field->getValue().b;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(unsigned &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::UINT32);
	val = field->getValue().u32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(int &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::INT32);
	val = field->getValue().i32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(float &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::FLOAT32);
	val = field->getValue().f32;
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(std::string &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::STRING);
	val = field->getString();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(std::vector<byte> &val) const
{
	const Packet::Field *field = verifyField(this->outFieldIndex, FieldType::ARRAY);
	val = field->getArray();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(glm::vec2 &val) const
{
	const Packet::Field *field = this->verifyField(this->outFieldIndex, FieldType::VEC2);
	val = field->getVec2();
	this->outFieldIndex++;
	return *this;
}

const Packet& Packet::operator>>(Packet &val) const
{
	val << *this;
	return *this;
}


Packet& Packet::operator<<(byte val)
{
	Packet::Field field(FieldType::BYTE);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(unsigned val)
{
	Packet::Field field(FieldType::UINT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(int val)
{
	Packet::Field field(FieldType::INT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(float val)
{
	Packet::Field field(FieldType::FLOAT32);
	field.set(&val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(std::string val)
{
	Packet::Field field(FieldType::STRING);
	field.set(val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(std::vector<byte> val)
{
	Packet::Field field(FieldType::ARRAY);
	field.set(&val[0], (unsigned)val.size());
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(glm::vec2 val)
{
	Packet::Field field(FieldType::VEC2);
	field.set(val);
	this->fields.push_back(field);
	return *this;
}

Packet& Packet::operator<<(const Packet& val)
{
	for (int i=0; i<val.getFieldCount(); i++)
	{
		const Packet::Field *field = val.getField(i);

		switch (field->getType())
		{
			case FieldType::ARRAY:
			{
				// Make a deep copy of the array
				Packet::Field cpy(FieldType::ARRAY);
				cpy.set(field->getValue().array.ptr, field->getValue().array.len);
				this->fields.push_back(cpy);
				break;
			}

			case FieldType::STRING:
			{
				// Make a deep copy of the string
				Packet::Field cpy(FieldType::STRING);
				cpy.set(field->getString());
				this->fields.push_back(cpy);
				break;
			}

			default:
				// All other fields can be copied directly
				this->fields.push_back(*field);
				break;
		}
	}

	return *this;
}


int Packet::getFieldCount() const
{
	return (int)this->fields.size();
}

const Packet::Field* Packet::getField(int index) const
{
	return &this->fields[(size_t)index];
}


void Packet::resetStream()
{
	this->outFieldIndex = 0;
}

unsigned Packet::getOutStreamPosition() const
{
	return this->outFieldIndex;
}

bool Packet::setOutStreamPosition(unsigned position) const
{
	if (position >= this->fields.size())
	{
		return false;
	}

	this->outFieldIndex = position;
	return true;
}



IPaddress Packet::getSourceAddress() const
{
	return this->sourceIp;
}

void Packet::setSourceAddress(IPaddress addr)
{
	this->sourceIp = addr;
}


void Packet::logContents() const
{
	std::stringstream ss;
	ss <<"Packet: { ";

	bool first = true;

	for (auto &field : this->fields)
	{
		if (!first)
			ss << ", ";
		first = false;

		ss << "'" << fieldTypeToStr(field.getType()) << "': '";

		switch (field.getType())
		{
			case FieldType::INT32:
				ss << field.getValue().i32;
				break;
			case FieldType::UINT32:
				ss << field.getValue().u32;
				break;
			case FieldType::FLOAT32:
				ss << field.getValue().f32;
				break;
			case FieldType::BYTE:
				ss << field.getValue().b;
				break;
			case FieldType::STRING:
				ss << field.getString();
				break;
			case FieldType::ARRAY:
				ss << "[array, " << field.getValue().array.len << " bytes]";
				break;
			case FieldType::VEC2:
			{
				Field::WrapVec2 v = field.getValue().vec2;
				ss << "[vec2 (" << v.x << ", " << v.y << ")]";
				break;
			}
			case FieldType::UNDEFINED:
				ss << "[undefined]";
				break;
		}

		ss << "'";
	}

	ss << " }";

	printf("%s\n", ss.str().c_str());
}


const Packet::Field* Packet::verifyField(unsigned fieldIndex,
										 FieldType expectedType) const
{
	if (fieldIndex >= this->fields.size())
	{
		throw std::runtime_error("Packet >> error: Expected to retrieve "
				"value of type " + fieldTypeToStr(expectedType) + ", but the "
				"Packet has no additional fields.");
	}

	const Packet::Field *field = &this->fields[fieldIndex];

	if (field->getType() != expectedType)
	{
		throw std::runtime_error("Packet >> error: Expected to retrieve "
				"value of type " + fieldTypeToStr(expectedType) + ", but the "
				"next value is of type " +
				fieldTypeToStr(field->getType()));
	}

	return field;
}

} } }

