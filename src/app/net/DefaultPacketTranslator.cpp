#include <nox/app/net/DefaultPacketTranslator.h>

#include <nox/app/net/event/ClientConnected.h>
#include <nox/app/net/event/ClientDisconnected.h>
#include <nox/app/net/event/CreateSynchronized.h>
#include <nox/app/net/event/DestroySynchronized.h>

namespace nox { namespace app { namespace net
{

DefaultPacketTranslator::DefaultPacketTranslator()
{
	/**
	 * Add a lambda to this->eventCreatorMap, returning a new instance of an event,
	 * given an event::ID.
	 */
	#define MAP_EVENT(_event_class)									 \
		this->eventCreatorMap[_event_class::ID] =					   \
		[](const Packet* pkt)										   \
		{															   \
			_event_class *raw = new _event_class(pkt);				  \
			std::shared_ptr<_event_class> shared(raw);				  \
			return shared;											  \
		}

	MAP_EVENT(event::ClientConnected);
	MAP_EVENT(event::ClientDisconnected);
	MAP_EVENT(event::CreateSynchronized);
	MAP_EVENT(event::DestroySynchronized);

	#undef MAP_EVENT
}

std::shared_ptr<logic::event::Event> DefaultPacketTranslator::translatePacket(std::string eventId, const Packet *packet)
{
	if (this->eventCreatorMap.count(eventId))
	{
		return this->eventCreatorMap[eventId](packet);
	}

	return nullptr;
}

}
} }
