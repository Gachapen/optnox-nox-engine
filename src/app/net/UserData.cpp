#include <nox/app/net/UserData.h>


namespace nox { namespace app { namespace net {


UserData::UserData():
	userName("Unkown Player")
{

}


void UserData::setUserName(std::string userName)
{
	this->userName = userName;
}

std::string UserData::getUserName() const
{
	return this->userName;
}


void UserData::setClientId(ClientId id)
{
	this->clientId = id;
}

ClientId UserData::getClientId() const
{
	return this->clientId;
}


void UserData::serialize(Packet *packet) const
{
	(*packet) << this->userName << this->clientId;
}

bool UserData::deSerialize(const Packet *packet)
{
	try
	{
		(*packet) >> this->userName >> this->clientId;
	}
	catch (...)
	{
		return false;
	}

	return true;
}



} } }
