#include <nox/app/net/ServerDelegate.h>
#include <nox/app/net/ServerNetworkManager.h>
#include <nox/app/net/RemoteClient.h>

#include <nox/app/net/event/CreateSynchronized.h>
#include <nox/app/net/event/DestroySynchronized.h>

#include <nox/app/net/RemoteClient.h>

#include <nox/app/net/event/ClientConnected.h>
#include <nox/app/net/event/ClientDisconnected.h>
#include <nox/app/net/protocol.h>

namespace nox { namespace app { namespace net
{

// Using a static, global counter to guarantee uniqueness. This value holds the SyncId of
// the *next* synchronized actor, and is intended to be read, assigned, and incremented.
static SyncId g_syncId = 1337;


void ServerDelegate::setNetworkManager(ServerNetworkManager *netMgr)
{
	assert(netMgr != nullptr);
	this->networkManager = netMgr;

	this->context = this->networkManager->getContext();

	this->log = this->context->createLogger();
	this->log.setName("ServerDelegate");
}

void ServerDelegate::synchronizeExistingActors(ClientId clientId)
{
	for (auto pair : this->actorMap)
	{
		auto set = pair.second;

		for (auto syncId : set)
		{
			std::string defName = this->actorDefinitionMap[syncId];

			event::CreateSynchronized event(defName, pair.first, syncId);
			Packet pkt;
			pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
			event.serialize(&pkt);

			this->sendExeclusivelyTo(clientId, pkt);
		}
	}
}

SyncId ServerDelegate::createSynchronizedActor(std::string actor, ClientId owner)
{
	SyncId currentSyncId = g_syncId;
	g_syncId++;

	if (!this->networkManager->isClientConnected(owner))
	{
		this->log.error().format("Attempted to create synchronized actor of type '%s' with "
				"non-existing owner of ID '%u'", actor.c_str(), owner);
		return 0;
	}

	this->log.debug().format("Creating actor of type '%s' withSyncId %u, owned by client %u",
			actor.c_str(), currentSyncId, owner);


	// Notify the lobby
	event::CreateSynchronized createEvent(actor, owner, currentSyncId);

	Packet pkt;
	pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
	createEvent.serialize(&pkt);

	this->sendToEveryone(pkt, TLProtocol::TCP);

	// Map the SyncId of the Actor to the owner
	this->actorMap[owner].insert(currentSyncId);
	this->actorDefinitionMap[currentSyncId] = actor;
	return currentSyncId;
}

bool ServerDelegate::destroySynchronizedActor(SyncId syncId)
{
	for (auto it = this->actorMap.begin(); it != this->actorMap.end(); it++)
	{
		std::set<SyncId> &set = it->second;
		if (set.find(syncId) != set.end())
		{
			this->raiseDestroySynchronizedActorEvent(it->first, syncId);
			set.erase(syncId);
			this->actorDefinitionMap.erase(syncId);
			return true;
		}
	}

	return false;
}

int ServerDelegate::destroyAllSynchronizedActors(ClientId owner)
{
	const int count = (int)this->actorMap[owner].size();

	for (SyncId syncId : this->actorMap[owner])
	{
		this->raiseDestroySynchronizedActorEvent(owner, syncId);
		this->actorDefinitionMap.erase(syncId);
	}

	this->actorMap[owner].clear();

	return count;
}


void ServerDelegate::sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendExeclusivelyTo(client, packet, proto);
}

void ServerDelegate::sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendToEveryoneElse(client, packet, proto);
}

void ServerDelegate::sendToEveryone(const Packet& packet, TLProtocol proto)
{
	this->networkManager->sendToEveryone(packet, proto);
}


log::Logger& ServerDelegate::getLog()
{
	return this->log;
}

logic::IContext* ServerDelegate::getContext()
{
	return this->context;
}

void ServerDelegate::raiseDestroySynchronizedActorEvent(ClientId owner, SyncId syncId)
{
	event::DestroySynchronized destroy(owner, syncId);

	Packet pkt;
	pkt << (unsigned)protocol::PacketId::EVENT_PACKET;
	destroy.serialize(&pkt);

	this->sendToEveryone(pkt, TLProtocol::TCP);
}

}
} }
