if (NOX_RESOURCE_JSON)
	if (NOX_BUILD_SHARED)
		set(JSONCPP_LIB_BUILD_SHARED ON CACHE BOOL "Enable jsoncpp shared library" FORCE)
	else ()
		set(JSONCPP_LIB_BUILD_SHARED OFF CACHE BOOL "Enable jsoncpp shared library" FORCE)
	endif ()
	set(JSONCPP_WITH_TESTS OFF CACHE BOOL "Enable jsoncpp tests" FORCE)
	set(JSONCPP_WITH_POST_BUILD_UNITTEST OFF CACHE BOOL "Enable jsoncpp post build unit tests" FORCE)
	set(JSONCPP_WITH_PKGCONFIG_SUPPORT OFF CACHE BOOL "Enable jsoncpp pkgconfig support" FORCE)

	add_subdirectory(jsoncpp)
endif ()

set(POLY2TRI_DIR poly2tri/poly2tri)

set(POLY2TRI_HEADERS
	${POLY2TRI_DIR}/poly2tri.h
	${POLY2TRI_DIR}/common/shapes.h
	${POLY2TRI_DIR}/common/utils.h
	${POLY2TRI_DIR}/sweep/cdt.h
	${POLY2TRI_DIR}/sweep/advancing_front.h
	${POLY2TRI_DIR}/sweep/sweep_context.h
	${POLY2TRI_DIR}/sweep/sweep.h
)

set(POLY2TRI_SOURCES
	${POLY2TRI_DIR}/common/shapes.cc
	${POLY2TRI_DIR}/sweep/cdt.cc
	${POLY2TRI_DIR}/sweep/advancing_front.cc
	${POLY2TRI_DIR}/sweep/sweep_context.cc
	${POLY2TRI_DIR}/sweep/sweep.cc
)

if (NOX_BUILD_SHARED)
	add_library(poly2tri-shared SHARED ${POLY2TRI_HEADERS} ${POLY2TRI_SOURCES})
	target_include_directories(poly2tri-shared PUBLIC ${POLY2TRI_DIR})
	set_target_properties(poly2tri-shared PROPERTIES
		OUTPUT_NAME poly2tri
	)
endif ()

if (NOX_BUILD_STATIC)
	add_library(poly2tri-static STATIC ${POLY2TRI_HEADERS} ${POLY2TRI_SOURCES})
	target_include_directories(poly2tri-static PUBLIC ${POLY2TRI_DIR})
	set_target_properties(poly2tri-static PROPERTIES
		OUTPUT_NAME poly2tri
	)
endif ()

if (NOX_BUILD_TEST)
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
	
	add_subdirectory(gtest)
	target_include_directories(gtest PUBLIC "gtest/include")
endif ()

if (NOX_PHYSICS_BOX2D)
	if (NOX_BUILD_SHARED)
		set(BOX2D_BUILD_SHARED ON CACHE BOOL "" FORCE)
	else ()
		set(BOX2D_BUILD_SHARED OFF CACHE BOOL "" FORCE)
	endif ()

	if (NOX_BUILD_STATIC)
		set(BOX2D_BUILD_STATIC ON CACHE BOOL "" FORCE)
	else ()
		set(BOX2D_BUILD_STATIC OFF CACHE BOOL "" FORCE)
	endif ()

	set(BOX2D_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
	set(BOX2D_INSTALL OFF CACHE BOOL "" FORCE)
	set(BOX2D_BUILD_UNITTESTS OFF CACHE BOOL "" FORCE)

	add_subdirectory(liquidfun/liquidfun/Box2D)

	if (NOX_BUILD_SHARED)
		target_include_directories(Box2D_shared PUBLIC "liquidfun/liquidfun/Box2D")
		set_target_properties(Box2D_shared PROPERTIES
			OUTPUT_NAME Box2D
		)
	endif ()
	if (NOX_BUILD_STATIC)
		target_include_directories(Box2D PUBLIC "liquidfun/liquidfun/Box2D")
		set_target_properties(Box2D PROPERTIES
			OUTPUT_NAME Box2D
		)
	endif ()
endif ()
