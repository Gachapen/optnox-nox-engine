/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <memory>
#include <fstream>
#include <string>

#include <nox/common/types.h>
#include <nox/util/IniFile.h>


class TestIni: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
        createValid();
        createSemiValid();
        createInvalid();
	}

	virtual void TearDown()
	{
        remove("valid.ini");
        remove("semivalid.ini");
        remove("invalid.ini");
        remove("out.ini");
	}


    void createValid()
    {
        std::ofstream file("valid.ini");
        file << "[Valid]" << std::endl;
        file << "value0 = 45" << std::endl;
        file << "value1 = 3.14" << std::endl;
        file << "value2=something something" << std::endl;
        file << std::endl << "; The sections will be joined " << std::endl;
        file << "[Valid]; comment" << std::endl;
        file << "value3 = hey_guys$# ; $#$# comment" << std::endl;
    }

    void createSemiValid()
    {
        std::ofstream file("semivalid.ini");
        file << "inval = error" << std::endl;
        file << "Nope!" << std::endl;
        file << "[Valid]" << std::endl;
        file << "value0 = 13" << std::endl;
    }

    void createInvalid()
    {
        std::ofstream file("invalid.ini");
        file << "invalid line" << std::endl;
        file << "[invalid ; section]" << std::endl;
        file << "technically = valid, but missing section" << std::endl;
        file << "][section?]" << std::endl;
    }
};


TEST_F(TestIni, TestParseValid)
{
    nox::util::IniFile ini;

    ASSERT_TRUE(ini.appendParse("valid.ini"));

    const nox::util::IniFile::Section *section = ini.getSection("Valid");
    ASSERT_NE(nullptr, section);

    ASSERT_EQ(45, section->getLong("value0", 0));
    ASSERT_EQ("45", section->getString("value0", "0"));

    ASSERT_EQ(3, section->getLong("value1", 0));
    ASSERT_NEAR(3.14f, section->getDouble("value1", 0.f), 0.01f);

    ASSERT_EQ(0, section->getLong("value2", 0));
    ASSERT_EQ("something something", section->getString("value2", ""));
    ASSERT_EQ("hey_guys$#", section->getString("value3", ""));
}

TEST_F(TestIni, TestParseOverride)
{
    nox::util::IniFile ini;

    ASSERT_TRUE(ini.appendParse("valid.ini"));
    ASSERT_TRUE(ini.appendParse("semivalid.ini"));

    // Value0 should now be overwritten by semivalid.ini
    ASSERT_EQ(13, ini.getSection("Valid")->getLong("value0", 0));
}

TEST_F(TestIni, TestParseOverrideAndSave)
{
    nox::util::IniFile orig;
    ASSERT_TRUE(orig.appendParse("valid.ini"));
    ASSERT_TRUE(orig.appendParse("semivalid.ini"));
    ASSERT_TRUE(orig.writeToFile());
    ASSERT_TRUE(orig.writeToFile("valid.ini"));

    nox::util::IniFile copy;
    ASSERT_TRUE(copy.appendParse("valid.ini"));
    ASSERT_EQ(13, copy.getSection("Valid")->getLong("value0", 0));
    ASSERT_EQ("hey_guys$#", copy.getSection("Valid")->getString("value3", ""));
}

TEST_F(TestIni, TestNewFile)
{
    nox::util::IniFile ini;

    ini.getSection("Section1")->setLong("value0", 50);
    ini.getSection("Section1")->setString("value1", "nei og nei");
    ini.getSection("Section2")->setString("value2", "interesting ;comment");

    ASSERT_FALSE(ini.writeToFile());
    ASSERT_TRUE(ini.writeToFile("out.ini"));

    nox::util::IniFile copy;
    ASSERT_TRUE(copy.appendParse("out.ini"));
    ASSERT_EQ(50, copy.getSection("Section1")->getLong("value0", 0));
    ASSERT_EQ("nei og nei", copy.getSection("Section1")->getString("value1", ""));
    ASSERT_EQ("interesting", copy.getSection("Section2")->getString("value2", ""));
}
