/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <memory>
#include <string>

#include <nox/app/net/Packet.h>
#include <nox/app/net/UserData.h>

#include <nox/common/types.h>
#include <nox/logic/event/Event.h>

#include <nox/app/net/NetworkOutEventListener.h>

using nox::app::net::Packet;
using nox::byte;


class TestNet: public ::testing::Test
{
protected:
	virtual void SetUp()
	{

	}

	virtual void TearDown()
	{

	}
};




template<typename T>
void testPacketBasicIO(T value)
{
    Packet pkt;
    const T in = value;
    T out;

    ASSERT_NE(in, out);

    ASSERT_NO_THROW(pkt << in);
    ASSERT_NO_THROW(pkt >> out);

    ASSERT_EQ(in, out);
}


TEST_F(TestNet, TestPacketBasicIO)
{
    testPacketBasicIO<unsigned>(42);
    testPacketBasicIO<int>(-42);
    testPacketBasicIO<byte>(200);
    testPacketBasicIO<float>(15.09);
    testPacketBasicIO<std::string>("Heisann sveisann");
}

TEST_F(TestNet, TestPacketArrayIO)
{
    Packet pkt;
    const std::vector<byte> input { 1, 2, 3, 4, 5 };
    std::vector<byte> output { 42 };

    ASSERT_EQ(5, input.size());
    ASSERT_EQ(1, output.size());

    ASSERT_NO_THROW(pkt << input);
    ASSERT_NO_THROW(pkt >> output);

    ASSERT_EQ(5, input.size());
    ASSERT_EQ(5, output.size());

    for (int i=0; i<5; i++)
        ASSERT_EQ(input[i], output[i]);
}

TEST_F(TestNet, TestPacketValidMulti)
{
    Packet pkt;

    const unsigned in0      = 15;
    const int in1           = 16;
    const float in2         = 17;
    const byte in3          = 18;
    const std::string in4   = "nitten, etc";

    unsigned out0           = 0;
    int out1                = 0;
    float out2              = 0;
    byte out3               = 0;
    std::string out4        = "";

    ASSERT_NO_THROW(pkt << in0 << in1 << in2 << in3 << in4);
    ASSERT_NO_THROW(pkt >> out0 >> out1 >> out2 >> out3 >> out4;);

    ASSERT_EQ(in0, out0);
    ASSERT_EQ(in1, out1);
    ASSERT_EQ(in2, out2);
    ASSERT_EQ(in3, out3);
    ASSERT_EQ(in4, out4);
}


template<typename Ta, typename Tb>
void testPacketInvalidTypes(Ta value)
{
    Packet pkt;
    Tb output;

    ASSERT_NO_THROW(pkt << value);
    ASSERT_ANY_THROW(pkt >> output);
}

TEST_F(TestNet, TestPacketInvalidTypes)
{
    testPacketInvalidTypes<unsigned,int>(42);
    testPacketInvalidTypes<unsigned,float>(42);
    testPacketInvalidTypes<unsigned,byte>(42);
    testPacketInvalidTypes<unsigned,std::string>(42);
    testPacketInvalidTypes<unsigned,std::vector<byte>>(42);

    testPacketInvalidTypes<int,unsigned>(42);
    testPacketInvalidTypes<int,float>(42);
    testPacketInvalidTypes<int,byte>(42);
    testPacketInvalidTypes<int,std::string>(42);
    testPacketInvalidTypes<int,std::vector<byte>>(42);

    testPacketInvalidTypes<float,unsigned>(42);
    testPacketInvalidTypes<float,int>(42);
    testPacketInvalidTypes<float,byte>(42);
    testPacketInvalidTypes<float,std::string>(42);
    testPacketInvalidTypes<float,std::vector<byte>>(42);

    testPacketInvalidTypes<byte,unsigned>(42);
    testPacketInvalidTypes<byte,int>(42);
    testPacketInvalidTypes<byte,float>(42);
    testPacketInvalidTypes<byte,std::string>(42);
    testPacketInvalidTypes<byte,std::vector<byte>>(42);

    testPacketInvalidTypes<std::string,unsigned>("hey ho");
    testPacketInvalidTypes<std::string,int>("hey ho");
    testPacketInvalidTypes<std::string,float>("hey ho");
    testPacketInvalidTypes<std::string,std::vector<byte>>("hey ho");
    testPacketInvalidTypes<std::string,byte>("hey ho");

    std::vector<byte> vec { 1, 2, 3, 4 };

    testPacketInvalidTypes<std::vector<byte>,unsigned>(vec);
    testPacketInvalidTypes<std::vector<byte>,int>(vec);
    testPacketInvalidTypes<std::vector<byte>,float>(vec);
    testPacketInvalidTypes<std::vector<byte>,std::string>(vec);
    testPacketInvalidTypes<std::vector<byte>,byte>(vec);
}

TEST_F(TestNet, TestPacketExhaustion)
{
    Packet pkt;

    const int input = 0xBADA55;
    int output = 0;

    ASSERT_NO_THROW(pkt << input);
    ASSERT_NO_THROW(pkt >> output);
    ASSERT_ANY_THROW(pkt >> output);
}

TEST_F(TestNet, TestPacketReset)
{
    Packet pkt;

    const int input = 0xBADA55;
    int output = 0;

    ASSERT_NO_THROW(pkt << input);
    ASSERT_NO_THROW(pkt >> output);
    ASSERT_EQ(input, output);

    output = 0;
    pkt.resetStream();

    ASSERT_NO_THROW(pkt >> output);
    ASSERT_EQ(input, output);
}

TEST_F(TestNet, TestVectorPacketSerialization)
{
    const glm::vec2 vecOut(15.3, -50.3);
    glm::vec2 vecIn(0.f, 0.f);

    Packet out;
    out << vecOut;

    int len = 0;
    byte *buf = out.serialize(len);

    Packet in;
    in.deserialize(buf, len);
    delete[] buf;

    in >> vecIn;

    ASSERT_NEAR(vecOut.x, vecIn.x, 0.001f);
    ASSERT_NEAR(vecOut.y, vecIn.y, 0.001f);
}



class CustomEvent: public nox::logic::event::Event
{
public:
    CustomEvent(int i, float f, std::string s, std::string type):
        Event(type),
        myInteger(i),
        myFloat(f),
        myString(s)
    {

    }

    int getInt()
    {
        return this->myInteger;
    }

    float getFloat()
    {
        return this->myFloat;
    }

    std::string getString()
    {
        return this->myString;
    }

protected:
    void doSerialize() const override
    {
        Event::doSerialize();
        (*this) << myInteger << myFloat << myString;
    }

    void doDeSerialize() override
    {
        Event::doDeSerialize();
        (*this) >> myInteger >> myFloat >> myString;
    }

private:
    int myInteger;
    float myFloat;
    std::string myString;
};

TEST_F(TestNet, TestEventPacketConversion)
{
    CustomEvent *outEvent = new CustomEvent(42, -1005.f, "hey ho", "firstEvent");
    CustomEvent *inEvent = new CustomEvent(90, 54.32f, "Please pass this test", "secondEvent");
    Packet packet;

    ASSERT_NE(outEvent->getInt(), inEvent->getInt());
    ASSERT_NE(outEvent->getFloat(), inEvent->getFloat());
    ASSERT_NE(outEvent->getString(), inEvent->getString());
    ASSERT_NE(outEvent->getType(), inEvent->getType());

    outEvent->serialize(&packet);
    inEvent->deSerialize(&packet);


    ASSERT_EQ(outEvent->getType(), inEvent->getType());
    ASSERT_EQ(outEvent->getInt(), inEvent->getInt());
    ASSERT_EQ(outEvent->getFloat(), inEvent->getFloat());
    ASSERT_EQ(outEvent->getString(), inEvent->getString());
}


TEST_F(TestNet, TestUserDataSerialization)
{
    const std::string inStr = "ti, neida, femten";
    const std::string outStr = "nope";

    nox::app::net::UserData in;
    in.setClientId(15);
    in.setUserName(inStr);
    ASSERT_EQ(inStr, in.getUserName());
    ASSERT_EQ(15, in.getClientId());

    nox::app::net::UserData out;
    out.setClientId(0);
    out.setUserName(outStr);
    ASSERT_EQ(outStr, out.getUserName());
    ASSERT_EQ(0, out.getClientId());

    nox::app::net::Packet pkt;
    in.serialize(&pkt);
    out.deSerialize(&pkt);

    ASSERT_EQ(in.getClientId(), out.getClientId());
    ASSERT_EQ(in.getUserName(), out.getUserName());
}


TEST_F(TestNet, TestOctalIpConv)
{
    using nox::app::net::octalToIp;

    ASSERT_EQ(0x7F000001, octalToIp("127.0.0.1"));
    ASSERT_EQ(0xC0A80201, octalToIp("192.168.2.1"));

    // Byte order validation
    ASSERT_EQ(0xFFFFFF00, octalToIp("255.255.255.0"));
    ASSERT_EQ(0xFFFF00FF, octalToIp("255.255.0.255"));
    ASSERT_EQ(0xFF00FFFF, octalToIp("255.0.255.255"));
    ASSERT_EQ(0x00FFFFFF, octalToIp("0.255.255.255"));

    // Nibble order validation
    ASSERT_EQ(0x0F0F0F0F, octalToIp("15.15.15.15"));
    ASSERT_EQ(0x10101010, octalToIp("16.16.16.16"));

    // Resolvable hostnames are not allowed.
    ASSERT_EQ(0, octalToIp("localhost"));

    // Bogus IP addresses are not allowed
    ASSERT_EQ(0, octalToIp("127.300.0.1"));
    ASSERT_EQ(0, octalToIp("127.0.0.0.1"));
    ASSERT_EQ(0, octalToIp("127.0.1"));
    ASSERT_EQ(0, octalToIp("127.0..1"));
    ASSERT_EQ(0, octalToIp("-127.0.0.1"));
    ASSERT_EQ(0, octalToIp("127.0.0.-1"));
    ASSERT_EQ(0, octalToIp("hi mom"));

}

