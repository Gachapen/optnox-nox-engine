/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_WINDOW_SDLWINDOWVIEW_H_
#define NOX_WINDOW_SDLWINDOWVIEW_H_

#include <nox/logic/View.h>
#include <nox/app/log/Logger.h>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_events.h>
#include <glm/vec2.hpp>
#include <string>

namespace nox
{

namespace app
{

class IContext;

}

namespace window
{

class NOX_API SdlWindowView: public logic::View
{
public:
	SdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, const bool enableOpenGl);

	virtual std::string getTypeString() const override;

	virtual bool initialize(logic::IContext* context) override;
	void destroy() override final;

	void setWindowSize(const glm::uvec2& size);
	const glm::uvec2& getWindowSize() const;

	virtual void onSdlEvent(const SDL_Event& event);

private:
	void update(const Duration& deltaTime);

	virtual bool onWindowCreated(SDL_Window* window);
	virtual void onWindowSizeChanged(const glm::uvec2& size);
	virtual void onMousePress(const SDL_MouseButtonEvent& event);
	virtual void onMouseRelease(const SDL_MouseButtonEvent& event);
	virtual void onMouseMove(const SDL_MouseMotionEvent& event);
	virtual void onMouseScroll(const SDL_MouseWheelEvent& event);
	virtual void onKeyPress(const SDL_KeyboardEvent& event);
	virtual void onKeyRelease(const SDL_KeyboardEvent& event);

	virtual void onUpdate(const Duration& deltaTime);
	virtual void onDestroy();

	app::IContext* applicationContext;

	app::log::Logger log;

	std::string windowTitle;
	glm::uvec2 windowSize;
	bool enableOpengl;

	SDL_Window* window;
	SDL_GLContext glContext;
};

}
}

#endif
