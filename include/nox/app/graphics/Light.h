/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_LIGHT_H_
#define NOX_APP_GRAPHICS_LIGHT_H_

#include <string>
#include <glm/glm.hpp>
#include "GeometrySet.h"

namespace nox { namespace app
{
namespace graphics
{

class Triangle;

struct LightRenderTriangle
{
	glm::vec2 lowerPoint;
	glm::vec2 upperPoint;

	float lowerAngle;
	float upperAngle;

	Triangle* geoemtry;
};

/**
 * Representation of a light.
 */
class Light
{
public:
	enum class RenderHint
	{
		DYNAMIC,
		STATIC
	};

	Light();

	void setZHeight(float height) const;
	void setRange(float range) const;
	void setPosition(const glm::vec2& position) const;
	void setPositionOffset(const glm::vec2& offset) const;
	void setColor(const glm::vec4& color) const;
	void setAttenuation(const glm::vec3& attenuation) const;
	void setEnabled(bool enabled);
	void setCastDirection(float directionAngle);

	const std::string& getName() const;
	void setName(const std::string& name);
	const glm::vec4& getColor() const;
	const glm::vec2& getPosition() const;
	const glm::vec2& getPositionOffset() const;
	const glm::vec3& getAttenuation() const;
	float getZHeight() const;
	float getRange() const;
	bool isEnabled() const;
	float getCastDirection() const;

	void setGeometryUpdated();
	void setGeometryOutdated();
	bool needsGeometryUpdate() const;

	bool needsRenderUpdate() const;
	void setRenderingUpdated();

	void setEffect(const bool state);
	bool isEffect() const;

	GeometrySet lightArea;
	std::vector<LightRenderTriangle> lightAreaTriangles;

	/**
	 * The alphaFalloff is used when rendering the light *without* taking into consideration
	 * the effect the light has on the surface material.
	 */
	float alphaFallOff;
	float coneAngleRadian;

	unsigned int numRays;

	bool isRadialLight;
	bool castShadows;
	bool shouldRotateWithPlayer;

	RenderHint renderHint;

private:
	std::string name;

	mutable glm::vec4 color;
	mutable glm::vec2 position;
	mutable glm::vec2 positionOffset;

	/**
	 * The attenuation vector is used when rendering the light's effect on the surface
	 * materials. The vector uses the "Constant-Linear-Quadratic Falloff" method, where
	 * X = constant term, Y = linear factor, Z = quadratic factor.
	 *
	 * This is a well-written article on the subject of CLQ-falloff:
	 * https://developer.valvesoftware.com/wiki/Constant-Linear-Quadratic_Falloff
	 */
	mutable glm::vec3 attenuation;

	/**
	 * The z-height attribute is used as the position of the light along the Z-axis when
	 * calculating normal values. The limited usage of this attribute is why it is its
	 * own member instead of making Light::position a glm::vec3.
	 */
	mutable float zHeight;

	mutable float range;
	float castDirectionRadian;

	bool enabled;
	mutable bool geometryChanged;
	mutable bool geometryOutdated;

	bool effect;
};

inline void Light::setName(const std::string &name)
{
	this->name = name;
}

inline const std::string& Light::getName() const
{
	return this->name;
}

inline void Light::setZHeight(float height) const
{
	this->zHeight = height;
}

inline void Light::setRange(float range) const
{
	this->range = range;
	this->geometryOutdated = true;
}

inline void Light::setPosition(const glm::vec2& position) const
{
	this->position = position;
	this->geometryOutdated = true;
}

inline void Light::setPositionOffset(const glm::vec2& offset) const
{
	this->positionOffset = offset;
	this->geometryOutdated = true;
}

inline void Light::setColor(const glm::vec4& color) const
{
	this->color = color;
	this->geometryOutdated = true;
}

inline void Light::setAttenuation(const glm::vec3& attenuation) const
{
	this->attenuation = attenuation;
}

inline void Light::setEnabled(bool enabled)
{
	if (this->enabled != enabled)
	{
		this->geometryOutdated = true;
	}

	this->enabled = enabled;
}

inline void Light::setCastDirection(float directionAngle)
{
	this->castDirectionRadian = directionAngle;
	this->geometryOutdated = true;
}

inline void Light::setGeometryUpdated()
{
	this->geometryOutdated = false;
	this->geometryChanged = true;
}

inline void Light::setGeometryOutdated()
{
	this->geometryOutdated = true;
}

inline bool Light::needsGeometryUpdate() const
{
	return this->geometryOutdated;
}

inline bool Light::needsRenderUpdate() const
{
	return this->geometryChanged;
}

inline void Light::setRenderingUpdated()
{
	this->geometryChanged = false;
}

inline const glm::vec4& Light::getColor() const
{
	return this->color;
}

inline const glm::vec3& Light::getAttenuation() const
{
	return this->attenuation;
}

inline const glm::vec2& Light::getPosition() const
{
	return this->position;
}

inline const glm::vec2& Light::getPositionOffset() const
{
	return this->positionOffset;
}

inline float Light::getZHeight() const
{
	return this->zHeight;
}

inline float Light::getRange() const
{
	return this->range;
}

inline bool Light::isEnabled() const
{
	return this->enabled;
}

inline float Light::getCastDirection() const
{
	return this->castDirectionRadian;
}

inline void Light::setEffect(const bool state)
{
	this->effect = state;
}

inline bool Light::isEffect() const
{
	return this->effect;
}

}
} }

#endif
