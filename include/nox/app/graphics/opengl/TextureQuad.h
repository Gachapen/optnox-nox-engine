/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TEXTUREQUAD_H_
#define NOX_APP_GRAPHICS_TEXTUREQUAD_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <nox/util/math/Box.h>

namespace nox { namespace app
{
namespace graphics
{

/**
 * Representation of a graphical sprite.
 */
class TextureQuad
{
public:
	/**
	 * A coordinate on a sprite.
	 * Contains both the geometry vertex to be rendered
	 * and the texture coordinate of the texture.
	 */
	struct VertexAttribute
	{
		using DataType = float;

		using Vec2Type = glm::tvec2<DataType, glm::precision::defaultp>;
		using Vec3Type = glm::tvec3<DataType, glm::precision::defaultp>;
		using Vec4Type = glm::tvec4<DataType, glm::precision::defaultp>;

		using PositionType = Vec4Type;
		using TextureCoordinateType = Vec4Type;
		using ColorType = Vec4Type;
		using LuminanceType = Vec4Type;

		using SizeType = glm::length_t;

		static SizeType positionSize();
		static SizeType textureCoordinateSize();
		static SizeType colorSize();
		static SizeType luminanceSize();

		static std::size_t positionOffsetBytes();
		static std::size_t textureCoordinateOffsetBytes();
		static std::size_t colorOffsetBytes();
		static std::size_t luminanceOffsetBytes();

		static void positionAttribPointer(const GLuint location);
		static void textureCoordinateAttribPointer(const GLuint location);
		static void colorAttribPointer(const GLuint location);
		static void luminanceAttribPointer(const GLuint location);

		VertexAttribute();

		void setPosition(const glm::vec2& position);
		void setPosition(const glm::vec3& position);
		void setPosition(const glm::vec4& position);

		void setTextureCoordinate(const glm::vec2& coordinate);

		void setColor(const glm::vec3& color);
		void setColor(const glm::vec4& color);

		void setLightMultiplier(const DataType multiplier);
		void setEmissiveLight(const DataType emissive);

		DataType* data();

		const PositionType& getPosition() const;
		const TextureCoordinateType& getTextureCoordinate() const;
		const ColorType& getColor() const;

	private:
		PositionType position;
		TextureCoordinateType textureCoordinate;
		ColorType color;
		LuminanceType lightLuminance;
	};

	/**
	 * Four Sprite::Coordinate representing a square.
	 */
	struct RenderQuad
	{
		void setColor(const glm::vec3& color);
		void setColor(const glm::vec4& color);

		void setLightMultiplier(const VertexAttribute::DataType multiplier);
		void setEmissiveLight(const VertexAttribute::DataType emissive);

		VertexAttribute::DataType* data();

		VertexAttribute bottomLeft;
		VertexAttribute bottomRight;
		VertexAttribute topRight;
		VertexAttribute topLeft;
	};

	TextureQuad();
	TextureQuad(unsigned int atlasId);

	void setRenderQuad(const TextureQuad::RenderQuad& quad);

	/**
	 * @return The atlas id of this sprite, or -1 if none is set.
	 */
	unsigned int getAtlasId() const;

	const TextureQuad::RenderQuad& getRenderQuad() const;
	float getWidth() const;
	float getHeight() const;

private:
	unsigned int atlasId;
	TextureQuad::RenderQuad renderQuad;
};

TextureQuad::RenderQuad transform(const TextureQuad::RenderQuad& quad, const glm::mat4& matrix);

TextureQuad::RenderQuad makeRenderQuadFromBox(const math::Box<glm::vec2>& box);


inline TextureQuad::VertexAttribute::SizeType TextureQuad::VertexAttribute::positionSize()
{
	return PositionType().length();
}

inline TextureQuad::VertexAttribute::SizeType TextureQuad::VertexAttribute::textureCoordinateSize()
{
	return TextureCoordinateType().length();
}

inline TextureQuad::VertexAttribute::SizeType TextureQuad::VertexAttribute::colorSize()
{
	return ColorType().length();
}

inline TextureQuad::VertexAttribute::SizeType TextureQuad::VertexAttribute::luminanceSize()
{
	return LuminanceType().length();
}

inline std::size_t TextureQuad::VertexAttribute::positionOffsetBytes()
{
	return offsetof(VertexAttribute, position);
}

inline std::size_t TextureQuad::VertexAttribute::textureCoordinateOffsetBytes()
{
	return offsetof(VertexAttribute, textureCoordinate);
}

inline std::size_t TextureQuad::VertexAttribute::colorOffsetBytes()
{
	return offsetof(VertexAttribute, color);
}

inline std::size_t TextureQuad::VertexAttribute::luminanceOffsetBytes()
{
	return offsetof(VertexAttribute, lightLuminance);
}

inline void TextureQuad::VertexAttribute::setPosition(const glm::vec2& position)
{
	this->position = PositionType(Vec4Type(position.x, position.y, 0.0f, 1.0f));
}

inline void TextureQuad::VertexAttribute::setPosition(const glm::vec3& position)
{
	this->position = PositionType(Vec4Type(position.x, position.y, position.z, 1.0f));
}

inline void TextureQuad::VertexAttribute::setPosition(const glm::vec4& position)
{
	this->position = PositionType(Vec4Type(position.x, position.y, position.z, position.w));
}

inline void TextureQuad::VertexAttribute::setTextureCoordinate(const glm::vec2& coordinate)
{
	this->textureCoordinate = TextureCoordinateType(Vec4Type(coordinate.x, coordinate.y, 0.0f, 0.0f));
}

inline void TextureQuad::VertexAttribute::setColor(const glm::vec3& color)
{
	this->color = ColorType(Vec4Type(color.r, color.g, color.b, 1.0f));
}

inline void TextureQuad::VertexAttribute::setColor(const glm::vec4& color)
{
	this->color = ColorType(Vec4Type(color.r, color.g, color.b, color.a));
}

inline void TextureQuad::VertexAttribute::setLightMultiplier(const DataType multiplier)
{
	this->lightLuminance.r = multiplier;
}

inline void TextureQuad::VertexAttribute::setEmissiveLight(const DataType emissive)
{
	this->lightLuminance.g = emissive;
}

inline TextureQuad::VertexAttribute::DataType* TextureQuad::VertexAttribute::data()
{
	return &this->position.x;
}

inline const TextureQuad::VertexAttribute::PositionType& TextureQuad::VertexAttribute::getPosition() const
{
	return this->position;
}

inline const TextureQuad::VertexAttribute::TextureCoordinateType& TextureQuad::VertexAttribute::getTextureCoordinate() const
{
	return this->textureCoordinate;
}

inline const TextureQuad::VertexAttribute::ColorType& TextureQuad::VertexAttribute::getColor() const
{
	return this->color;
}

inline void TextureQuad::VertexAttribute::positionAttribPointer(const GLuint location)
{
	const GLvoid* pointer(0x00);
	pointer = static_cast<const char*>(pointer) + TextureQuad::VertexAttribute::positionOffsetBytes();

	glVertexAttribPointer(
		location,
		TextureQuad::VertexAttribute::positionSize(),
		GL_FLOAT,
		GL_FALSE,
		sizeof(TextureQuad::VertexAttribute),
		pointer
	);
}

inline void TextureQuad::VertexAttribute::textureCoordinateAttribPointer(const GLuint location)
{
	const GLvoid* pointer(0x00);
	pointer = static_cast<const char*>(pointer) + TextureQuad::VertexAttribute::textureCoordinateOffsetBytes();

	glVertexAttribPointer(
		location,
		TextureQuad::VertexAttribute::textureCoordinateSize(),
		GL_FLOAT,
		GL_FALSE,
		sizeof(TextureQuad::VertexAttribute),
		pointer
	);
}

inline void TextureQuad::VertexAttribute::colorAttribPointer(const GLuint location)
{
	const GLvoid* pointer(0x00);
	pointer = static_cast<const char*>(pointer) + TextureQuad::VertexAttribute::colorOffsetBytes();

	glVertexAttribPointer(
		location,
		TextureQuad::VertexAttribute::colorSize(),
		GL_FLOAT,
		GL_FALSE,
		sizeof(TextureQuad::VertexAttribute),
		pointer
	);
}

inline void TextureQuad::VertexAttribute::luminanceAttribPointer(const GLuint location)
{
	const GLvoid* pointer(0x00);
	pointer = static_cast<const char*>(pointer) + TextureQuad::VertexAttribute::luminanceOffsetBytes();

	glVertexAttribPointer(
		location,
		TextureQuad::VertexAttribute::luminanceSize(),
		GL_FLOAT,
		GL_FALSE,
		sizeof(TextureQuad::VertexAttribute),
		pointer
	);
}

inline void TextureQuad::RenderQuad::setColor(const glm::vec3& color)
{
	this->bottomLeft.setColor(color);
	this->bottomRight.setColor(color);
	this->topRight.setColor(color);
	this->topLeft.setColor(color);
}

inline void TextureQuad::RenderQuad::setColor(const glm::vec4& color)
{
	this->bottomLeft.setColor(color);
	this->bottomRight.setColor(color);
	this->topRight.setColor(color);
	this->topLeft.setColor(color);
}

inline void TextureQuad::RenderQuad::setLightMultiplier(const VertexAttribute::DataType multiplier)
{
	this->bottomLeft.setLightMultiplier(multiplier);
	this->bottomRight.setLightMultiplier(multiplier);
	this->topRight.setLightMultiplier(multiplier);
	this->topLeft.setLightMultiplier(multiplier);
}

inline void TextureQuad::RenderQuad::setEmissiveLight(const VertexAttribute::DataType emissive)
{
	this->bottomLeft.setEmissiveLight(emissive);
	this->bottomRight.setEmissiveLight(emissive);
	this->topRight.setEmissiveLight(emissive);
	this->topLeft.setEmissiveLight(emissive);
}

inline TextureQuad::VertexAttribute::DataType* TextureQuad::RenderQuad::data()
{
	return this->bottomLeft.data();
}

}
} }

#endif
