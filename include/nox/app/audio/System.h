/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_SYSTEM_H_
#define NOX_APP_AUDIO_SYSTEM_H_

#include <list>
#include <memory>

namespace nox { namespace app
{

namespace resource
{

class Handle;

}

namespace audio
{

class Buffer;

/**
 * Provide audio capabilities.
 */
class System
{
public:
	System();
	virtual ~System();

	/**
	 * Initialize the system so that it is ready to use.
	 */
	virtual bool initialize() = 0;
	virtual void shutdown();

	void stopAllSounds();
	void pauseAllSounds();
	void resumeAllSounds();

	/**
	 * Initialize an audio buffer from a resource.
	 * @param handle Handle to resource to initialize from.
	 * @note Call releaseAudioBuffer to free the buffer after use.
	 * @return Pointer to audio buffer created.
	 */
	Buffer* createAudioBuffer(const std::shared_ptr<resource::Handle>& handle);

	/**
	 * Release an audio buffer.
	 * @pre Buffer must be created with createAudioBuffer.
	 * @param audioBuffer Pointer to the buffer to release.
	 */
	void releaseAudioBuffer(Buffer* audioBuffer);

private:
	using AudioBufferList = std::list<std::unique_ptr<Buffer>>;

	/**
	 * Create the Buffer instance to be initialized. Should be overridden as this is called
	 * from createAudioBuffer.
	 * @return Buffer created.
	 */
	virtual std::unique_ptr<Buffer> createBuffer() = 0;

	AudioBufferList buffers;
};

}
} }

#endif
