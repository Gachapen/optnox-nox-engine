/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_AUDIO_BUFFER_H_
#define NOX_APP_AUDIO_BUFFER_H_

#include <memory>

namespace nox { namespace app
{

namespace resource
{

class Handle;

}

namespace audio
{

/**
 * Buffer representing one sound.
 */
class Buffer
{
public:
	Buffer();
	virtual ~Buffer();

	/**
	 * Initialize the buffer from a resource.
	 * An initialized buffer will be automatically cleaned up from the destructor.
	 * @param handle Handle to resource to initialize from.
	 * @return If successfully initialized or not.
	 */
	virtual bool initialize(const std::shared_ptr<resource::Handle>& handle) = 0;

	void playAudio();
	void pauseAudio();
	void stopAudio();
	void resumeAudio();
	void enableLooping(const bool looping);

	bool isLooping() const;
	float getVolume() const;
	bool isPlaying();

	/**
	 * Set the volume of the buffer.
	 * Volume will be clamped to range [0, 1].
	 */
	void setVolume(const float vol);

private:
	virtual void handlePause() = 0;
	virtual void handleResume() = 0;
	virtual void handleVolumeChange(const float volume) = 0;
	virtual void handleLoopingChange(const bool looping) = 0;
	virtual void rewindPosition() = 0;
	virtual bool checkIfPlaying() const;

	bool playing;
	bool looping;
	float volume;

};

}
} }

#endif
