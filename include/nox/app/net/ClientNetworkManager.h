#ifndef NOX_APP_NET_CLIENTNETWORKMANAGER_H_
#define NOX_APP_NET_CLIENTNETWORKMANAGER_H_

#include <nox/app/net/NetworkManager.h>
#include <nox/app/net/LocalClient.h>
#include <nox/app/net/IPacketTranslator.h>
#include <nox/app/net/Conversation.h>

#include <nox/logic/IContext.h>


namespace nox { namespace app { namespace net {

class BroadcastReceiver;

namespace protocol
{
class JoinServerConversation;
}


class ClientNetworkManager: public NetworkManager
{
public:
	explicit ClientNetworkManager(IPacketTranslator *translator);
	~ClientNetworkManager();

	bool connectToServer(std::string ipaddr, Uint16 tcpPort, UserData &ud);
	bool connectToServer(IPaddress ipaddr, UserData &ud);
	bool isConnected() const;

	void setBroadcastListening(bool listen);

	bool initialize(nox::logic::IContext *context) override;
	void destroy() override;
	void update(const Duration& deltaTime) override;

	LocalClient* getClient();

protected:
	void sendSyncOutStateUpdates();

private:
	protocol::JoinServerConversation *joinConvo;
	LocalClient *client;
	BroadcastReceiver *broadcastRecv;
	std::string serverHostname;
	bool listenToBroadcasts;

	void handleJoinConvo();

	virtual void onConnectionStarted();
	virtual void onConnectionSuccess();
	virtual void onConnectionFailed(protocol::DcReason dc, std::string reason="", unsigned dur=0);
};

} } }


#endif
