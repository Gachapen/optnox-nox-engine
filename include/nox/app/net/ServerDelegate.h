#ifndef NOX_APP_NET_IDECISIONMANAGER_H_
#define NOX_APP_NET_IDECISIONMANAGER_H_

#include <nox/logic/event/Event.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>

#include <set>

namespace nox { namespace app { namespace net {

class ServerNetworkManager;
class ClientStats;
class RemoteClient;

class ServerDelegate
{
public:
	void setNetworkManager(ServerNetworkManager *netMgr);

	/**
	 * Whenever an unknown event is retrieved from the network, the Server
	 * Delegate is notified of the event, and the client that raised the event.
	 * The event will, without any action from the SD, be
	 * discarded and never seen again. The SD is free to respond to this event
	 * in any way it sees fit - but bear in mind that if no action is taken,
	 * absolutely nothing will happen.
	 */
	virtual void onClientNetworkEvent(ClientId raiser, const std::shared_ptr<nox::logic::event::Event> event) = 0;

	/**
	 * Retrieve the size of the lobby. This number is used when determining
	 * if there is room for another client.
	 *
	 * Ridiculously important: ALWAYS RETURN THE SAME VALUE WITHIN THE LIFE-SPAN
	 * OF YOUR ServerNetworkManager! To "Jimmy"-proof the relation, this method
	 * is only called once per ServerNetworkManager, but to keep things consistent
	 * with what you expect to happen, always return the same value.
	 *
	 * Obviously - return a non-zero value. Preferably, a value > 1.
	 */
	virtual unsigned getLobbySize() const = 0;

	/**
	 * Called after a client has been successfully accepted into the lobby. Game
	 * specific synchronization should be initiated here.
	 */
	virtual void onClientJoined(const ClientStats *client) = 0;

	/**
	 * Called after a client has been disconnected from the lobby for any reason.
	 */
	virtual void onClientLeft(const ClientStats *client) = 0;

	/**
	 * Called before onClientJoined by the ServerNetworkManager. This method ensures
	 * that all currently existing synchronized actors are created on the newly
	 * arrived client.
	 *
	 * This method must not be called by subclasses of ServerDelegate.
	 */
	void synchronizeExistingActors(ClientId clientId);

	/**
	 * Notify all hosts to create a specified synchronized actor.
	 *
	 * @return 0 if the owner is invalid, the SyncId (always > 1) of the new actor
	 *		 that will be created on success.
	 */
	SyncId createSynchronizedActor(std::string actor, ClientId owner);

	/**
	 * Destroy a single synchronized actor. All hosts will be notified to destroy
	 * the specified actor..
	 *
	 * @return True if the SyncId is valid, false otherwise.
	 */
	bool destroySynchronizedActor(SyncId syncId);

	/**
	 * Notify all hosts to destroy all actors owned by a specific player.
	 *
	 * @return The number of actors removed.
	 */
	int destroyAllSynchronizedActors(ClientId owner);

	/**
	 * Send a packet ONLY to a specified client.
	 */
	void sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to everyony BUT a specified client.
	 */
	void sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to everyone
	 */
	void sendToEveryone(const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	log::Logger& getLog();
	logic::IContext* getContext();

private:
	ServerNetworkManager *networkManager;
	logic::IContext *context;
	log::Logger log;

	std::map<ClientId,std::set<SyncId>> actorMap;
	std::map<SyncId,std::string> actorDefinitionMap;

	void raiseDestroySynchronizedActorEvent(ClientId owner, SyncId syncId);
};


} } }


#endif
