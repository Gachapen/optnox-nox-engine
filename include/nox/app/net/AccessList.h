#ifndef NOX_APP_NET_ACCESSLIST_H_
#define NOX_APP_NET_ACCESSLIST_H_

#include <nox/logic/IContext.h>
#include <nox/app/log/Logger.h>
#include <nox/common/types.h>
#include <nox/util/IniFile.h>

#include <SDL2/SDL_net.h>

#include <map>

namespace nox { namespace app { namespace net {


enum AccessType
{
	// The user in question is not listed.
	UNDEFINED	   = 0,

	// The user should be accepted without question.
	ALWAYS_ALLOW	= 1,

	// The user should be rejected without question. https://www.youtube.com/watch?v=qMLH4vzwJ4c&t=49
	ALWAYS_DENY	 = 2,
};

enum AccessFilter
{
	FILTER_IP	   = 0x01,
	FILTER_UNAME	= 0x02,
	FILTER_BOTH	 = 0xFF,
};


/**
 * AccessList is a joined functionality between a "blacklist" and a "whitelist".
 * Access may be filtered on username and/or IP. Games with "accounts" may use the
 * username approach quite safely, but games in which usernames are arbitrary should
 * use the IP access filter instead.
 *
 * The access list must be on the following format:
 *
 *	  <access.ini>
 *	  [IP]			; Regulations done on an IP-address basis
 *	  127.0.0.1=0	 ; = 0 means indefinite ban
 *	  192.168.2.2=1   ; = 1 means indefinite access
 *	  1.1.1.1=1548471 ; Any value bar 0 and 1 are UNIX timestamps for when the ban is lifted
 *
 *	  [UserName]	  ; Regulations done on a user name basis
 *	  lion=1		  ; "lion" may always join
 *	  1337hax0r=0	 ; 1337hax0r is banned indefinitely
 *	  mom=1548924845  ; mom may join in 2019
 *
 * Servers may regulate their behaviour by setting the default access type. A default
 * access type of ALWAYS_ALLOW means that only the blacklisted players are acted upon.
 * A default value of ALWAYS_DENY means that only explicitly whitelisted players are
 * accepted.
 */
class AccessList
{
public:
	AccessList(std::string file, AccessFilter filter);
	AccessList(logic::IContext *ctx, std::string file, AccessFilter filter);

	/**
	 * The AccessList will attempt to save the contents upon destruction. Obviously,
	 * if the program crashes and the dtor is never executed, the runtime modified contents
	 * are lost, unless AccessList::saveList() was called manually.
	 */
	~AccessList();

	/**
	 * Set the default value for clients which are not listed.
	 */
	void setDefaultAccess(AccessType access);

	AccessType getDefaultAccess() const;

	/**
	 * Get the pre-defined access type for a specific client. Based on the AccessFilter,
	 * one of the parameters (ipaddr or userName) may be ignored.
	 *
	 * If the user is banned for a timed period, ALWAYS_DENY will be returned.
	 *
	 * If there is a mismatch between the access for IP and username, the strictest
	 * regulation will apply.
	 */
	AccessType getAccessType(IPaddress ipaddr, std::string userName);


	void setAccess(IPaddress ipaddr, std::string userName, AccessType access);
	void setAccess(std::string userName, AccessType access);
	void setAccess(IPaddress ipaddr, AccessType access);

	void banWithDuration(IPaddress ipaddr, std::string userName, const Duration &duration);
	void banWithDuration(std::string userName, const Duration &duration);
	void banWithDuration(IPaddress ipaddr, const Duration &duration);

	/**
	 * The list may be reloaded at runtime. The existing content of the AccessList
	 * will be discarded.
	 */
	bool loadList();

	/**
	 * The list may be saved during runtime. By default, the list only saves itself
	 * in the destructor.
	 */
	bool saveList();

private:
	struct AccessRules
	{
		AccessType accessType;
		unsigned long banLift;
	};

	log::Logger log;
	util::IniFile iniFile;
	std::string fileName;
	AccessFilter filterType;
	AccessType defaultAccess;

	std::map<Uint32,AccessRules> ipAccess;
	std::map<std::string,AccessRules> unameAccess;


	void parseSection(AccessFilter filter);

	/**
	 * Returnes AccessRules::accessType, unless the rules in question is a timed ban which
	 * has expired, when AccessType::UNDEFINED is returned - this is an indicator that the
	 * AccessRules should be removed from the list.
	 */
	AccessType getAccessTypeForRules(AccessRules &rules);
};


} } }


#endif
