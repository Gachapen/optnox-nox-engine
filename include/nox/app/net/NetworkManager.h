#ifndef NOX_APP_NET_NETWORKMANAGER_H_
#define NOX_APP_NET_NETWORKMANAGER_H_

#include <nox/app/net/IPacketTranslator.h>
#include <nox/app/net/CommunicationController.h>
#include <nox/app/net/ClientStats.h>

#include <nox/logic/IContext.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/event/IBroadcaster.h>

namespace nox { namespace app { namespace net {


class NetworkManager
{
public:
	explicit NetworkManager(IPacketTranslator *translator);

	virtual bool initialize(nox::logic::IContext* context);
	virtual void destroy();
	virtual void update(const Duration& deltaTime);

	const std::vector<const ClientStats*> getConnectedClients() const;
	const ClientStats* getClientStats(ClientId client) const;
	bool isClientConnected(ClientId clientId) const;

	unsigned getClientsInLobbyCount() const;
	unsigned getLobbySize() const;

	nox::logic::IContext* getContext() const;

protected:
	virtual void sendSyncOutStateUpdates() = 0;

	void setCommunicationController(CommunicationController *comCont);

	/**
	 * Set the maximum number of players allowed in the lobby. This value can
	 * only be assigned once per NetworkManager.
	 */
	virtual void setLobbySize(unsigned size);

	log::Logger& getLogger();

	/**
	 * onClientConnected must be called by subclasses when a new client
	 * connects.
	 */
	virtual void onClientConnected(UserData ud);

	/**
	 * onClientDisconnected must be called by subclasses when a client
	 * disconnects, for *any* reason.
	 */
	virtual void onClientDisconnected(ClientId id);

	/**
	 * onClientPingUpdate must be called by subclasses whenever the round-trip
	 * duration has been re-calculated.
	 */
	virtual void onClientPingUpdate(ClientId id, unsigned ms);

	/**
	 * onEventPacketReceived broadcasts the event contained within the packet.
	 */
	void onEventPacketReceived(const Packet *packet);
	IPacketTranslator *packetTranslator;
private:
	nox::logic::IContext *context;
	log::Logger logger;
	CommunicationController *comControl;
	unsigned lobbySize;

	Duration syncOutFrequency;
	Duration syncOutTimer;

	// ClientStats are stored in a map for faster lookup when modifying the
	// contents. The cost of this comes when copying all the entires to a
	// vector in NetworkManager::getConnectedClients() - however, modifications
	// are more likely to occur frequently.
	std::map<ClientId,ClientStats*> clientStats;
};


} } }


#endif
