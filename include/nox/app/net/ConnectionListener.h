#ifndef NOX_APP_NET_CONNECTIONLISTENER_H_
#define NOX_APP_NET_CONNECTIONLISTENER_H_

#include <nox/app/net/SocketTcp.h>

#include <thread>
#include <mutex>

namespace nox { namespace app { namespace net {


/* Runs a background thread listening for new connections over TCP. 
 */
class ConnectionListener 
{
public:
	typedef void (ConnectionListener::*NewConnectionCallback)(TCPsocket);
	typedef void (ConnectionListener::*ErrorCallback)(int);

	ConnectionListener(Uint16 listenPort);
	~ConnectionListener();

	/**
	 * Check if the ConnectionListener is actively listening for connections
	 * without error.
	 *
	 * @return		  True if everything is OK, false otherwise.
	 */
	bool isGood();

	/**
	 * Check if the ConnectionListener has received a new connection.
	 *
	 * @return		  True if there are waiting connections, and a call to
	 *				  popNewConnection() 
	 */
	bool hasNewConnection();

	/**
	 * Retrieve a new TCP socket, initialized with the oldest connection.
	 *
	 * @return		  A valid, connected SocetTCP if there are waiting 
	 *				  sockets, NULL otherwise.
	 */
	SocketTcp* popNewConnection(logic::IContext *context);

	Uint16 getListenPort() const;

private:
	std::thread *thread;
	std::mutex mutex;
	Uint16 listenPort;

	/**
	 * Set by the ConnectionListener, read by the background worker thread. 
	 * If it is ever true, the background thread will exit.
	 */
	bool abortThreads;

	/**
	 * Set by the background worker thread by using a pointer to setError(int).
	 */
	int threadError;

	std::vector<TCPsocket> newConnections;
	

	/**
	 * Called by hte background worker thread when an error has occurred.
	 */
	void setError(int error);

	/**
	 * Called by the background worker thread when a new connection is 
	 * available. 
	 */
	void addNewSocket(TCPsocket);
};


} } }

#endif
