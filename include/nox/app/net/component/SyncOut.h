#ifndef NOX_APP_NET_SYNCOUT_H_
#define NOX_APP_NET_SYNCOUT_H_

#include <nox/logic/actor/Component.h>

#include <set>

namespace nox { namespace app { namespace net
{

class Packet;


class SyncOut: public logic::actor::Component
{
public:
	static const logic::actor::Component::IdType NAME;
	static const std::string PACKET_HEADER;
	static const std::string PACKET_FOOTER;

	SyncOut();

	const IdType& getName() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;

	/**
	 * Packs network data from all registered output components. All output
	 * components are asked to pack their data via Component::serialize(Packet&).
	 */
	void packNetworkData(app::net::Packet &packet);
};


} } }


#endif
