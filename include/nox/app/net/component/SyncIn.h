#ifndef NOX_APP_NET_SYNCIN_H_
#define NOX_APP_NET_SYNCIN_H_

#include <nox/logic/actor/Component.h>

#include <set>

namespace nox { namespace app { namespace net
{

class Packet;


/**
 * SyncIn received SyncOut packets from the remote SyncOut component and applies its sibling
 * components states transmitted across the network to its sibling components.
 *
 * SyncIn does not blindly apply component states. Only components which has been marked as
 * "synchronized" are applied. Unregistered component states are ignored.
 */
class SyncIn: public logic::actor::Component
{
public:
	static const logic::actor::Component::IdType NAME;

	SyncIn();

	const IdType& getName() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;

	/**
	 * Unpack network data. Component states defined in the Packet are
	 * given to the sibling Components via Component::deSerialize(Packet&).
	 *
	 * The Packet must have read the packet's SyncId, meaning that the next
	 * element in the packet output stream is either a Component's IdType or
	 * SyncOut::PACKET_FOOTER.
	 */
	void unpackNetworkData(const app::net::Packet &packet) const;
};


} } }


#endif
