#ifndef NOX_APP_NET_REMOTECLIENT_H_
#define NOX_APP_NET_REMOTECLIENT_H_


#include <nox/app/net/CommunicationController.h>
#include <nox/app/net/UserData.h>


namespace nox { namespace app { namespace net {

class SyncIn;
class ServerDelegate;

class RemoteClient : public CommunicationController
{
public:
	RemoteClient(nox::logic::IContext *context, UserData user, SocketTcp *tcp, SocketUdp *udp);

	virtual bool update(const Duration& deltaTime) override;

	void setServerDelegate(ServerDelegate *delegate);

protected:
	virtual void onActorCreated(logic::actor::Actor* actor) override;
	void onNetworkEvent(const Packet& packet);

private:
	ServerDelegate *serverDelegate;
};


} } }

#endif
