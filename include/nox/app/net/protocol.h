#ifndef NOX_APP_NET_PROTOCOL_H_
#define NOX_APP_NET_PROTOCOL_H_

/* protocol.h and protocol.cpp defines commonly used networking functions, enumerations
 * and constants.
 */

#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <map>
#include <vector>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>


namespace nox { namespace app { namespace net {

const Uint16 SERVER_BROADCAST_PORT_NO = 9090;
const Duration SERVER_BROADCAST_INTERV = Duration(std::chrono::seconds(2));

/**
 * Transport Layer Protocol global identification enumeration.
 */
enum class TLProtocol
{
	UDP,
	TCP,
};

/**
 * If there ever is any ambiguity of which direction an event, a message or a
 * packet is going, use this enum to explicitly state the direction.
 */
enum class NetworkDirection
{
	// The destination is a remote host.
	NET_OUT,

	// The destination is localhost.
	NET_IN,
};

enum class FieldType: byte
{
	UNDEFINED,			  // If ever encountered, an error has occurred!
	UINT32,				 // Unsigned 32 bit integer
	INT32,				  // Signed 32 bit integer
	FLOAT32,				// 32 bit float
	BYTE,				   // 8 bit unsigned
	STRING,				 // C string
	ARRAY,				  // Binary array
	VEC2,				   // glm::vec2
};


/**
 * The lag compensation algorithm is a *guideline* set in Actor for synchronized
 * Components.
 */
enum class LagCompensation
{
	NONE,
};


/**
 * Struct that defines certain attributes of a server, and how a the server
 * can be reached.
 */
struct ServerConnectionInfo
{
	std::string appId;
	std::string versionId;
	std::string serverName;
	unsigned connectedClients;
	unsigned maxClients;
	std::string hostname;
	Uint16 tcpPort;
};


namespace protocol
{

/**
 * All packets sent across the network must have an unsigned int as it's first
 * field - this is the packet identifier. If the ID is unknown, the packet must
 * be disregarded
 */
enum PacketId
{
	// The values of the PacketId enumerations should be created using your
	// favorite editors GUID-generator. The number must under no circumstances
	// consistently collide with any other 32 bit value transmitted across the
	// network.
	JOIN_REQUEST		= 0xe7e889de,
	JOIN_RESPONSE	   = 0x6cfe9c18,

	HEARTBEAT_SYN	   = 0xa0fd7379,
	HEARTBEAT_SYNACK	= 0x50354522,
	HEARTBEAT_ACK	   = 0xcc1ae05c,

	SERVER_BROADCAST	= 0x2e251827,

	EVENT_PACKET		= 0x65766e47,
	SYNC_PACKET		 = 0xe6e93e07,
};

enum DcReason
{
	UNKNOWN,

	// The client willyfully (or accidentally) closed the connection to
	// the server.
	CONNECTION_LOST,

	// The server forcefully removed the client. The client will be
	// unable to join for a defined number of seconds.
	BAN_TIMED,

	// The server forcefully removed the client. The client will not
	// be able to join without the admin of the server manually
	// removing him from the blacklist.
	BAN_INDEFINITE,
};

}


std::string fieldTypeToStr(FieldType type);

LagCompensation stringToLagCompensation(std::string str);

/**
 * Safe function for testing the string-length of a buffer that cannot be
 * trusted to contain a terminating null-character. The function checks
 * "maxlen" bytes for a null-character.
 *
 * @param str		   The untrusted string-buffer
 * @param maxlen		The maximum allowed length of the string
 * @return			  If a null-character is found, the number of preceding
 *					  bytes is returned. -1 is returned if the buffer does
 *					  not contain a terminating null character.
 */
int volatileStrlen(const char *str, int maxlen);

/**
 * Convert the IP address to octal string notation. The port is ignored
 * by this function.
 *
 * The "host" memeber of ipaddr must be in network byte order - unless
 * you created the IPaddress instance yourself, this is almost always the
 * case.
 */
std::string octalIpAddress(const IPaddress &ipaddr);

/**
 * Converts an IP address on octal notation to a 32 bit Uint in network
 * or host byte order (depending on the value of convertToNetOrder).
 *
 * No warning should be required, but one is given anyway; if you even
 * remotely intend to put the returned value into an IPaddress-struct,
 * you *MUST* have first converted the returned value to network byte
 * order. 1.0.0.127 is rarely a valid host...
 *
 * Returns 0 if the format of the string is invalid.
 */
Uint32 octalToIp(std::string octal, bool convertToNetOrder=false);

int readInt(const byte *buf);

unsigned readUint(const byte *buf);

float readFloat(const byte *buf);

unsigned networkDword(void *ptr);


} } }

#endif

