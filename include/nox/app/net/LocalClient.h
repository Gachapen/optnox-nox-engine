#ifndef NOX_APP_NET_LOCALCLIENT_H_
#define NOX_APP_NET_LOCALCLIENT_H_


#include <nox/app/net/CommunicationController.h>
#include <nox/app/net/UserData.h>


namespace nox { namespace app { namespace net
{

/**
 * LocalClient represents the connection FROM a client TO a server. All communication
 * related to all remote (and local) actors are passed through this object.
 */
class LocalClient : public CommunicationController
{
public:
	LocalClient(nox::logic::IContext *context, UserData user,
				SocketTcp *tcp, SocketUdp *udp);

	virtual bool update(const Duration& deltaTime) override;

protected:
	void onNetworkEvent(const Packet& packet);

private:
};


} } }

#endif
