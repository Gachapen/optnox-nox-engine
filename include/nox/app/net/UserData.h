#ifndef NOX_APP_NET_USERDATA_H_
#define NOX_APP_NET_USERDATA_H_


#include <nox/app/net/Packet.h>

#include <string>


namespace nox { namespace app { namespace net {


/**
 * UserData contains the most common attributes that identify a player. The
 * class is *NOT* extendible - if more data is required than what is defined
 * here, you must find another way of distributing it.
 */
class UserData
{
public:
	UserData();

	void setUserName(std::string userName);
	std::string getUserName() const;

	void setClientId(ClientId id);
	ClientId getClientId() const;

	/**
	 * Serialize the contents of the class into a Packet.
	 */
	void serialize(Packet *packet) const;

	/**
	 * Deserialize the contents of a Packet into this UserData.
	 *
	 * @return  True on valid deserialization, false if the data is malformed
	 *		  in *any* way.
	 */
	bool deSerialize(const Packet *packet);


private:
	std::string userName;
	ClientId clientId;
};


} } }


#endif
