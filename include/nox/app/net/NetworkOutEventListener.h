#ifndef NOX_APP_NET_NETWORKOUTEVENTLISTENER_H_
#define NOX_APP_NET_NETWORKOUTEVENTLISTENER_H_

#include <nox/app/net/NetworkManager.h>
#include <nox/app/net/Packet.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/net/event/NetworkOutEvent.h>
#include <nox/app/net/protocol.h>


namespace nox { namespace app { namespace net {

class NetworkOutEventListener: logic::event::IListener
{
public:
	NetworkOutEventListener(bool server, NetworkManager *netMgr);
	void setServer();
	void setClient();
	~NetworkOutEventListener();
protected:
	void onEvent(const std::shared_ptr<logic::event::Event>& event) override;
	bool isServer;
	void sendPacketAsServer(const Packet* packet, TLProtocol protocol);
	void sendPacketAsClient(const Packet* packet, TLProtocol protocol);
private:
	NetworkManager *netMgr;
	nox::app::log::Logger log;
	logic::event::ListenerManager eventListener;
	logic::event::IBroadcaster* eventBroadcaster;
};

} } }


#endif
