#ifndef NOX_APP_NET_BROADCASTRECEIVER_H_
#define NOX_APP_NET_BROADCASTRECEIVER_H_

#include <nox/app/net/SocketUdp.h>
#include <nox/app/net/protocol.h>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>

namespace nox { namespace app { namespace net {


class BroadcastReceiver
{
public:
	BroadcastReceiver(logic::IContext *context);
	~BroadcastReceiver();

	void update(const Duration& deltaTime);

	std::vector<ServerConnectionInfo> getServerList() const;

private:
	struct ServerBroadcast
	{
		struct ServerConnectionInfo serverInfo;
		Duration age;
	};

	SocketUdp *socket;
	std::map<Uint32,ServerBroadcast*> servers;
	logic::IContext *context;
	log::Logger logger;


	void ageBroadcasts(const Duration& deltaTime);
	void readNewBroadcasts();
	void removeOldBroadcasts();

	void raiseBroadcastEvent(ServerConnectionInfo info, bool online);
};


} } }

#endif

