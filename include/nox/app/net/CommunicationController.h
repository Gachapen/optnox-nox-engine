#ifndef NOX_APP_NET_COMMUNICATIONCONTROLLER_H_
#define NOX_APP_NET_COMMUNICATIONCONTROLLER_H_

#include <nox/app/net/SocketTcp.h>
#include <nox/app/net/SocketUdp.h>
#include <nox/app/net/protocol.h>
#include <nox/app/net/UserData.h>
#include <nox/app/net/IPacketTranslator.h>
#include <nox/app/net/DefaultPacketTranslator.h>

#include <nox/logic/IContext.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>

#include <functional>

namespace nox {

namespace logic {
namespace actor {

class Actor;

}
}

namespace app { namespace net {

class Conversation;
class SyncOut;
class SyncIn;


// Subclasses of CommunicationController may register themselves for callbacks
// through the PacketHandler lambda-type.
using PacketHandler = std::function<void(const Packet&)>;


/**
 * CommunicationController contains general logic upon which actual protocols can
 * be realized.
 */
class CommunicationController: public logic::event::IListener
{
public:
	CommunicationController(nox::logic::IContext *context, UserData ud,
							SocketTcp *tcp, SocketUdp *udp);
	virtual ~CommunicationController();

	virtual bool update(const Duration& deltaTime);

	virtual bool isGood();

	bool sendPacket(const Packet *packet, TLProtocol protocol);

	const UserData& getUserData() const;
	IPaddress getIpAddress(TLProtocol protocol);

	log::Logger& getLogger();

	void onEvent(const std::shared_ptr<logic::event::Event>& event) override;

	void setCustomPacketTranslator(IPacketTranslator* translator);
	std::shared_ptr<logic::event::Event> translatePacket(const Packet& packet);

	void packSyncOutUpdates(Packet& packet);
	void onSyncInUpdate(const Packet& packet);

protected:
	bool disconnect(TLProtocol protocol);

	virtual void handleIncomingPackets();

	virtual void onActorRemoved(logic::actor::Actor* actor);
	virtual void onActorCreated(logic::actor::Actor* actor);

	/**
	 * Add a Conversation object to this CommunicationController. The Conversation
	 * will be updated until it flags itself as "done", at which point it will be
	 * deleted by the CommunicationController.
	 */
	void addConversation(Conversation* conversation);
	void updateConversations();

	nox::logic::IContext* getContext();

	SocketBase* getSocket(TLProtocol protocol);
	SocketUdp* getSocketUdp();
	SocketTcp* getSocketTcp();

	void addPacketHandler(protocol::PacketId, PacketHandler handler);

private:
	logic::IContext *context;
	logic::event::ListenerManager eventListener;

	SocketUdp *udpSocket;
	SocketTcp *tcpSocket;

	std::list<Conversation*> conversations;
	std::map<protocol::PacketId,PacketHandler> packetHandlers;

	UserData userData;
	log::Logger logger;

	IPacketTranslator *customPacketTranslator;
	DefaultPacketTranslator defaultPacketTranslator;

	std::map<SyncId,SyncOut*> syncOutComps;
	std::map<SyncId,SyncIn*> syncInComps;
};


} } }

#endif
