#ifndef NOX_APP_NET_BROADCASTSENDER_H_
#define NOX_APP_NET_BROADCASTSENDER_H_

#include <nox/app/net/SocketUdp.h>
#include <nox/app/net/Packet.h>

#include <nox/logic/IContext.h>
#include <nox/app/log/Logger.h>

namespace nox { namespace app { namespace net {

class NetworkManager;

/**
 * BroadcastSender dispatches a broadcast on a semi-regular interval. The packet
 * is broadcasted on the local network's broadcast channel (255.255.255.255) on
 * port SERVER_BROADCAST_PORT_NO.
 *
 * The packets sent contains the local hostname of the server, along with the
 * port on which clients may connect.
 */
class BroadcastSender
{
public:
	BroadcastSender(Uint16 tcpPort, logic::IContext *context, NetworkManager *nv);
	~BroadcastSender();

	void setBroadcastInterval(const Duration &interval);

	void update(const Duration &deltaTime);

private:
	logic::IContext *context;
	Duration interval;
	Duration accumulatedTime;
	Uint16 tcpPort;
	SocketUdp *socket;
	NetworkManager *netMgr;
	log::Logger logger;

	void sendBroadcast();
};

} } }

#endif
