#ifndef NOX_APP_NET_CREATESYNCHRONIZED_H_
#define NOX_APP_NET_CREATESYNCHRONIZED_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace app { namespace net { namespace event
{


class CreateSynchronized: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	CreateSynchronized(std::string actor, ClientId owner, SyncId sync);
	CreateSynchronized(const Packet* packet);

	std::string getActorName() const;
	ClientId getOwningClientId() const;
	SyncId getSyncId() const;

	void doSerialize() const override;
	void doDeSerialize() override;

private:
	std::string actorName;
	ClientId ownerClient;
	SyncId syncId;
};


}
} } }


#endif
