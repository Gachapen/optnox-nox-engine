#ifndef NOX_APP_NET_CLIENTCONNECTED_H_
#define NOX_APP_NET_CLIENTCONNECTED_H_

#include <nox/logic/event/Event.h>
#include <nox/app/net/UserData.h>

namespace nox { namespace app { namespace net { namespace event
{

class ClientConnected: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ClientConnected(UserData userData);
	ClientConnected(const Packet* packet);

	const UserData& getUserData() const;

	void doSerialize() const override;
	void doDeSerialize() override;

private:
	UserData userData;
};

}
} } }

#endif
