#ifndef NOX_APP_NET_CONNECTIONSUCCESS_H_
#define NOX_APP_NET_CONNECTIONSUCCESS_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace app { namespace net { namespace event
{

class ConnectionSuccess: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ConnectionSuccess(std::string hostName, UserData userData);

	std::string getServerHostname() const;
	const UserData& getUserData() const;

private:
	std::string serverHostname;
	UserData userData;
};

}
} } }

#endif
