#ifndef NOX_APP_NET_CONNECTIONSTARTED_H_
#define NOX_APP_NET_CONNECTIONSTARTED_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace app { namespace net { namespace event
{

class ConnectionStarted: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ConnectionStarted(std::string hostname);

	std::string getServerHostname() const;

private:
	std::string serverHostName;
};

}
} } }

#endif
