#ifndef NOX_APP_NET_DESTROYSYNCHRONIZED_H_
#define NOX_APP_NET_DESTROYSYNCHRONIZED_H_

#include <nox/logic/event/Event.h>


namespace nox { namespace app { namespace net { namespace event {

class DestroySynchronized: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	DestroySynchronized(ClientId owner, SyncId id);
	DestroySynchronized(const Packet* packet);

	ClientId getOwningClientId() const;
	SyncId getSyncId() const;

	void doSerialize() const override;
	void doDeSerialize() override;

private:
	ClientId ownerClient;
	SyncId syncId;
};

} } } }

#endif
