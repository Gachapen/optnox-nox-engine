#ifndef NOX_APP_NET_CONNECTIONFAILED_H_
#define NOX_APP_NET_CONNECTIONFAILED_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace app { namespace net { namespace event
{

/**
 * ConnectionFailed is raised whenever the client disconnects from the server, *FOR ANY REASON*.
 * The event is also raised if the connection failed for reasons such as the host being
 * unreachable.
 */
class ConnectionFailed: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	ConnectionFailed(std::string hostname, protocol::DcReason dc,
					 std::string reason = "", unsigned dur = 0);

	std::string getServerHostname() const;

	/**
	 * Return types and their meaning:
	 * UNKNOWN: The server is unreachable or the connection failed for some other reason
	 * CONNECTION_LOST: The client was connected, but the connection got interrupted.
	 * BAN_TIMED: The client was kicked or banned for a discrete time period.
	 * BAN_INDEFINITE: The client was banned indefinitely.
	 */
	protocol::DcReason getDisconnectType() const;

	/**
	 * Only valid if getDisconnectType() is BAN_TIMED or BAN_INDEFINITE.
	 */
	std::string getKickReason() const;

	/**
	 * Only valid if getDisconnectType() is BAN_TIMED.
	 */
	unsigned getBanDuration() const;

private:
	std::string serverHostname;
	protocol::DcReason dcType;
	std::string kickReason;
	unsigned banDuration;
};

}
} } }

#endif
