#ifndef NOX_APP_NET_SERVERBROADCASTEVENT_H_
#define NOX_APP_NET_SERVERBROADCASTEVENT_H_

#include <nox/app/net/protocol.h>
#include <nox/logic/event/Event.h>

namespace nox { namespace app { namespace net { namespace event {


/**
 * ServerBroadcastEvent is raised whenever the BroadcastReceiver is made aware
 * of a new, broadcasting server, or when a previously broadcasting server has
 * ceased broadcasting.
 */
class ServerBroadcastEvent: public logic::event::Event
{
public:
	static const IdType ID;

	ServerBroadcastEvent(bool online, ServerConnectionInfo sci);

	bool getOnline() const;
	ServerConnectionInfo getServerConnectionInfo() const;

private:
	bool online;
	ServerConnectionInfo serverInfo;
};


} } } }


#endif
