/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_NET_EVENT_NETWORKOUTEVENT_H_
#define NOX_APP_NET_EVENT_NETWORKOUTEVENT_H_

#include <nox/logic/event/Event.h>
#include <nox/app/net/protocol.h>

namespace nox { namespace app { namespace net { namespace event {

class NetworkOutEvent: public logic::event::Event
{
public:
	static const logic::event::Event::IdType ID;

	NetworkOutEvent(logic::event::Event* inEvent, TLProtocol protocol = TLProtocol::TCP);
	virtual ~NetworkOutEvent();

	logic::event::Event* getContainedEvent() const;
	
	TLProtocol getProtocol();

private:
	TLProtocol protocol;
	logic::event::Event* containedEvent;
};

}
}
}
}

#endif
