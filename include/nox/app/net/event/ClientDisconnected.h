#ifndef NOX_APP_NET_CLIENTDISCONNECTEDEVENT_H_
#define NOX_APP_NET_CLIENTDISCONNECTEDEVENT_H_

#include <nox/app/net/protocol.h>
#include <nox/logic/event/Event.h>


namespace nox { namespace app { namespace net { namespace event {


class ClientDisconnected: public logic::event::Event
{
public:
	static const IdType ID;

	ClientDisconnected(ClientId id, protocol::DcReason dcr, unsigned durSec=0, std::string banReason="");
	ClientDisconnected(const Packet* packet);

	void doSerialize() const override;
	void doDeSerialize() override;

	ClientId getClientId() const;
	protocol::DcReason getDisconnectReason() const;

	// Only applicable if the DcReason is BAN_TIMED.
	unsigned getBanDuration() const;

	// Only applicable if the DcReason is BAN_TIMED or BAN_INDEFINITE.
	std::string getBanReason() const;

private:
	ClientId clientId;
	protocol::DcReason dcReason;
	unsigned banDuration;
	std::string banReason;
};


} } } }


#endif
