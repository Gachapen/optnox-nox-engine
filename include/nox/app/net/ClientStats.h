#ifndef NOX_APP_NET_CLIENTSTATS_H_
#define NOX_APP_NET_CLIENTSTATS_H_


#include <nox/app/net/UserData.h>


namespace nox { namespace app { namespace net {


/**
 * ClientStats contains details about a client connected to the lobby.
 * This data is intended for use only by the "game space". This class is not
 * used in the engine for other purpopses than providing the game with 
 * details on the connected clients.
 */
class ClientStats
{
public:
	explicit ClientStats(UserData ud);
	ClientStats(const ClientStats &other);

	/**
	 * Retrieve the UserData associated with this client.
	 */
	const UserData& getUserData() const;

	/**
	 * Retrieve the round-trip duration in milliseconds.
	 */
	unsigned getPing() const;

	void setPing(unsigned ms);

private:
	UserData userData;
	unsigned msPing;
};


} } }

#endif
