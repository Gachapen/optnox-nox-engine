#ifndef NOX_APP_NET_COMMON_CONVO_H_
#define NOX_APP_NET_COMMON_CONVO_H_

#include <nox/app/net/Conversation.h>

#include <nox/app/net/SocketBase.h>
#include <nox/app/net/SocketUdp.h>
#include <nox/app/net/SocketTcp.h>

#include <nox/app/net/LocalClient.h>
#include <nox/app/net/RemoteClient.h>

#include <nox/app/net/ServerNetworkManager.h>
#include <nox/app/net/ServerDelegate.h>

#include <nox/app/net/Packet.h>
#include <nox/app/net/protocol.h>


namespace nox { namespace app { namespace net {

class ServerNetworkManager;

namespace protocol {


/**
 * Conversation executed on the client side to connect to a remote server.
 * The conversation is initiated by the client.
 *
 * If the connection was successul, the LocalClient instance *MUST* be
 * retrieved, as it will not be deleted by the JoinServerConversation.
 *
 * If the server does not accept the connection, the passed SocketTcp instance
 * is deleted by the conversation.
 *
 * The server counterpart is AcceptClientConversation.
 */
class JoinServerConversation: public Conversation
{
public:
	JoinServerConversation(nox::logic::IContext *ctx,
						   SocketTcp *tcp,
						   UserData &user);
	virtual ~JoinServerConversation();

	bool start() override final;
	bool update() override final;
	bool isDone() override final;

	// Was the connection successful?
	bool wasAccepted() const;

	unsigned getLobbySize() const;

	/**
	 * Retrieve the initiated UDP socket. This method only returns a non-nil
	 * value when the server accepted us.
	 */
	LocalClient* getLocalClient();

private:
	UserData userData;
	SocketTcp *tcpSocket;
	SocketUdp *udpSocket;
	LocalClient *localClient;
	unsigned lobbySize;

	bool init;
	bool sentTcp;
	bool sentUdp;

	bool accepted;
	bool done;
	bool error;

	bool sendTcpRequest();

	// Returns true if a TCP JOIN_RESPONSE was received, we were accepted, and the
	// UDP socket was created successfully. Other flags, such as "done" may be flagged
	// by this method to indicate to the handler that we failed.
	bool handleTcpResponse();

	bool sendUdpRequest();
	void handleUdpResponse();
};


/**
 * Conversation executed on the server side after a new host has connected to
 * the server.
 *
 * If the connection was successul, the SocketUdp instance *MUST* be
 * retrieved, as it will not be deleted by the AcceptClientConversation.
 *
 * If the server does not accept the connection, the passed SocketTcp instance
 * is deleted by the conversation.
 *
 * The client counterpart is JoinServerConversation.
 */
class AcceptClientConversation: public Conversation
{
public:
	AcceptClientConversation(ServerNetworkManager *mgr, nox::logic::IContext* ctx,
							 SocketTcp *tcp);
	virtual ~AcceptClientConversation();

	bool update() override final;
	bool isDone() override final;

	bool acceptedClient() const;
	RemoteClient* getRemoteClient();

private:
	ServerNetworkManager *serverMgr;
	SocketTcp *tcpSocket;
	SocketUdp *udpSocket;
	RemoteClient *remoteClient;
	UserData userData;

	bool recvTcpRequest;
	bool recvUdpRequest;

	bool done;
	bool accepted;

	bool handleTcpRequest();
	bool sendTcpResponse(bool response);
	bool handleUdpRequest();
	bool sendUdpResponse();
};

}
} } }

#endif
