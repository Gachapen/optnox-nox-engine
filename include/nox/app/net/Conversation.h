#ifndef NOX_APP_NET_ICONVERSATION_H_
#define NOX_APP_NET_ICONVERSATION_H_

#include <nox/app/net/SocketBase.h>
#include <nox/app/net/Packet.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>

namespace nox { namespace app { namespace net {

/**
 * Conversations are two or more coherent messages between two hosts flowing in
 * both directions. Conversation is an interface to abstract away the handling
 * of packets which belong strictly in a specific conversation.
 *
 * To implement a conversation, the two parts involved should use two different
 * implementations of Conversations. For example, the conversation in which a
 * client wishes to join a server: the client *starts* the conversation with a
 * JoinServerConversation-instance. The server responds in a
 * AcceptClientConversation. The two Conversations needn't share any common
 * ancestor or functionality. Although their functionality may at first *seem*
 * similar, it rarely actually is.
 */
class Conversation
{
public:
	Conversation(SocketBase *socket, logic::IContext *context);
	virtual ~Conversation() = default;

	/**
	 * Conversations should only be started from one of the engaged hosts.
	 */
	virtual bool start();

	/**
	 * Implementations do their work and waiting for packets in the update
	 * method.
	 */
	virtual bool update() = 0;

	/**
	 * The conversation should be fully discarded after the first occurrence
	 * of this method returning true.
	 */
	virtual bool isDone() = 0;

	log::Logger& getLogger();
	logic::IContext* getContext();

protected:
	/**
	 * Find a packet in the SocketBase in which the first field of the packet
	 * holds the value "id".
	 */
	template<typename IdType>
	Packet* findPacket(IdType id);

	template<typename IdType>
	Packet* findPacket(SocketBase *socket, IdType id);

	void createLogger(std::string name);

	SocketBase *getSocket();

private:
	SocketBase *socket;
	logic::IContext *context;

	// The Logger instance is lazily initiated.
	log::Logger logger;
	bool initLogger;
};


/**
 * Search in a Socket's incoming packet queue for a packet with a specific
 * first-field value. If a packet in which the first field is of type
 * "IdType" and it's value is "id", it is extracted from the socket's packet
 * queue and returned. The packet must be explicitly deleted by the
 * caller.
 */
template<typename IdType>
Packet* Conversation::findPacket(IdType id)
{
	return this->findPacket<IdType>(this->socket, id);
}

template<typename IdType>
Packet* Conversation::findPacket(SocketBase *searchSocket, IdType id)
{
	for (unsigned i = 0; i < searchSocket->getIncomingPacketQueueSize(); i++)
	{
		const Packet *peek = searchSocket->peekPacket(i);

		try
		{
			IdType type;
			*peek >> type;

			if (type == id)
			{
				Packet *pkt = searchSocket->getPacket(i);
				return pkt;
			}
		}
		catch (...)
		{
			continue;
		}
	}

	return nullptr;
}


} } }

#endif
