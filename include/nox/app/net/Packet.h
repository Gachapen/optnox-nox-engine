#ifndef NOX_APP_NET_PACKET_H_
#define NOX_APP_NET_PACKET_H_

#include <nox/app/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>
#include <glm/vec2.hpp>

namespace nox { namespace app { namespace net {


class Packet
{
public:
	class Field
	{
	public:
		// Wrapper type for glm::vec2. glm::vec2 is of non-POD type, and is
		// at best tricky to use in a union.
		struct WrapVec2
		{
			float x;
			float y;
		};

		typedef union
		{
			unsigned u32;   // UINT32
			int i32;		// INT32
			float f32;	  // FLOAT32
			byte b;		 // BYTE
			char *str;	  // STRING
			WrapVec2 vec2;  // VEC2
			struct
			{
				unsigned len;
				byte *ptr;
			} array;		// ARRAY
		} FieldValue;

		Field(FieldType type);
		Field(const Field &other);
		~Field();

		/**
		 * Setter for primitive types: i32, u32, f32, b. The pointer is
		 * dereferenced to the type correlating to the active field type.
		 *
		 * @param val   If non-null, the value to set.
		 * @return	  True on success, false on failure.
		 */
		bool set(void *val);

		bool set(glm::vec2 vec);

		/**
		 * Setter for Field-instances of type FieldType::STRING.
		 *
		 * @param   strValue	The value to set.
		 * @return  True if the type of the Field is STRING, false otherwise.
		 */
		bool set(std::string strValue);

		/**
		 * Setter for Field-instances of type FieldType::ARRAY.
		 *
		 * @param   buffer  The buffer.
		 * @param   len	 The length.
		 * @return  True if the type of field is ARRAY and the assignment went
		 *		  OK, false otherwise.
		 */
		bool set(const byte *buffer, unsigned len);

		const FieldValue& getValue() const;
		FieldType getType() const;

		/**
		 * Get the string value of a Field of type STRING.
		 *
		 * @return  The std::string value of "value.str" if the type of the
		 *		  field is STRING, an empty
		 *		  string otherwise.
		 */
		std::string getString() const;

		/**
		 * Get the byte-array of a Field of type ARRAY. The array is pushed
		 * into a std::vector for easier usage.
		 *
		 * @return  A std::vector with the contents of FieldType::array if
		 *		  the type of the Field is FieldType::ARRAY, an empty
		 *		  std::vector otherwise.
		 */
		std::vector<byte> getArray() const;

		/**
		 * Get the glm::vec2 (wrapped in a WrapVec2) of a Field of type VEC2.
		 *
		 * @return  A glm::vec2 with the values of "value.vec2" if the type of
		 *		  this field is VEC2, glm::vec2(0,0) otherwise.
		 */
		glm::vec2 getVec2() const;

	private:
		FieldType type;
		FieldValue value;
	};

public:
	Packet();
	~Packet();

	/**
	 * Serialize the Packet's fields into a binary array. The returned array
	 * must be explicitly deleted by the caller.
	 *
	 * @param[out]  pktlen  The number of bytes returned is assigned to
	 *					  this variable.
	 *
	 * @return  Byte-array if any fields could be serialized, NULL if the
	 *		  Packet does not contain any serializable fields. The array
	 *		  is in network byte order.
	 */
	byte* serialize(int &pktlen) const;

	/**
	 * Deserialize a byte-buffer into the proper fields. The Packet-instance
	 * must have been initialized with it's correct fields beforehand.
	 *
	 * If the Packet contains any data, the data will be removed.
	 *
	 * @param   buf	 The byte-buffer containing packed field data. The array
	 *				  must be in network byte order.
	 * @param   buflen  The total length of the byte buffer.
	 *
	 * @return  The number of bytes read from the buffer. 0 is returned if the
	 *		  format of the data is invalid or upon other errors.
	 */
	int deserialize(const byte *buf, size_t buflen);

	const Packet& operator>>(byte &val) const;
	const Packet& operator>>(unsigned &val) const;
	const Packet& operator>>(int &val) const;
	const Packet& operator>>(float &val) const;
	const Packet& operator>>(std::string &val) const;
	const Packet& operator>>(std::vector<byte> &val) const;
	const Packet& operator>>(glm::vec2 &val) const;
	const Packet& operator>>(Packet &val) const;

	Packet& operator<<(byte val);
	Packet& operator<<(unsigned val);
	Packet& operator<<(int val);
	Packet& operator<<(float val);
	Packet& operator<<(std::string val);
	Packet& operator<<(std::vector<byte> val);
	Packet& operator<<(glm::vec2 val);
	Packet& operator<<(const Packet& val);

	int getFieldCount() const;
	const Packet::Field* getField(int index) const;

	/**
	 * Reset the output stream to the first field. The input stream is
	 * unaffected by this method.
	 */
	void resetStream();

	unsigned getOutStreamPosition() const;

	// Seek to a specific index.
	bool setOutStreamPosition(unsigned position) const;

	/**
	 * Seek from the curent OutStreamPosition to a field with a specific value and
	 * FieldType. If the field at the current OutStreamIndex matches the criteria,
	 * nothing is done.
	 *
	 * @param fieldType	 The desired FieldType.
	 * @param value		 The value the field must have.
	 * @tparam T			Type corresponding to the FieldType. Can not be FieldType::ARRAY.
	 *
	 * @return			  True if a Field of type "fieldType" with value "value" was found, false
	 *					  if no such field exists or "T" does not match "fieldType". If true is returned,
	 *					  the OutStreamPosition is pointing at the first field matching the requirements. If
	 *					  false is returned - for any reason - the OutStreamPosition is unchanged.
	 */
	template<typename T>
	bool seekToNext(FieldType fieldType, const T& value) const;

	/**
	 * This value is only meaningful if the packet arrived from an external
	 * host.
	 */
	IPaddress getSourceAddress() const;

	void setSourceAddress(IPaddress ipaddr);

	void logContents() const;

private:
	std::vector<Packet::Field> fields;
	mutable unsigned outFieldIndex;
	IPaddress sourceIp;

	/**
	 * Ensure that a field (A) exists, and (B) holds the expected type.
	 *
	 * @throws  std::runtime_exception
	 *		  Thrown when the fieldIndex is out of range, or the field at the
	 *		  specified index is of a different type.
	 */
	const Packet::Field* verifyField(unsigned fieldIndex, FieldType expectedType) const;
};

template<typename T>
bool Packet::seekToNext(FieldType fieldType, const T& value) const
{
	const unsigned originalOutStream = this->outFieldIndex;

	if (fieldType == FieldType::ARRAY)
	{
		return false;
	}

	while (this->outFieldIndex < this->fields.size())
	{
		if (this->fields[this->outFieldIndex].getType() == fieldType)
		{
			try
			{
				T t;
				*this >> t;

				if (t == value)
				{
					// Streaming into a variable increments the outFieldIndex - revert
					// the increment.
					this->outFieldIndex--;
					return true;
				}
			}
			catch (std::runtime_error rte)
			{
				printf("Packet::seekToNext<T>(FieldType,constT&): %s\n", rte.what());
				this->outFieldIndex = originalOutStream;
				return false;
			}
		}
		else
		{
			this->outFieldIndex++;
		}
	}

	this->outFieldIndex = originalOutStream;
	return false;
}

} } }

#endif

