#ifndef NOX_APP_NET_SOCKETTCP_H_
#define NOX_APP_NET_SOCKETTCP_H_

#include <nox/app/net/SocketBase.h>
#include <nox/app/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <string>
#include <vector>
#include <list>

namespace nox { namespace app { namespace net { 

/**
 * Socket using the TCP transport layer protocol.
 */
class SocketTcp: public SocketBase {
public:
	/**
	 * To initialize a server socket, pass 0 to "host" and a valid port no
	 * to "port". The socket will then listen on "port". To create a 
	 * client socket, pass the remote host to  "host"", and the server port to 
	 * "port".
	 * @param host	  The IP address to connect to in host in host byte order
	 * @param port	  The port to bind to in host byte order
	 */
	SocketTcp(logic::IContext *ctx, Uint32 host, Uint16 port);
	SocketTcp(logic::IContext *ctx, std::string hostname, Uint16 port);

	/**
	 * Constructor for when the connection has been initialized exeternally.
	 */
	SocketTcp(logic::IContext *ctx, TCPsocket sock);

	/**
	 * @param ipaddr	Connection details in network byte order
	 */
	SocketTcp(logic::IContext *ctx, IPaddress ipaddr);

	~SocketTcp();

	virtual bool initSocket() final;
	virtual TLProtocol getTransportLayerProtocol() const final;

private:
	virtual SDLNet_GenericSocket getGenericSocket() const final;
	virtual byte* getSocketData(int *bufferLen, IPaddress &source) final;
	virtual bool sendBuffer(const byte *buffer, int bufferLen) const final;

	/**
	 * Frees the socket (if it exists) and does any other nessecary
	 * cleanup. This method should be used when destroing the object
	 * and if the initialization fails.
	 */
	void cleanup();

	TCPsocket socket;
};


} } }

#endif
