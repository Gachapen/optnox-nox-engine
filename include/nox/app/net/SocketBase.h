#ifndef NOX_APP_NET_SOCKETBASE_H_
#define NOX_APP_NET_SOCKETBASE_H_

#include <nox/app/net/Packet.h>
#include <nox/app/net/protocol.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <string>
#include <vector>
#include <list>

namespace nox { namespace app { namespace net { 


/**
 * SocketBase
 * Abstract superclass without an underlying Transport Layer protocol. 
 *
 * Constant references to SocketBase instances can be used to send data, but
 * not retrieve it.
 */
class SocketBase {
public:
	/**
	 * Resolve a human readable host address into an API-friendly IPaddress
	 * instance.
	 *
	 * @param hostname A resolvable hostname. "google.com" or "1.1.1.1".
	 * @param port	 The port to resolve in HOST BYTE ORDER
	 * @param ipaddr   The result variable. On success, the host and 
	 *				 port fields will be assigned with proper values.
	 * @return		 True on success, false on error.
	 */
	static bool resolveHost(std::string hostname, Uint16 port, 
							IPaddress &ipaddr);

	/**
	 * @param ipaddr	Connection info in network byte order
	 */
	SocketBase(logic::IContext *ctx, IPaddress ipaddr);

	/**
	 * @param host	  The IP address to bind to in host byte order
	 * @param port	  The port to bind in host byte order
	 */
	SocketBase(logic::IContext *ctx, Uint32 host, Uint16 port);

	/**
	 * @param hostname  A resolveable hostname 
	 * @param port	  The port to bind in host byte order
	 */
	SocketBase(logic::IContext *ctx, std::string hostname, Uint16 port);
	virtual ~SocketBase();

	/**
	 * Initialize the socket. Subclasses are *REQUIRED* to use this
	 * method for full initialization of the socket. This includes
	 * setting up a connection to another computer or preparing a socket
	 * for listening on a specified port.
	 *
	 * If the socket for *ANY* reason cannot initialize, it MUST return
	 * false. Implementations are also required in the event of a failed
	 * initialization to reset it's broken state to a point where initSocket()
	 * can be called again to retry the initialization.
	 *
	 * Implementations are required to call SocketBase::onInitSuccess() if
	 * the initialization succeeded, and the return value of onInitSuccess()
	 * must be handled properly.
	 */
	virtual bool initSocket() = 0;

	/**
	 * Get the underlying transport protocol.
	 */
	virtual TLProtocol getTransportLayerProtocol() const = 0;

	/**
	 * Check if the socket is a server socket or not.
	 *
	 * @return		  True if the socket is a server socket, false otherwise.
	 */
	bool isServerSocket() const;

	/**
	 * Check if the initialization done in SocketBase::initSocket() was
	 * successful.
	 */
	bool isInitialized() const;

	/**
	 * Check if the socket is good and ready for use. 
	 */
	bool isGood() const;

	/**
	 * Check if the IPaddress is valid. Useful if the hostname needs to be
	 * resolved (i.e., the SocketBase(std::string, Uint16) ctor 
	 * was used. This is checked by assuming that BOTH the host IP and the
	 * port cannot be zero. If they are both zero, we may have a server socket
	 * on an randomly assigned port, which is considered invalid.
	 */
	bool validIPAddress() const;


	/**
	 * Check if the socket has activity. Activity can mean waiting packets, 
	 * errors, or disconnections. 
	 * @return		  True if there are packets waiting, false otherwise.
	 */
	bool hasActivity() const;

	/**
	 * Retrieve all packets from the network stream and parse them into 
	 * Packet-objects. The parsed objects are added to the packet queue, and 
	 * can be retrieved by SocketBase::popPacket().
	 * @return		  True if no errors occurred, false if the socket is in
	 *				  a bad state.
	 */
	bool parseTrafficData();

	/**
	 * Send a packet to the remote host. Server sockets cannot send packets.
	 *
	 * @return		  False if the packet cannot be sent for whatever reason,
	 *				  true if the packet was sent.
	 */
	bool sendPacket(const Packet *packet) const;

	/**
	 * Send raw bytes to the remote host. This method allows queueing of multiple
	 * packets and sending them as a single packets.
	 *
	 * @param buffer	Array of serialized Packet-instances
	 * @param len	   The length of the buffer-array
	 * @return		  True if the packet was sent, false if an error occurred
	 */
	bool sendPacket(const byte *buffer, int len) const;

	/**
	 * See how many incoming packets are waiting on the packet-queue. Packets 
	 * can be retrieved by SocketBase::popPacket().
	 */
	unsigned getIncomingPacketQueueSize() const;

	/**
	 * Put an outgoing packet on the queue. The packet will be sent to the 
	 * remote peer on the next call to flushOutgoingPacketQueue(). This method
	 * is intended to accommodate batch-sending many smaller packets and should
	 * almost always be used. The Socket takes ownership of the Packet after
	 * a call to this method, and the Socket will delete it after it has been
	 * sent.
	 */
	void queuePacket(Packet *packet) const;

	/**
	 * See how many outgoing packets are waiting to be sent. The packet queue
	 * can be dispatched to the remote peer with flushOutgoingPacketQueue().
	 */
	unsigned getOutgoingPacketQueueSize() const;

	/**
	 * Send all packets queued on this socket via queuePacket(Packet*) to the
	 * connected peer.
	 * @return	  True if no packets are on the queue or sending the packets
	 *			  was successful. False is returned on an error.
	 */
	bool flushOutgoingPacketQueue() const;

	/**
	 * Retrieve the first packet of the packet queue. SocketBase::hasActivity()
	 * will return true if any packets are waiting on the queue. The returned 
	 * packet is owned by the caller and MUST be deleted by it. Packets are 
	 * parsed from the socket and added to the packet queue when calling 
	 * SocketBase::parseTrafficData().
	 *
	 * @return		  The oldest packet in the packet queue if any exists,
	 *				  NULL if no packets exists on the queue. 
	 */
	Packet* popPacket();

	/**
	 * Retrieve an arbitrary packet from the packet queue. Similar to
	 * peekPacket, but the 
	 */
	Packet* getPacket(unsigned index = 0);

	/**
	 * Peek at the front packet. It is not removed from the queue.
	 * @return		  The first packet in the packet queue, if any exists.
	 */
	const Packet* peekPacket(unsigned index = 0) const;

	/**
	 * Get the IPaddress of the (potentially) remote host. Keep in mind that
	 * the values of the returned struct are in network-byte-order, and may
	 * appear to be wrong. Use "getOctalIP()", "getIP()" and "getPort()" to 
	 * retrieve the values in host-byte-order.
	 */
	IPaddress getIPAddress() const;

	/**
	 * @return		  The IP-address of the remote host in dotted octal
	 *				  notation, e.g. "127.0.0.1".
	 */
	std::string getOctalIP() const;

	/**
	 * Get the IP address of the remote host in host-byte-order.
	 */
	Uint32 getIP() const;

	/**
	 * Get the port of the remote host in host-byte-order.
	 */
	Uint16 getPort() const;

protected:
	log::Logger& getLogger() const;

	/**
	 * Read all waiting packets from the socket. 
	 * @return		  The number of Packet-objects parsed from the socket. 
	 *				  If there is no activity on the socket or the packets
	 *				  were not possible to interpret, 0 is returned. If the
	 *				  socket indicated activity but no data was returned, -1
	 *				  is returned and the _dc flag is set to "true". An error
	 *				  occurred, or the socket disconnected.
	 */
	int readPackets();

	/**
	 * Subclasses are required to call this method after a successful 
	 * initializatino in SocketBase::initSocket(). If this method fails,
	 * the socket is to be considered unusable and uninitialized.
	 *
	 * As onInitSuccess() can fail, implementations are required to
	 * return it's value as the final step of initSocket(), and take
	 * appropriate action if it fails.
	 *
	 * @return		  True if the final initialization step succeeded,
	 *				  false otherwise.
	 */
	bool onInitSuccess();

	virtual SDLNet_GenericSocket getGenericSocket() const = 0;

	/**
	 * Method used to retrieve all waiting data from a socket. Implementations
	 * are expected to fully drain the socket buffer.
	 *
	 * @param bufferlen The number of bytes read by the function from the socket.
	 * @param source	Implementations may override the source of the packet
	 *				  by setting "host" and "port" to non-zero values. If
	 *				  either field is zero, the default Socket remote peer IP
	 *				  address is used as the source.
	 * @return		  The data read from the socket. Can be NULL if no data
	 *				  was available. The buffer will be deleted by the caller,
	 *				  so it's critical that it is allocated inside the method.
	 */
	virtual byte* getSocketData(int *bufferLen, IPaddress &source) = 0;

	/**
	 * Implementations use this method to send data to the remote host
	 *
	 * @param buffer	The data to be sent.
	 * @param bufferLen The length of the buffer-array.
	 * @return		  True if the packet was sent successfully, false on error.
	 */
	virtual bool sendBuffer(const byte *buffer, int bufferLen) const = 0;

private:
	SocketBase();

	bool createSocketSet();
	bool addToSocketSet(SDLNet_GenericSocket socket);

	/**
	 * Undo any actions performed in a failed initialization
	 */
	void reset();

	SDLNet_SocketSet sockset;
	bool initialized;
	bool dc;
	IPaddress ipaddr;
	std::list<Packet*> inQueue;
	mutable std::list<Packet*> outQueue;

	mutable log::Logger logger;
};

} } }


#endif
