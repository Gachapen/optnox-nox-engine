#ifndef NOX_APP_NET_DEFUALTPACKETTRANSLATOR_H_
#define NOX_APP_NET_DEFUALTPACKETTRANSLATOR_H_

#include <nox/app/net/IPacketTranslator.h>
#include <nox/logic/event/Event.h>

#include <map>
#include <functional>

namespace nox { namespace app { namespace net
{

class Packet;

/**
 * DefaultPacketTranslator translates serialized system events (ClientConnected, ClientDisconnected,
 * etc) into instantiated system Event instances.
 */
class DefaultPacketTranslator final: public IPacketTranslator
{
public:
	DefaultPacketTranslator();

	virtual std::shared_ptr<logic::event::Event> translatePacket(std::string eventId, const Packet *packet) override;

private:
	using EventCreator = std::function<std::shared_ptr<logic::event::Event>(const Packet*)>;

	std::map<std::string,EventCreator> eventCreatorMap;
};

}
} }

#endif
