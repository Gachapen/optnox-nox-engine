#ifndef NOX_APP_NET_IPACKETTRANSLATOR_H_
#define NOX_APP_NET_IPACKETTRANSLATOR_H_

#include <nox/logic/event/Event.h>
#include <nox/logic/actor/Component.h>
#include <nox/app/net/Packet.h>

namespace nox { namespace app { namespace net {


/**
 * IPacketTranslator is an interface used to translate Packet objects into
 * Event objects. *HOW* the implementation decides to realize this interface is
 * up to the implementation, but the Event classes can directly deserialize from
 * a Packet.
 *
 * Using the method Event::deSerialize(const Packet*), and the assumption that
 * the first field of every Packet is an identifier, the implementation may
 * look like the following. The Packet interface was designed with this
 * specific implementation in mind, but any translation will obviously work.
 *
 * std::shared_ptr<Event> translatePacket(std::string eventId, const Packet *packet)
 * {
 *	  if (eventId == MyEvent::ID)
 *	  {
 *		  return new MyEvent(packet);
 *	  }
 *
 *	  return nullptr;
 * }
 */
class IPacketTranslator
{
public:
	/**
	 * The Packet is *GUARANTEED* to be of type nox::app::net::protocol::PacketId::EVENT_PACKET. The packet's out-stream
	 * position is pointing to the start of the serialized event, meaning that the packet can be given directly to an
	 * events Event::deSerialize(const Packet*) method.
	 */
	virtual std::shared_ptr<logic::event::Event> translatePacket(std::string eventId, const Packet *packet) = 0;
};


} } }


#endif
