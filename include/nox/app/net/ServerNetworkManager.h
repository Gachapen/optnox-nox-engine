#ifndef NOX_APP_NET_SERVERNETWORKMANAGER_H_
#define NOX_APP_NET_SERVERNETWORKMANAGER_H_

#include <nox/app/net/NetworkManager.h>
#include <nox/app/net/IPacketTranslator.h>
#include <nox/app/net/ConnectionListener.h>
#include <nox/app/net/ServerDelegate.h>
#include <nox/app/net/AccessList.h>


namespace nox { namespace app { namespace net {

class RemoteClient;
class BroadcastSender;

namespace protocol
{
class AcceptClientConversation;
}


class ServerNetworkManager: public NetworkManager
{
public:
	ServerNetworkManager(IPacketTranslator *translator, ServerDelegate *delegate);
	~ServerNetworkManager();

	bool initialize(nox::logic::IContext *context) override;
	void destroy() override;
	void update(const Duration& deltaTime) override;

	bool startServer(Uint16 tcpListenPort);

	/**
	 * Enable or disable discovery broadcast. The server will broadcast it's
	 * existance every 2 seconds on the broadcast channels on the local network
	 * on port SERVER_BROADCAST_PORT_NO.
	 *
	 * The broadcast is enabled until manually disabled (i.e., full servers
	 * will broadcast unless explicitly told otherwise).
	 */
	void setEnableDiscoveryBroadcast(bool enable);

	std::vector<RemoteClient*> getClients();

	/**
	 * Send a packet ONLY to a specified client.
	 */
	void sendExeclusivelyTo(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to everyony BUT a specified client.
	 */
	void sendToEveryoneElse(ClientId client, const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	/**
	 * Send a packet to everyone
	 */
	void sendToEveryone(const Packet& packet, TLProtocol proto = TLProtocol::UDP);

	void kickClient(ClientId id, std::string reason);

	/**
	 * Ban a client for a specified duration.
	 * @param duration  The duration to ban the client for. Use a zero-value to
	 *				  ban the client indefinitely.
	 */
	void banClient(ClientId id, const Duration &duration, std::string reason);

	bool shouldAcceptClient(IPaddress ipaddr, std::string userName);

protected:
	void sendSyncOutStateUpdates() override;

	void onClientConnected(UserData userData) override;
	void onClientDisconnected(ClientId clientId) override;

private:
	AccessList accessList;
	ServerDelegate *serverDelegate;

	bool doBroadcast;
	BroadcastSender *broadcastSender;

	ConnectionListener *serverSocket;
	std::vector<protocol::AcceptClientConversation*> acceptConvos;

	std::vector<RemoteClient*> clients;

	virtual void setLobbySize(unsigned size) override final;

	void handleNewConnections();
	void handleAcceptConvos();
	void updateRemoteClients(const Duration &deltaTime);

	void raiseDcEvent(ClientId id, protocol::DcReason dcReason, unsigned dur=0, std::string banReason="");
};

} } }


#endif
