#ifndef NOX_APP_NET_SOCKETUDP_H_
#define NOX_APP_NET_SOCKETUDP_H_

#include <nox/app/net/SocketBase.h>
#include <nox/app/net/protocol.h>
#include <nox/common/types.h>

#include <SDL2/SDL_net.h>

#include <string>
#include <vector>
#include <list>

namespace nox { namespace app { namespace net { 

#define UDP_MAX_PKT_LEN	 (2048)

/**
 * Socket using the UDP transport layer protocol
 *
 * Reading from UDP sockets requires the API to read into a UDPpacket
 * struct. The packet MUST be large enough to handle the incoming
 * packet - this could be problematic for large packets. The SocketUdp
 * object therefore allocates a UDPpacket with UDP_MAX_PKT_LEN upon
 * construction and uses this packet as a buffer for reading. If a
 * caller attempts to send more than UDP_MAX_PKT_LEN as a single
 * packet, the packet is divided into chunks of (UDP_MAX_PKT_LEN-1).
 */
class SocketUdp : public SocketBase {
public:
	/**
	 * Connect to a known remote host. As UDP is connectionless, it is
	 * extremely wise to specify a port on which you listen for activity.
	 *
	 * @param host	  The IP address of the remote host in host byte order
	 * @param port	  The port of the remote host in host byte order
	 * @param listPort  The UDP-port used for listening in host byte order. 
	 *				  Use 0 to have it automatically assigned.
	 */
	SocketUdp(logic::IContext *ctx, Uint32 host, Uint16 port, Uint16 listPort);

	/**
	 * Create a socket connected to "hostname:port", and listen on listPort.
	 */
	SocketUdp(logic::IContext *ctx, std::string hostname, Uint16 port, Uint16 listPort);

	/**
	 * @param ipaddr	Connection details in network byte order
	 * @param listPort  The port to listen to in host byte order
	 */
	SocketUdp(logic::IContext *ctx, IPaddress ipaddr, Uint16 listPort);

	~SocketUdp();

	virtual bool initSocket() final;
	virtual TLProtocol getTransportLayerProtocol() const final;

	/**
	 * Get the bound input-port in host-byte order.
	 */
	Uint16 getListenPort() const;

private:
	virtual SDLNet_GenericSocket getGenericSocket() const final;
	virtual byte* getSocketData(int *bufferLen, IPaddress &source) final;
	virtual bool sendBuffer(const byte *buffer, int bufferLen) const final;

	/**
	 * Frees the socket (if it exists) and does any other nessecary
	 * cleanup. This method should be used when destroing the object
	 * and if the initialization fails.
	 */
	void cleanup();

	UDPsocket socket;
	Uint16 listenPort;
	UDPpacket *bufferPacket;
};

} } }

#endif

