/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_CACHE_H_
#define NOX_APP_RESOURCE_CACHE_H_

#include "../IResourceAccess.h"

#include <functional>

namespace nox { namespace app { namespace resource
{

class Provider;
class ILoader;

/**
 * Loads and caches resources.
 */
class Cache: public IResourceAccess
{
public:
	/**
	 * Called when preloading resources with preload.
	 *
	 * The first parameter is the percentage of resources that has been loaded.
	 *
	 * If the called function sets the second parameter to false, the preloading
	 * will stop.
	 */
	using PreloadCallback = std::function<void (unsigned int, bool&)>;

	virtual ~Cache();

	/**
	 * Register a Provider to provide access to resources.
	 * @return If the provider was successfully opened and added.
	 */
	virtual bool addProvider(std::unique_ptr<Provider> provider) = 0;

	/**
	 * Register a resource loader to be used to load resources.
	 * @param loader Loader to register.
	 */
	virtual void addLoader(std::unique_ptr<ILoader> loader) = 0;

	/**
	 * Pre-load resources matching a pattern into the cache.
	 *
	 * The pattern allows characters (match exactly) and asterix (match any).
	 * E.g "*.json" will load any resources ending in ".json".
	 *
	 * @param pattern Resources matching pattern will be loaded.
	 * @param progressCallback Called each time a resource has been loaded.
	 * @return Number of resources loaded
	 */
	virtual int preload(const std::string& pattern, PreloadCallback progressCallback) = 0;

	/**
	 * Free all resources stored in the cache.
	 */
	virtual void flush() = 0;
};

} } }

#endif
