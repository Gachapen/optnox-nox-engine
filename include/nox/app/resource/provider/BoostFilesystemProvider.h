/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_BOOSTFILESYSTEMPROVIDER_H_
#define NOX_APP_RESOURCE_BOOSTFILESYSTEMPROVIDER_H_

#include <nox/app/resource/provider/Provider.h>
#include <nox/app/log/Logger.h>
#include <nox/common/api.h>

#include <vector>
#include <unordered_map>
#include <boost/filesystem.hpp>

namespace nox { namespace app
{
namespace resource
{

namespace bfs = boost::filesystem;

class NOX_API BoostFilesystemProvider: public Provider
{
public:
	BoostFilesystemProvider(std::string rootPath);

	bool open() override;
	unsigned int getRawResourceSize(const Descriptor& resource) override;
	unsigned int getRawResource(const Descriptor& resource, char* buffer) override;
	unsigned int getNumResources() const override;
	std::string getResourcePath(unsigned int resourceNumber) const override;

	void setLogger(log::Logger log);

private:
	struct FileInfo {
		unsigned int size;
		bfs::path path;
	};

	void scanDirectoryRecursively(const bfs::path& dirPath, Directory& currentDirectory);
	unsigned int getFileSize(std::string path) const;
	bool fileIsHidden(const bfs::path& filePath) const;
	bfs::path removeRootSegment(const bfs::path& path) const;

	log::Logger log;

	bfs::path rootPath;
	std::vector<FileInfo> fileInfoVector;
	std::unordered_map<std::string, unsigned int> filePathToNumberMap;

	bool ignoreHiddenFiles;
};

}
} }

#endif
