/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_IRESOURCEACCESS_H_
#define NOX_APP_RESOURCE_IRESOURCEACCESS_H_

#include "Handle.h"

#include <vector>

namespace nox { namespace app { namespace resource {


class IResourceAccess
{
public:
	/**
	 * Get a handle to a resource.
	 *
	 * If resource is in cache, a handle to the cached value will be returned. Otherwise the resource
	 * will be loaded from the resource provider. If resource is not accessible through the resource provider,
	 * the handler returned is nullptr.
	 *
	 * @param resource Resource to get handle to.
	 * @return Handle to resource, or nullptr if not found.
	 */
	virtual std::shared_ptr<Handle> getHandle(const Descriptor& resource) = 0;

	/**
	 * Get all the resources that are available recursively in a directory.
	 * @param directoryPath Directory to get resources from.
	 * @return All resources found.
	 */
	virtual std::vector<Descriptor> getResourcesRecursivelyInDirectory(const std::string& directoryPath) const = 0;

protected:
	virtual ~IResourceAccess();
};

} } }

#endif
