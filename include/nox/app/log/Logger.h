/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_LOG_LOGGER_H_
#define NOX_APP_LOG_LOGGER_H_

#include <nox/app/log/Message.h>
#include <nox/common/api.h>

#include <string>

namespace nox { namespace app
{
namespace log
{

class OutputManager;
class LogClient;


class NOX_API Logger
{
public:
	class NOX_API Output
	{
	public:
		static const std::size_t MAX_FORMAT_MESSAGE_SIZE;

		Output(const std::string& loggerName, OutputManager* manager, const Message::Level logLevel);

		void raw(const std::string& string);
		void format(const std::string format, ...);

	private:
		std::string loggerName;
		OutputManager* outputManager;
		Message::Level logLevel;
	};

	Logger();
	Logger(OutputManager* manager);
	Logger(const std::string& name, OutputManager* manager);

	Logger(Logger&& other);
	Logger& operator=(Logger&& other);

	Logger(const Logger& other) = delete;
	Logger& operator=(const Logger& other) = delete;

	void setName(const std::string& name);
	void setOutputManager(OutputManager* manager);

	Output info();
	Output verbose();
	Output warning();
	Output error();
	Output fatal();
	Output debug();

private:
	std::string name;
	OutputManager* outputManager;
};

}
} }

#endif
