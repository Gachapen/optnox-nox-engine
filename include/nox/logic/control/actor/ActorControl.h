/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_CONTROL_ACTORCONTROL_H_
#define NOX_LOGIC_CONTROL_ACTORCONTROL_H_

#include <nox/logic/actor/Component.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>

namespace nox { namespace logic { namespace control
{

/**
 * Abstract class for controlling an Actor.
 *
 * Other classes can extend this to implement movement for control actions. Actions
 * are received as control::Action instances through handleControl(). This class automatically
 * sets up listeners for control events.
 */
class ActorControl: public actor::Component, public event::IListener
{
public:
	ActorControl();
	virtual ~ActorControl();

	virtual bool initialize(const Json::Value& componentJson) override;
	virtual void onActivate() override;
	virtual void onDeactivate() override;

	virtual void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<event::Event>& event) override;

protected:
	virtual bool handleControl(const std::shared_ptr<Action>& controlEvent) = 0;

private:
	event::ListenerManager listener;
};

} } }

#endif
