/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_CONTROL_ACTOR2DDIRECTIONCONTROL_H_
#define NOX_LOGIC_CONTROL_ACTOR2DDIRECTIONCONTROL_H_

#include "ActorControl.h"

namespace nox { namespace logic
{

namespace actor
{

class Transform;

}

namespace physics
{

class ActorPhysics;


}

namespace control
{

/**
 * Moves the Actor in any direction in a 2D space.
 *
 * The component responds to control::Action with the "move" action. The control vector
 * is used as the input for the movement and is applied as a force. The strength of the force
 * can be adjusted by setting __movementForce__.
 *
 * By default the movement is in world coordinates, but the movement can be applied relative to the
 * Actor rotation by setting __relativeToRotation__ to true.
 *
 * # JSON Description
 * ## Name
 * 2dDirectionControl
 *
 * ## Properties
 * - __movementForce__:real - How strong the force applied is. Multiplied with the control vector. Default 1.
 * - __relativeToRotation__:boolean - If the movement is relative to the Actor's rotation. Default false.
 */
class Actor2dDirectionControl final: public ActorControl
{
public:
	static const IdType NAME;

	const IdType& getName() const override;
	bool initialize(const Json::Value& componentJson) override;
	void serialize(Json::Value& componentJson) override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onCreate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;

private:
	bool handleControl(const std::shared_ptr<Action>& controlEvent) override;

	float movementForce;
	bool relativeToRotation;

	actor::Transform* actorTransform;
	physics::ActorPhysics* actorPhysics;

	glm::vec2 controlInput;
	glm::vec2 currentMovementDirection;
};

} } }

#endif
