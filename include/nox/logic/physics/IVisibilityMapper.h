#ifndef NOX_LOGIC_PHYSICS_VISIBILITYMAPPER_H_
#define NOX_LOGIC_PHYSICS_VISIBILITYMAPPER_H_

#include <nox/app/graphics/Geometry.h>

#include <array>
#include <vector>

namespace nox { namespace logic { namespace physics
{

class Simulation;

class IVisibilityMapper
{
public:
	/**
	 * Calculate the areas invisible from a point in the world.
	 */
	virtual std::vector<std::array<glm::vec2,3>> getInvisibilityRegions(glm::vec2 pos, float radius) = 0;
};

}
} }


#endif
