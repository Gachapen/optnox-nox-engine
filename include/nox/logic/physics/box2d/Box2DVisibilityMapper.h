#ifndef NOX_LOGIC_PHYSICS_BOX2DVISIBILITYMAPPER_H_
#define NOX_LOGIC_PHYSICS_BOX2DVISIBILITYMAPPER_H_

#include <nox/logic/physics/IVisibilityMapper.h>

class b2Vec2;
class b2PolygonShape;

namespace nox { namespace logic { namespace physics
{

class Box2DSimulation;

class Box2DVisibilityMapper: public IVisibilityMapper
{
public:
	Box2DVisibilityMapper(Box2DSimulation *sim);

	std::vector<std::array<glm::vec2,3>> getInvisibilityRegions(glm::vec2 pos, float radius) override;

private:
	Box2DSimulation *simulation;

	std::vector<glm::vec2> getShapeVertices(const b2PolygonShape* polyShape);
};

}
} }

#endif
