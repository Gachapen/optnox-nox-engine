/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ACTORPHYSICS_H_
#define NOX_LOGIC_PHYSICS_ACTORPHYSICS_H_

#include "../physics_utils.h"
#include <glm/vec2.hpp>
#include <nox/logic/actor/Component.h>
#include <nox/util/math/Box.h>

namespace nox { namespace logic
{

namespace actor
{

class Identifier;

}

namespace physics
{

class Simulation;

/**
 * Enables an Actor to be physically simulated.
 *
 * # Dependencies
 * - Transform
 *
 * # JSON Description
 * ## Name
 * %Physics
 *
 * ## Properties
 * - __density__:real - The density of the actor. Default 1.0.
 * - __shape__:object - What kind of shape the body has. See parseBodyShapeJson().
 * - __shapeScale__:vec2 - The scale of the body shape. Default vec2(1.0, 1.0).
 * - __linearVelocity__:vec2 - Linear velocity of the actor. Default vec2(0, 0).
 * - __angularVelocity__:real - Angular velocity of the actor. Default 0.
 * - __angularDamping__:real - Angular damping on the actor. This slows down the rotation velocity over time. 0 will never slow down. Default 0.
 * - __linearDamping__:real - Linear damping on the actor. This slows down the linear velocity over time. 0 will never slow down. Default 0.
 * - __friction__:real - Friction of the actor. This slows down the velocity when touching other bodies. Default 0.2.
 * - __depth__:unsigned int - Decides if the actor should cast shadows. If it's below 5 it will cast shadows. Range of [0,9]. Default 9.
 * - __fixedRotation__:boolean - With fixed rotation the actor will never rotate. Default false.
 * - __sensor__:boolean - A sensor won't collide with bodies, but will receive collision callbacks. Default false.
 * - __bullet__:boolean - If the actor body should do continuous collision checks. Default false.
 * - __collisionCategory__:string - If the actor body should be a part of a collision category. Default empty string.
 * - __collisonMask__:object - Masks the actor from colliding with other bodies. By default it will collide with every body.
 *	 + __exclude__:array[string] - List of collision categories that the actor won't collide with. Default empty.
 * - __stickyness__:real - The force needed for the object to stick to a surface. Default 0.
 * - __gravityMultiplier__:real - Multiplied with the world gravity to get the actual applied gravitation force. Default 1.
 * - __rotateTowardsPull__:boolean - If body should keep its rotation perpendicular to the gravitational pull. Default false.
 */
class ActorPhysics: public actor::Component
{
public:
	static const IdType NAME;

	const IdType& getName() const override;
	bool initialize(const Json::Value& componentJsonObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	void onUpdate(const Duration& deltaTime) override;
	void onActivate() override;
	void onDeactivate() override;

	void serialize(nox::app::net::Packet& packet) override;
	void deSerialize(const nox::app::net::Packet& packet) override;

	/**
	 * Applies the passed in force to the actor's body.
	 * @param force Force to apply.
	 */
	void applyForce(const glm::vec2& force);

	/**
	 * Apply a force at a world position.
	 * @param force Force to apply
	 * @param position World position to apply the force at.
	 */
	void applyForceAtWorldPosition(const glm::vec2& force, const glm::vec2& worldPosition);

	/**
	 * Apply a force at a body position.
	 * @param force Force to apply
	 * @param position Body position to apply the force at (position local to body).
	 */
	void applyForceAtBodyPosition(const glm::vec2& force, const glm::vec2& position);

	/**
	 * Applies the passed in torque to the actor's body.
	 * @param torque Torque to apply.
	 */
	void applyTorque(const float torque);

	/**
	 * Applies the passed in impulse to the actor's body.
	 * @param actor Id of actor to apply on.
	 * @param impulse Impulse to apply.
	 */
	void applyImpulse(const glm::vec2& impulse);

	/**
	 * Set the angular velocity of the actor's body.
	 */
	void setAngularVelocity(const float angVel);

	/**
	 * Sets the position of the actor
	 *
	 * Please note that this might break the physics,
	 * setting the position directly is dangerous.
	 * @param position the position to set.
	 */
	void setPosition(const glm::vec2& position);

	/**
	 * Sets the rotation of the actor
	 *
	 * Setting the physics is not something that should be done,
	 * due to the fact that this might break it in some cases.
	 * @param rotation
	 */
	void setRotation(const float rotation);

	/**
	 * Set the rotation to be fixed (not simulated) or not.
	 * @param fixed If it should be fixed or not.
	 */
	void setFixedRotation(const bool fixed);

	/**
	 * Check if the rotation is fixed.
	 */
	bool hasFixedRotation() const;

	/**
	 * Sets the velocity of the body.
	 */
	void setVelocity(const glm::vec2& velocity);

	/**
	 * Set if the body should be a sensor or not.
	 * A sensor body does not collide, but receives contacts callbacks.
	 */
	void setSensor(bool state);

	/**
	 * Set how much gravity affects the actor
	 * @param multiplier, the multiple that gravity force is affecting the actor.
	 */
	void setGravityMultiplier(const float multiplier);


	/**
	 * Set the linear damping of the actor.
	 * @param damping The new damping.
	 * @return The previous damping.
	 */
	float setLinearDamping(const float damping);

	/**
	 * Set the angular damping of the actor.
	 * @param damping the new damping.
	 */
	void setAngularDamping(const float damping);

	/**
	 * Calculates and returns the area of the body.
	 */
	float getBodyShapeArea() const;

	/**
	 * Get the current velocity of the actor.
	 */
	glm::vec2 getVelocity() const;

	float getAngularVelocity() const;

	glm::vec2 getPosition() const;

	float getRotation() const;

	float getMass() const;

	glm::vec2 getCenterOfMass() const;

	/**
	 * Sets if the actor is awake or not.
	 * @param state true if the actor is awake.
	 */
	void setAwakeState(bool state);

	void setFriendActorGroup(const actor::Identifier& friendlyId);

	/**
	 * Get the gravitational force that is being applied to this body.
	 */
	const glm::vec2 getGravitationalPull() const;

	/**
	 * Get the angle of the gravitational force that is being applied to this body.
	 */
	float getGravitationalPullAngle() const;

	/**
	 * Get the angle of the horizon relative to the gravitational pull.
	 * This is the same as getGravitationalPullAngle() + pi/2
	 */
	float getHorizonAngle() const;

	bool bodyAabbIntersectsAabb(const math::Box<glm::vec2>& aabb) const;

	float getBodyRadius() const;

	float getMassOfStrongestGravityPullBody() const;

	float getDistanceOfStrongestGravityPullBody() const;

	/**
	 * Get the sensor state.
	 */
	bool isSensor() const;

	/**
	 * Get the gravity multiplier.
	 */
	float getGravityMultiplier() const;

	void setBodyShape(const BodyShape& shape);

	bool isDynamic() const;
	bool isStatic() const;
	bool isKinematic() const;

private:
	void serialize(Json::Value& componentObject) override;
	void updateScaledShape();

	bool readShapeFromJson(const Json::Value& componentJsonObject);
	PhysicalBodyType readBodyTypeFromJson(const Json::Value& componentJsonObject);

	BodyDefinition physicalProperties;
	BodyShape originalShape;

	float stickiness;
	glm::vec2 shapeScale;
	glm::vec2 actorScale;
	bool rotateTowardsPull;

	Simulation* physics;
};

} } }

#endif
