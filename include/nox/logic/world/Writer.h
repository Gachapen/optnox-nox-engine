/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_WRITER_H_
#define NOX_LOGIC_WORLD_WRITER_H_

#include <json/value.h>
#include <fstream>

namespace nox { namespace logic
{

class IContext;

namespace actor
{

class Actor;

}

namespace world
{

/**
 * Writer to write the world state to file.
 */
class Writer
{
public:
	Writer(const IContext* logicContext);

	void addActor(const actor::Actor* actor);
	void addActors(std::vector<const actor::Actor*> actors);

	template<class InputIterator>
	void addActors(InputIterator first, InputIterator last);

	void clear();

	/**
	 * Writes all actors, views and who they control as a json file to the path specified.
	 * @param outputPath Path to write file to.
	 */
	void write(std::ofstream& output) const;

	void write(Json::Value& worldJson) const;

private:
	void removeDuplicates(Json::Value& removeFrom, const Json::Value& duplicates) const;

	const IContext* logicContext;

	std::vector<const actor::Actor*> actorVector;
};

template<class InputIterator>
void Writer::addActors(InputIterator first, InputIterator last)
{
	auto current = first;

	while (current != last)
	{
		this->actorVector.push_back(*current);
		current++;
	}
}

} } }

#endif
