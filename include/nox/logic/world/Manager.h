/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_MANAGER_H_
#define NOX_LOGIC_WORLD_MANAGER_H_

#include <nox/app/log/Logger.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>

#include <glm/vec2.hpp>
#include <nox/logic/actor/Factory.h>
#include <nox/util/math/Box.h>

#include <thread>
#include <queue>

namespace nox { namespace logic
{

namespace actor
{

class Transform;

}

namespace event
{

class IBroadcaster;

}

namespace world
{

class Manager;

using SyncDestroyHandler = std::function<void(actor::Actor*,Manager*)>;


/**
 * Handles the world and all its Actors.
 *
 * The world is split into an infinite number of chunks with a certain size.
 * These chunks are loaded when close to the camera, and unloaded when far away.
 */
class Manager: public event::IListener
{
public:
	//! Callback called when a world save has completed.
	using SaveCompleteCallback = std::function<void ()>;

	Manager(IContext* context);
	virtual ~Manager();

	void onUpdate(const Duration& deltaTime);
	void reset();
	void saveEverything(const std::string& saveDirectory, SaveCompleteCallback completeCallback = SaveCompleteCallback());
	void setVisibleArea(const glm::vec2& position, const glm::vec2& size, const float rotation);
	void loadInitialWorld(const glm::vec2& position, const glm::vec2 size);

	void loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& definitionPath);

	/**
	 * Registers an ActorComponent for creation so that the world can create that component from
	 * an actor definition.
	 *
	 * @tparam ActorComponent The component class to register.
	 */
	template<class ActorComponentType>
	void registerActorComponent();

	/**
	 * Add a destruction handler for synchronized actors. These allow you to override what happens when the
	 * server issues a command to destroy a synchronized actor.
	 *
	 * Do realize that whatever you do, the Actor will *not* be in a synchronized state when the lambda gets
	 * called.
	 *
	 * @param name	  The name of the handler, as referenced from the actor definitions. This name can not
	 *				  already be assigned to a custom handler.
	 * @param handler   The handling lambda. It will be called when an actor with this destroy-handler name
	 *				  is destroyed.
	 */
	void addSyncActorDestroyHandler(std::string name, SyncDestroyHandler handler);

	std::unique_ptr<actor::Actor> createActorFromDefinitionName(const std::string& actorDefinitionName);
	std::unique_ptr<actor::Actor> createActorFromSource(const Json::Value& jsonSource, const std::string& definitionName);

	actor::Actor* manageActor(std::unique_ptr<actor::Actor>&& actor);

	void removeActor(std::unique_ptr<actor::Actor>&& actor);
	void removeActor(const actor::Identifier& actorId);

	/**
	 * Find an actor from an id.
	 * @param id Id of the actor.
	 * @return Pointer to actor with id, or nullptr if non found.
	 */
	actor::Actor* findActor(const actor::Identifier& id) const;
	actor::Actor* findActor(SyncId syncId) const;

	/**
	 * Find all actors within a range of a point.
	 * @param position Center of range.
	 * @param range Range radius.
	 * @return Actors within range.
	 */
	std::vector<actor::Actor*> findActorsWithinRange(const glm::vec2& position, const float range) const;

	/**
	 * Find all actors within an axis aligned box.
	 * @param box Axis aligned box.
	 * @return Actors within box.
	 */
	std::vector<actor::Actor*> findActorsWithinAxisAlignedBox(const math::Box<glm::vec2>& box) const;

	void onEvent(const std::shared_ptr<event::Event>& event);

private:

	void handleQueuedTasks();
	void clearActors();
	void handleActorCreation(actor::Actor* actor);

	void initDefaultDestroyHandlers();

	/**
	 * Called when the vision of the world has changed.
	 * This will start unloading or loading chunks based on the change of visibility.
	 * @param position Position of the visibility.
	 * @param size Size of the visibility.
	 */
	virtual void onVisionChange(const glm::vec2& position, const glm::vec2& size, const float rotation);

	virtual void saveWorld(SaveCompleteCallback completeCallback, const std::string& saveDirectory);

	virtual void onActorCreated(actor::Actor* actor);
	virtual void onActorRemoved(actor::Actor* actor);

	IContext* context;
	event::IBroadcaster* eventBroadcaster;
	app::log::Logger log;

	std::vector<actor::Transform*> nonPhysicalActorTransforms;
	mutable std::mutex nonPhysicalActorTransformsMutex;

	std::thread saveThread;

	actor::Factory actorFactory;
	std::vector<std::unique_ptr<actor::Actor>> actors;
	std::unordered_map<actor::Identifier, actor::Actor*> idToActorMap;
	std::unordered_map<SyncId, actor::Identifier> syncToIdMap;

	mutable std::mutex actorStorageMutex;
	mutable std::mutex actorIdMutex;
	bool actorUpdateLocked;

	std::queue<std::unique_ptr<actor::Actor>> actorCreations;
	thread::ThreadSafeQueue<actor::Identifier> actorRemovalQueue;

	std::unordered_map<std::string,SyncDestroyHandler> syncDestroyHandlers;

	event::ListenerManager listenerManager;
};

template<class ActorComponentType>
inline void Manager::registerActorComponent()
{
	this->actorFactory.registerComponent<ActorComponentType>();
}

}
}
}

#endif
