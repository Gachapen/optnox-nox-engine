/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_ACTORREMOVED_H_
#define NOX_LOGIC_WORLD_ACTORREMOVED_H_

#include <nox/logic/event/Event.h>
#include <memory>

namespace nox { namespace logic
{

namespace actor
{

class Actor;

}

namespace world
{

/**
 * An actor was removed from the game.
 * As long as the event instance exists, the actor will also exist.
 * While the actor exists, it has been destroyed (Actor::onDestroy()) and removed from the system.
 * The actor is destroyed with the event.
 */
class ActorRemoved final: public event::Event
{
public:
	static const IdType ID;

	ActorRemoved();
	~ActorRemoved();

	/**
	 * Get the pointer to the actor removed.
	 */
	actor::Actor* getActor() const;

	void setActor(std::unique_ptr<actor::Actor> actor);

private:
	std::unique_ptr<actor::Actor> actor;
};

} } }

#endif
