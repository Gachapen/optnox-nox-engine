/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_COMPONENT_H_
#define NOX_LOGIC_ACTOR_COMPONENT_H_

#include <nox/common/types.h>
#include <nox/app/log/Logger.h>

#include <memory>
#include <algorithm>
#include <json/value.h>

namespace nox {

namespace app
{

namespace net
{

class Packet;

}

}

namespace logic
{

class IContext;

namespace event
{

class Event;

}

namespace actor
{

class Actor;

/**
 * A component of an Actor providing a specific function.
 * Derive from this class to create custom components.
 * A component has one single owner (Actor).
 *
 * Derived classes are required to have a static IdType member named 'NAME':
 *	  static const IdType NAME = ...;
 */
class Component
{
public:
	using IdType = std::string;

	Component();

	virtual ~Component();

	Component(const Component&) = delete;
	Component& operator = (const Component&) = delete;

	void setOwner(Actor* owner);
	void setContext(IContext* logicContext);

	/**
	 * Get the actor that owns this component.
	 */
	Actor* getOwner() const;

	/**
	 * Check if the component is currently active, i.e. it's owner is active.
	 */
	bool isActive() const;

	/**
	 * Check if the component is of a specific component type.
	 *
	 * The component will be of the type if it is exactly the same type, or inherits from the type.
	 *
	 * @tparam ComponentType The component type to check that it is. Must inherit from ActorComponent.
	 * @return true if it is the type specified or inherits from it, otherwise false.
	 */
	template<class ComponentType>
	bool isType() const;

	/**
	 * Cast the component to another component type.
	 *
	 * This function is safe to use since it verifies that it can be casted to the type
	 * specified.
	 *
	 * @tparam ComponentType Type to cast component to.
	 * @return Pointer to the cast type, or nullptr if it cannot be cast to the type.
	 */
	template<class ComponentType>
	ComponentType* castToType();

	/**
	 * Initialize this component from the provided json object.
	 * @param componentJsonObject Json object to load data from.
	 * @return true if successfully initialized, otherwise false.
	 */
	virtual bool initialize(const Json::Value& componentJsonObject) = 0;

	/**
	 * Called when this component should write its properties to a json object.
	 * Subclasses should write all their properties here.
	 * @param componentObject Object to write to.
	 */
	virtual void serialize(Json::Value& componentObject) = 0;

	/**
	 * Called when the actor is created.
	 * The component should set up all data and prepare itself to be simulated.
	 */
	virtual void onCreate();

	/**
	 * Called when the component is activated.
	 * The component should enable all of its system so that it can be simulated.
	 */
	virtual void onActivate();

	/**
	 * Called when the component is deactivated from being active.
	 * The component should disable all of its systems so that it won't bother the CPU.
	 */
	virtual void onDeactivate();

	/**
	 * Called before component is removed from memory.
	 * The component should clean up it's data here.
	 */
	virtual void onDestroy();

	/**
	 * Called when a new component was attached to the owning actor.
	 * This function is also called for each actor component already attached to the actor owner
	 * when this is attached to it.
	 * @param component Component attached.
	 */
	virtual void onComponentAttached(Component* component);

	/**
	 * Called one each frame.
	 */
	virtual void onUpdate(const Duration& deltaTime);

	/**
	 * Called when an event is broadcasted locally from another ActorComponent
	 * in the same Actor.
	 * @param event The event received.
	 */
	virtual void onComponentEvent(const std::shared_ptr<event::Event>& event);

	/**
	 * Get all the unique inherited names of this ActorComponent.
	 *
	 * The list is empty if the component does not inherit from any other component.
	 * The order of the names in the list is the order of the inheritance with the deepest (base class) at
	 * the bottom.
	 *
	 * All component classes inheriting another component class (excluding ActorComponent) must override this method and provide
	 * the base class/classes names.
	 */
	virtual std::vector<IdType> findInheritedNames() const;

	/**
	 * Return the unique name of this ActorComponent class.
	 * This name identifies each ActorComponent types.
	 * @return The name of the class of this ActorComponent instance.
	 */
	virtual const IdType& getName() const = 0;

	/**
	 * Called when a child actor is attached.
	 */
	virtual void onChildActorAttached(Actor* childActor, const std::string& childName);

	/**
	 * Called when a child actor is detached.
	 */
	virtual void onChildActorDetached(Actor* childActor, const std::string& childName);

	/**
	 * Write synchronizable attributes of this Component to a Packet. It's crucial
	 * that all data serialized is also properly deSerialized. *ALL* data written into
	 * a packet in Component::serialize(nox::app::net::Packet&) *MUST* also be retrieved in
	 * Component::deSerialize(nox::app::net::Packet&).
	 */
	virtual void serialize(nox::app::net::Packet &packet);

	/**
	 * Read synchronizable attributes of this Component from a Packet. It's crucial that
	 * all data written in Component::serialize() is retrieved from this method.
	 */
	virtual void deSerialize(const nox::app::net::Packet &packet);

protected:
	IContext* getLogicContext() const;

	app::log::Logger& getLog();

private:
	app::log::Logger log;

	Actor* owner;
	IContext* logicContext;
};


inline Actor* Component::getOwner() const
{
	return this->owner;
}

inline IContext* Component::getLogicContext() const
{
	return this->logicContext;
}

inline app::log::Logger& Component::getLog()
{
	return this->log;
}

template<class ComponentType>
inline bool Component::isType() const
{
	std::vector<IdType> names = this->findInheritedNames();
	names.push_back(this->getName());

	const bool isOfType = std::any_of(
			names.begin(),
			names.end(),
			[](const IdType& name)
			{
				return (name == ComponentType::NAME);
			}
	);

	return isOfType;
}

template<class ComponentType>
inline ComponentType* Component::castToType()
{
	if (this->isType<ComponentType>() == true)
	{
		return static_cast<ComponentType*>(this);
	}
	else
	{
		return nullptr;
	}
}

}
} }

#endif
