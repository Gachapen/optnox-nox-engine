#ifndef NOX_APP_NET_SYNCATTRIBUTES_H_
#define NOX_APP_NET_SYNCATTRIBUTES_H_

#include <nox/logic/actor/Actor.h>
#include <nox/app/net/protocol.h>

#include <set>

namespace nox { namespace logic { namespace actor
{

class Factory;

class SyncAttributes
{
	friend class Factory;
public:
	static const app::net::LagCompensation DEFAULT_LAG_COMP;
	static const std::string DEFAULT_DESTROY_HANDLER;

	void addSyncComponent(const Component::IdType& idtype);
	void setLagCompensation(app::net::LagCompensation lag);
	void setDestroyHandlerName(std::string name);

	const std::set<Component::IdType>& getSyncComponents() const;
	app::net::LagCompensation getLagCompensation() const;
	bool getOwnedLocally() const;
	ClientId getOwnerClientId() const;
	SyncId getSyncId() const;
	std::string getDestroyHandlerName() const;

private:
	SyncAttributes(bool ownedLocally, ClientId owner, SyncId syncId);
	SyncAttributes(const SyncAttributes&) = delete;


	bool ownedLocally;
	ClientId ownerClient;
	SyncId syncId;
	std::string destroyHandler;

	std::set<Component::IdType> syncComponents;
	app::net::LagCompensation lagCompensation;
};

}
} }

#endif
