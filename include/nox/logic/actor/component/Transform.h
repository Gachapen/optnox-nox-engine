/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_TRANSFORM_H_
#define NOX_LOGIC_ACTOR_TRANSFORM_H_

#include "../Component.h"
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

namespace nox { namespace logic { namespace actor
{

/**
 * Place an Actor with a position, rotation, and scale.
 * An important component used by many other components.
 *
 * # JSON Description
 * ## Name
 * %Transform
 *
 * ## Properties
 * - __position__:vec2 - Position of the Actor. Default vec2(0, 0).
 * - __rotation__:real - Rotation of the Actor. Default 0.
 * - __scale__:vec2 - Scale of the actor. Default vec2(1, 1).
 */
class Transform: public Component
{
public:
	//! Overloaded functions using this tag do not broadcast the transform change.
	struct NoBroadcast_t {};

	const static IdType NAME;

	virtual ~Transform();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	void serialize(nox::app::net::Packet& packet) override;
	void deSerialize(const nox::app::net::Packet& packet) override;

	/**
	 * Get the transform matrix representing the operations scale->rotate->translate.
	 */
	glm::mat4 getTransformMatrix();

	void setPosition(const glm::vec2& position);
	void setPosition(const glm::vec2& position, NoBroadcast_t);

	void setScale(const glm::vec2& scale);
	void setScale(const glm::vec2& scale, NoBroadcast_t);

	void setRotation(const float rotation);
	void setRotation(const float rotation, NoBroadcast_t);

	void setTransform(const glm::vec2& position, const float rotation, const glm::vec2& scale);
	void setTransform(const glm::vec2& position, const float rotation, const glm::vec2& scale, NoBroadcast_t);

	const glm::vec2& getPosition() const;
	float getRotation() const;
	const glm::vec2& getScale() const;

	/**
	 * Broadcast a transformation change to other components,
	 * and globally to the EventManager.
	 */
	void broadcastTransformChange();
private:

	glm::vec2 position;
	glm::vec2 scale;
	float rotation;
};


inline void Transform::setPosition(const glm::vec2& position, NoBroadcast_t)
{
	this->position = position;
}

inline void Transform::setScale(const glm::vec2& scale, NoBroadcast_t)
{
	this->scale = scale;
}

inline void Transform::setRotation(const float rotation, NoBroadcast_t)
{
	this->rotation = rotation;
}

inline void Transform::setTransform(const glm::vec2& position, const float rotation, const glm::vec2& scale, NoBroadcast_t)
{
	this->position = position;
	this->scale = scale;
	this->rotation = rotation;
}

inline const glm::vec2& Transform::getPosition() const
{
	return this->position;
}

inline float Transform::getRotation() const
{
	return this->rotation;
}

inline const glm::vec2& Transform::getScale() const
{
	return this->scale;
}

} } }

#endif
