#ifndef NOX_UTIL_CLOCK_H_
#define NOX_UTIL_CLOCK_H_

#include <nox/common/types.h>
#include <chrono>

namespace nox { namespace util
{

/**
 * The Clock class measures time. The timer can be restarted using the Clock::start()
 * method, and the time passed since the last Clock::start()-call (or creation) can
 * be retrieved from Clock::getElapsedTime().
 */
class Clock
{
public:
	Clock();

	void start();

	Duration getElapsedTime() const;

private:
	std::chrono::time_point<std::chrono::high_resolution_clock,Duration> startTime;
};

}
}


#endif
