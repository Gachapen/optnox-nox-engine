/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_THREAD_THREADSAFEQUEUE_H_
#define NOX_THREAD_THREADSAFEQUEUE_H_

#include <deque>
#include <condition_variable>
#include <algorithm>

namespace nox
{
namespace thread
{

/**
 * A queue that has thread safe operations.
 * @tparam Type The type to be contained in the queue.
 */
template<class Type>
class ThreadSafeQueue {
public:
	/**
	 * Construct queue with invalid value defined as default constructed Type.
	 */
	ThreadSafeQueue();

	/**
	 * Push a value to the queue.
	 * @param value Value to push
	 */
	void push(const Type& value);

	/**
	 * Push a value to the queue.
	 * @param value Value to push
	 */
	void push(Type&& value);


	/**
	 * Pop off and return a value on the queue.
	 * @param invalidTypeArgs Arguments passed to the object constructed when queue is empty.
	 * @return The value popped, or value constructed from invalidTypeArgs if queue is empty.
	 */
	template<class... InvalidTypeArgs>
	Type pop(InvalidTypeArgs&&... invalidTypeArgs);

	/**
	 * Clear all values in the queue.
	 */
	void clear();

	/**
	 * Get the number of values in the queue.
	 */
	unsigned int getSize() const;

	/**
	 * Check if the queue is empty or not.
	 */
	bool isEmpty() const;

	/**
	 * Check if the queue contains an element.
	 * @param element Element to check.
	 * @return true if it has it, otherwise false.
	 */
	bool containsElement(const Type& element) const;

private:
	std::deque<Type> queue;

	mutable std::mutex queueMutex;
};

template<class Type>
inline ThreadSafeQueue<Type>::ThreadSafeQueue()
{
}


template<class Type>
inline void ThreadSafeQueue<Type>::push(const Type& value)
{
	std::lock_guard<std::mutex> queueLock(this->queueMutex);

	this->queue.push_back(value);
}

template<class Type>
inline void ThreadSafeQueue<Type>::push(Type&& value)
{
	std::lock_guard<std::mutex> queueLock(this->queueMutex);

	this->queue.push_back(std::forward<Type>(value));
}

template<class Type>
template<class... InvalidTypeArgs>
inline Type ThreadSafeQueue<Type>::pop(InvalidTypeArgs&&... invalidTypeArgs)
{
	std::lock_guard<std::mutex> queueLock(this->queueMutex);

	if (this->queue.empty() == true)
	{
		return Type(invalidTypeArgs...);
	}

	Type value(std::move(this->queue.front()));
	this->queue.pop_front();

	return value;
}

template<class Type>
inline unsigned int ThreadSafeQueue<Type>::getSize() const
{
	return static_cast<unsigned int>(this->queue.size());
}

template<class Type>
inline bool ThreadSafeQueue<Type>::isEmpty() const
{
	return this->queue.empty();
}

template<class Type>
inline void ThreadSafeQueue<Type>::clear()
{
	std::lock_guard<std::mutex> queueLock(this->queueMutex);

	this->queue.clear();
}

template<class Type>
inline bool ThreadSafeQueue<Type>::containsElement(const Type& element) const
{
	std::lock_guard<std::mutex> queueLock(this->queueMutex);

	const auto elementIt = std::find(this->queue.begin(), this->queue.end(), element);
	return (elementIt != this->queue.end());
}

}
}

#endif
