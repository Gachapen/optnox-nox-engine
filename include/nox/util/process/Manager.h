/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_PROCESS_MANAGER_H_
#define NOX_PROCESS_MANAGER_H_

#include <nox/util/process/Implementation.h>

#include <vector>
#include <memory>

namespace nox
{

namespace process
{

class Process;

/**
 * Manager of running processes.
 * Handles a list of processes and updates them.
 */
class Manager
{
public:
	enum class UpdateMode
	{
		ONCE_PER_UPDATE,
		FIXED_DELTATIME
	};

	/**
	 * Handle to a process.
	 * Used to interact with a running Process in the ProcessManager.
	 */
	class ProcessHandle
	{
		friend class Manager;

	public:
		ProcessHandle();
		ProcessHandle(Process* process, Manager* manager);

		/**
		 * Check if the handle is valid.
		 * The handle is valid if the handle's process is running in
		 * the manager.
		 */
		bool isValid() const;

	private:
		/**
		 * Check if the handle is handle to the passed process pointer.
		 */
		bool isHandleToProcess(const Process* process) const;

		Process* process;
		Manager* manager;
	};

	Manager();
	Manager(const UpdateMode updateMode, const Duration& deltaTime);

	Manager(Manager&& other);
	Manager& operator=(Manager&& other);

	virtual ~Manager();

	/**
	 * Run the update tick for all processes.
	 */
	void updateProcesses(const Duration& deltaTime);

	/**
	 * Start a process from a ProcessImplementation.
	 * @param implementation Implementation to start.
	 * @return Handle to the process started.
	 */
	ProcessHandle startProcess(std::unique_ptr<Implementation> implementation);

	/**
	 * Chain a ProcessImplementation to an already started process.
	 * The process chained will be started automatically if the process
	 * chained to succeeds its execution.
	 * @param chainToHandle Handle to running process to chain to.
	 * @param implementation Implementation to chain.
	 * @return Handle to the chained process. Not valid until process started.
	 */
	ProcessHandle chainProcess(const ProcessHandle& chainToHandle, std::unique_ptr<Implementation> implementation);

	/**
	 * Abort a process.
	 * The process will reach the ProcessImplementation::onAbort() function.
	 * @param handle Handle to process to abort.
	 * @return true if process aborted, false if it isn't valid.
	 */
	bool abortProcess(const ProcessHandle& handle);

	/**
	 * Abort every process currently managed.
	 *
	 * Equal to calling abortProcess on each process.
	 */
	void abortAllProcesses();

private:
	void runUpdate(const Duration& deltaTime);

	/**
	 * Check if the manager has the process specified.
	 */
	bool hasProcess(const Process* process) const;
	
	/**
	 * Check if the manager has the running process.
	 */
	bool isRunning(const Process* process) const;
	
	std::vector<std::unique_ptr<Process>> processes;

	UpdateMode updateMode;
	Duration fixedDeltaTime;
	Duration accumulatedTime;
};

}
}

#endif
